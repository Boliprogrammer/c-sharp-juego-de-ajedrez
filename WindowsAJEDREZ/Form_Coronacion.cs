﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



namespace Chess
{
    
    public partial class Form_Coronacion : Form
    {
        
        CodificacionPiezas m_cod_piezas = new CodificacionPiezas();
        public byte m_Cod_Form_Coronacion = 0;
        public Form_Coronacion()
        {
            InitializeComponent();
        }
        private void panel_caballo_MouseClick(object sender, MouseEventArgs e)
        {
            
            if ( (byte)(m_Cod_Form_Coronacion & 1) == (byte)1)
            {
                m_Cod_Form_Coronacion = m_cod_piezas.CB();
                
            }
            else
            {
                m_Cod_Form_Coronacion = m_cod_piezas.CN();
            }
            
        }

        private void Alfil_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Alfil_Click(object sender, MouseEventArgs e)
        {
            if ((byte)(m_Cod_Form_Coronacion & 1) == (byte)1)
            {
                m_Cod_Form_Coronacion = m_cod_piezas.AB();
            }
            else
            {
                m_Cod_Form_Coronacion = m_cod_piezas.AN();
            }
        }

        private void Torre_Click(object sender, MouseEventArgs e)
        {
            if ((byte)(m_Cod_Form_Coronacion & 1) == (byte)1)
            {
                m_Cod_Form_Coronacion = m_cod_piezas.TB();
            }
            else
            {
                m_Cod_Form_Coronacion = m_cod_piezas.TN();
            }
        }

        private void Reina_Click(object sender, MouseEventArgs e)
        {
            if ((byte)(m_Cod_Form_Coronacion & 1) == (byte)1)
            {
                m_Cod_Form_Coronacion = m_cod_piezas.QB();
            }
            else
            {
                m_Cod_Form_Coronacion = m_cod_piezas.QN();
            }
            
        }

        private void panel_caballo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form_Coronacion_Load(object sender, EventArgs e)
        {

        }
    }
}
