﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    class TimePeriod
    {
        private double m_seconds;

        
        public double Hours
        {
            get{ return m_seconds/3600;}
            set { m_seconds = value * 3600; }
        }
    }
}
