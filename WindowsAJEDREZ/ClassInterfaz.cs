using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;

namespace WindowsAJEDREZ
{
    class Interfaz
    {
        Form m_Form;
        PictureBox[] m_ImgPiezas;
        PaintEventArgs m_PaintEventArgs;
        Graphics m_gfx_Tablero;
        Rectangle m_Rectangle;
        short m_dificultad;
        bool m_P_Blancas_Abajo;
        String m_ruta_imagenes;
        String m_extension_imgs;
        int m_cont_imgs;
        int m_separ_piezas_x;
        int m_separ_piezas_y;
        int m_pos_x_tablero;
        int m_pos_y_tablero;

        public Interfaz()
        {
            m_separ_piezas_x = 66;
            m_separ_piezas_y = 66;
            m_extension_imgs = "bmp";
            m_dificultad = 3;
            m_cont_imgs = 0;

        }


        public void Crear_Interfaz(Form form, PaintEventArgs paintEventArgs, int pos_x_tablero, int pos_y_tablero, Int16 tam_lado, bool P_Blancas_Abajo, String ruta_imagenes)
        {
            m_P_Blancas_Abajo = P_Blancas_Abajo;
            m_Form = form;
            m_PaintEventArgs = paintEventArgs;
            m_ruta_imagenes = ruta_imagenes;
            m_ImgPiezas = new PictureBox[33];
            m_pos_x_tablero = pos_x_tablero;
            m_pos_y_tablero = pos_y_tablero;
            //m_gfx_Tablero = gfx_Tablero;


            /*
            Graphics gfx = m_PaintEventArgs.Graphics;
            
            int cont = 1;
            Pen myPen = new Pen(Color.Black,3);

            for (int h = 0; h <= 8; h++)//Lineas horizontales
            {
                gfx.DrawLine(myPen, m_pos_x_tablero - 2, m_pos_y_tablero + (tam_lado * h) - 1, (tam_lado * 8) + m_pos_x_tablero + 1, m_pos_y_tablero + (tam_lado * h) - 1);
            }

            for (int v = 0; v <= 8; v++)//lineas verticales
            {
                gfx.DrawLine(myPen, m_pos_x_tablero + (tam_lado * v) - 1, m_pos_y_tablero, m_pos_x_tablero + (tam_lado * v) - 1, m_pos_y_tablero + tam_lado * 8 - 1);
            }


            
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {

                    int residuo = cont % 2;
                    Color brushColor;
                    if (residuo == 0)
                    {
                        brushColor = Color.FromArgb(200, 0, 0);
                    }
                    else
                    {
                        brushColor = Color.FromArgb(255, 255, 255);
                    }
                    SolidBrush myBrush = new SolidBrush(brushColor);
                    gfx.FillRectangle(myBrush, m_pos_x_tablero + (tam_lado * x), m_pos_y_tablero + (tam_lado * y), tam_lado - 1, tam_lado - 1);

                    cont++;
                }
                cont--;

            }
             */
            Crear_Piezas_Todas();

        }

        public void Mover_Pieza()
        {

        }

        public void Crear_Piezas_Todas()
        {
            CrearPeones();
            CrearLineaDeTorreATorre();
        }

        public void CrearPeones()
        {
            int blancas, negras;
            if (m_P_Blancas_Abajo == true) { blancas = 1; negras = 2; }
            else { blancas = 2; negras = 1; }
            for (int i = 1; i <= 8; i++)
            {
                String ruta_img = Obtener_ruta_img_v(negras, 6);
                CrearPiezaYa(i - 1, 1, ruta_img, true, 23, 21);
            }

            for (int i = 1; i <= 8; i++)
            {
                String ruta_img = Obtener_ruta_img_v(blancas, 6);
                CrearPiezaYa(i - 1, 6, ruta_img, true, 23, 18);
            }
        }

        public void CrearLineaDeTorreATorre()
        {
            CrearTorreCaballoAlfil();
            CrearReyReina();
        }

        public String Obtener_ruta_img_v(int pos1, int pos0)
        {
            String a_string = Convert.ToString(pos1);
            String b_string = Convert.ToString(pos0);
            String ruta_img = m_ruta_imagenes + a_string + b_string + "." + m_extension_imgs;
            return ruta_img;
        }

        public void CrearPiezaYa(int x, int y, String ruta_img_final, bool Transparencia, int Desface_x, int Desface_y)
        {
            //m_ImgPiezas[m_cont_imgs] = new TImage(m_Form1);
            m_ImgPiezas[1].Parent = m_Form;
            m_ImgPiezas[m_cont_imgs].Left = m_pos_x_tablero + Desface_x + (x * m_separ_piezas_x);
            m_ImgPiezas[m_cont_imgs].Top = m_pos_x_tablero + Desface_y + (y * m_separ_piezas_y);

            //m_ImgPiezas[m_cont_imgs]->AutoSize = true;
            //m_ImgPiezas[m_cont_imgs]->Transparent = Transparencia;
            m_ImgPiezas[m_cont_imgs].Load(ruta_img_final);
            m_cont_imgs++;
        }

        public void CrearTorreCaballoAlfil()
        {
            int blancas, negras;
            if (m_P_Blancas_Abajo == true) { blancas = 1; negras = 2; }
            else { blancas = 2; negras = 1; }

            for (int x = 1; x <= 3; x++)
            {
                for (int y = 1; y <= 2; y++)
                {
                    int x_aux = x;
                    if (y == 2) x_aux = 9 - x;
                    String ruta_img = Obtener_ruta_img_v(negras, x);
                    CrearPiezaYa(x_aux - 1, 0, ruta_img, true, 23, 17);
                }
            }

            for (int x = 1; x <= 3; x++)
            {
                for (int y = 1; y <= 2; y++)
                {
                    int x_aux = x;
                    if (y == 2) x_aux = 9 - x;
                    String ruta_img = Obtener_ruta_img_v(blancas, x);
                    CrearPiezaYa(x_aux - 1, 7, ruta_img, true, 23, 17);
                }
            }
        }

        public void CrearReyReina()
        {
            int blancas, negras;
            if (m_P_Blancas_Abajo == true) { blancas = 1; negras = 2; }
            else { blancas = 2; negras = 1; }

            if (m_P_Blancas_Abajo == true)
            {
                int i = 4;
                for (int y = 4; y <= 5; y++)
                {
                    String ruta_img = Obtener_ruta_img_v(negras, y);
                    CrearPiezaYa(i - 1, 0, ruta_img, true, 23, 17);
                    i++;
                }

                i = 4;
                for (int y = 4; y <= 5; y++)
                {
                    String ruta_img = Obtener_ruta_img_v(blancas, y);
                    CrearPiezaYa(i - 1, 7, ruta_img, true, 23, 15);
                    i++;
                }
            }
            else
            {
                int i = 5;
                for (int y = 4; y <= 5; y++)
                {
                    String ruta_img = Obtener_ruta_img_v(negras, y);
                    CrearPiezaYa(i - 1, 0, ruta_img, true, 23, 17);
                    i--;

                }
                i = 5;
                for (int y = 4; y <= 5; y++)
                {
                    String ruta_img = Obtener_ruta_img_v(blancas, y);
                    CrearPiezaYa(i - 1, 7, ruta_img, true, 23, 15);
                    i--;
                }
            }
        }
        /*       
        protected override void OnPaint(PaintEventArgs paintEvnt)
        {

            
            // Get the graphics object
            Graphics gfx = paintEvnt.Graphics;
            int cont = 1;
            Pen myPen = new Pen(Color.Black);

            for (int h = 1; h <=9; h++)
            {
                gfx.DrawLine(myPen, 100, 50+(50 * h)-1, 500 , 50+(50 * h)-1);
            }

            for (int v = 1; v <= 9; v++)
            {
                gfx.DrawLine(myPen, 50+ (50 * v)-1, 100, 50+(50 * v)-1, 500);
            }

            for (int x = 1; x <= 8; x++)
            {
                for (int y = 1; y <= 8; y++)
                {
                    
                    int residuo = cont % 2;
                    Color brushColor;
                    if (residuo == 0)
                    {
                        brushColor = Color.FromArgb(200, 0, 0);
                    }
                    else
                    {
                        brushColor = Color.FromArgb(255, 255, 255);
                    }
                    SolidBrush myBrush = new SolidBrush(brushColor);
                    gfx.FillRectangle(myBrush, 50 + (50*x), 50 + (50*y), 49, 49);
                    
                    cont++;
                }
                cont--;
                
            }
        }
        */

    }
}
