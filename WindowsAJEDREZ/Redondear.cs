using System;
using System.Collections.Generic;
using System.Text;

namespace Chess
{
    class Redondear
    {
        byte separ_piezas_x;
        public Redondear() { byte separ_piezas_x = 66; }
        public int RedondearValor_SeparPiezas(float CoordXY)
        {
            float CoordXY_separ_piezas = CoordXY / separ_piezas_x;
            //REDONDEA POSIBLES MOVIMIENTOS > 0
            if (CoordXY_separ_piezas >= -0.5 && CoordXY_separ_piezas <= 0.5)
                CoordXY_separ_piezas = 0;
            else if (CoordXY_separ_piezas > 0.5 && CoordXY_separ_piezas < 1.5)
                CoordXY_separ_piezas = 1;
            else if (CoordXY_separ_piezas >= 1.5 && CoordXY_separ_piezas < 2.5)
                CoordXY_separ_piezas = 2;
            else if (CoordXY_separ_piezas >= 2.5 && CoordXY_separ_piezas < 3.5)
                CoordXY_separ_piezas = 3;
            else if (CoordXY_separ_piezas >= 3.5 && CoordXY_separ_piezas < 4.5)
                CoordXY_separ_piezas = 4;
            else if (CoordXY_separ_piezas >= 4.5 && CoordXY_separ_piezas < 5.5)
                CoordXY_separ_piezas = 5;
            else if (CoordXY_separ_piezas >= 5.5 && CoordXY_separ_piezas < 6.5)
                CoordXY_separ_piezas = 6;
            else if (CoordXY_separ_piezas >= 6.5 && CoordXY_separ_piezas < 7.5)
                CoordXY_separ_piezas = 7;

            //REDONDEA POSIBLES MOVIMIENTOS < 0
            else if ((CoordXY_separ_piezas < -0.5 && CoordXY_separ_piezas >= -1.5))
                CoordXY_separ_piezas = -1;
            else if ((CoordXY_separ_piezas < -1.5 && CoordXY_separ_piezas >= -2.5))
                CoordXY_separ_piezas = -2;
            else if ((CoordXY_separ_piezas < -2.5 && CoordXY_separ_piezas >= -3.5))
                CoordXY_separ_piezas = -3;
            else if ((CoordXY_separ_piezas < -3.5 && CoordXY_separ_piezas >= -4.5))
                CoordXY_separ_piezas = -4;
            else if ((CoordXY_separ_piezas < -4.5 && CoordXY_separ_piezas >= -5.5))
                CoordXY_separ_piezas = -5;
            else if ((CoordXY_separ_piezas < -5.5 && CoordXY_separ_piezas >= -6.5))
                CoordXY_separ_piezas = -6;
            else if ((CoordXY_separ_piezas < -6.5 && CoordXY_separ_piezas >= -7.5))
                CoordXY_separ_piezas = -7;

            return (byte)CoordXY_separ_piezas;
        }
    }
}
