﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace WindowsAJEDREZ
{
    class Peon : Pieza
    {
        protected bool m_BlancasAbajo;
        protected byte m_separ_piezas;

        public override void EstablecerValoresImportantes(bool BlancasAbajo, byte separ_piezas)
        {
            m_BlancasAbajo = BlancasAbajo;
            m_separ_piezas = separ_piezas;
        }
        public override CoordenadaXY ValidarMovimiento(int x_inicial, int y_inicial, int x_destino, int y_destino, byte separ_piezas, byte Codificacion)
        {
            CoordenadaXY valido = new CoordenadaXY();
            valido.x = 0;
            valido.y = 0;
            float dif_X = x_destino - x_inicial;
            float dif_Y = y_destino - y_inicial;

            if (ObtenerColorPieza(Codificacion) == 0)//blancas
            {
                if (dif_Y / separ_piezas <= 1.5 && dif_Y / separ_piezas >= 0 && dif_X / separ_piezas < 0.5 && dif_X / separ_piezas > -0.5)
                {
                    valido.y = dif_Y / separ_piezas;
                    valido.x = dif_X / separ_piezas;




                   
                }
                if (dif_Y / separ_piezas <= 2.5 && dif_Y / separ_piezas > 1.5 && dif_X / separ_piezas < 0.5 && dif_X / separ_piezas > -0.5 && y_inicial / separ_piezas == 1)
                {
                    valido.y = dif_Y / separ_piezas;
                    valido.x = dif_X / separ_piezas;
                }
                

            }
            if (ObtenerColorPieza(Codificacion) == 1)//negras
            {
                dif_Y = Math.Abs(dif_Y);
                if (dif_Y / separ_piezas <= 1.5 && dif_Y / separ_piezas >= 0 && dif_X / separ_piezas < 0.5 && dif_X / separ_piezas > -0.5)
                {
                    valido.y = dif_Y / separ_piezas;
                    valido.x = dif_X / separ_piezas;
                   
                }
                if (dif_Y / separ_piezas <= 2.5 && dif_Y / separ_piezas > 1.5 && dif_X / separ_piezas < 0.5 && dif_X / separ_piezas > -0.5 && (y_inicial-17 )/ separ_piezas == 6)
                {
                    valido.y = dif_Y / separ_piezas;
                    valido.x = dif_X / separ_piezas;
                }            
            }
            return valido;
        }
        public override CoordenadaXY ValidarComer(int x_inicial, int y_inicial, int x_destino, int y_destino, byte separ_piezas, byte Codificacion)
        {
            CoordenadaXY valido = new CoordenadaXY();
            valido.x = 0;
            valido.y = 0;
            float dif_X = x_destino - x_inicial;
            float dif_Y = y_destino - y_inicial;
            if (ObtenerColorPieza(Codificacion) == 1)
                //si el peon está en la posición 3,3 hace al tablero un GET a la posicion 4,4 
                //y le saca su coloro y si es contrario al 3,3 que coma
                //*sino no coma

            {
                if (dif_Y / separ_piezas <= 1.5 && dif_Y / separ_piezas >= 0 && dif_X / separ_piezas < 0.5 && dif_X / separ_piezas > -0.5)
                {
                    valido.y = dif_Y / separ_piezas;
                    valido.x = dif_X / separ_piezas;
                }
            }
            return valido;
        }

        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte peon = new byte();
            peon = (byte)(peon | 1); //0000 0001
            peon = (byte)(peon << 1); //0000 0010
            peon = (byte)(peon | 1); //0000 0011
            peon = (byte)(peon << 1); // 0000 0110
            peon = (byte)(peon | 1); // 0000 0111
            peon = (byte)(peon << 1); // 0000 1110 //NEGRA
            if (es_blanca == true)
                peon = (byte)(peon | 1); //0000 1111 //BLANCA

            return peon;
        }

        public override void MoverPieza(System.Windows.Forms.Panel PanelFichaX, int PanelFichas_Left, int PanelFichas_Top, int mouseX_inicial, int mouseY_inicial, CoordenadaXY DesplazamientoValido, byte Codificacion)
        {
            
            int PosicionFicha_Y = (mouseY_inicial) / m_separ_piezas;
            int PosicionFicha_X = (mouseX_inicial) / m_separ_piezas;
            PanelFichaX.Left = PanelFichas_Left;
            if (DesplazamientoValido.y > 0.5 && DesplazamientoValido.y < 1.5)
                DesplazamientoValido.y = 1;
            if (DesplazamientoValido.y > 1.5 && DesplazamientoValido.y < 2.5)
                DesplazamientoValido.y = 2;
            if (ObtenerColorPieza(Codificacion) == 0)
            {
                PanelFichaX.Top = m_separ_piezas * (Convert.ToInt32(DesplazamientoValido.y) + PosicionFicha_Y) + 17;
            }
            else
            {
                PanelFichaX.Top = ((7* m_separ_piezas)+32) - (m_separ_piezas * (Convert.ToInt32(DesplazamientoValido.y) + 7-PosicionFicha_Y) + 17);            
            }
            
        }
    }
}
