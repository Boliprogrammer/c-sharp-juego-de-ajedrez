﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chess2
{
    class ListaDoblementeEncadenadaNodo<T>
    {
        private Link<T> first;

        private Link<T> last;
        
        public ListaDoblementeEncadenadaNodo()
        {
            first = null;
            last = null;
            
        }

        public bool EstaVacio()
        {
            return first == null;
        }

        public void InsertarPrimero(T dd)
        {
            Link<T> newLink = new Link<T>(dd);
            if (EstaVacio())
                last = newLink;
            else
                first.previous = newLink;
            newLink.next = first;
            first = newLink;
        }

        public void InsertarUltimo(T dd)
        {
            Link<T> newLink = new Link<T>(dd);
            if (EstaVacio())
                first = newLink;
            else
            {
                last.next = newLink;
                newLink.previous = last;
            }
            last = newLink;
        }

        public Link<T> EliminarPrimero()
        {
            Link<T> temp = first;
            if (first.next == null)
                last = null;
            else
                first.next.previous = null;
            first = first.next;
            return temp;
        }

        public Link<T> EliminarUltimo()
        {
            Link<T> temp = last;
            if (first.next == null)
                first = null;
            else
                last.previous.next = null;
            last = last.previous;
            return temp;
        }
        /*
        public bool InsertarDespuesDe(T clave, T dd)
        {
            Link<T> current = first;
            while (current.dData != clave)
            {
                current = current.next;
                if (current == null)
                    return false; // cannot find it
            }
            Link<T> newLink = new Link<T>(dd); // make new link

            if (current == last) // if last link,
            {
                newLink.next = null;
                last = newLink;
            }
            else // not last link,
            {
                newLink.next = current.next;
                current.next.previous = newLink;
            }
            newLink.previous = current;
            current.next = newLink;
            return true; // found it, insert
        }
        */
        /*
        public Link<T> EliminarClave(T Clave)
        {
            Link<T> current = first;
            while (current.dData != Clave)
            {
                current = current.next;
                if (current == null)
                    return null; // cannot find it
            }

            if (current == first) // found it; first item?
                first = current.next;
            else
                current.previous.next = current.next;

            if (current == last) // last item?
                last = current.previous;
            else
                // not last
                current.next.previous = current.previous;
            return current; // return value
        }
        */
        /*
        public void displayForward() 
        {
            System.out.print("List (first to last): ");
            Link current = first; // start at beginning
            while (current != null) // until end of list,
            {
                current.displayLink();
                current = current.next; // move to next link
            }
            System.out.println("");
        }

        public void displayBackward() 
        {
            System.out.print("List : ");
            Link current = last;
            while (current != null)
            {
                current.displayLink();
                current = current.previous;
            }
            System.out.println("");
        }
         */

        
    }
    class Link<T>
    {
        public T dData; // data item

        public Link<T> next; // next link in list

        public Link<T> previous; // previous link in list

        public Link(T d)
        {
            dData = d;
        }

    }
}
