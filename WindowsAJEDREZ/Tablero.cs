﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    public class Tablero : Funciones
    {
        #region Variables Privadas
        byte[] m_Tablero;
        public byte m_Ahogado_Tablas_Jaque_mate; // 1 = Ahogado; 2 = Tablas; 3 = Jaque; 4 = Jaque_Mate;
        public Evaluador m_Evaluador;

        #endregion Variables Privadas

        #region Constructor
        public Tablero()
        {
            m_Tablero = new byte[32];
            m_Evaluador = new Evaluador();
            for (int i = 1; i <= 4; i++)
                m_Tablero[i - 1] = 238; //PN PN
            for (int i = 5; i <= 8; i++)
                m_Tablero[i - 1] = 255; //PB PB               
            m_Tablero[9 - 1] = 68; // 2TN
            m_Tablero[10 - 1] = 34; // 2CN                           
            m_Tablero[11 - 1] = 102; // 2AN                           
            m_Tablero[12 - 1] = 85; // 2TB                           
            m_Tablero[13 - 1] = 51; // 2CB                           
            m_Tablero[14 - 1] = 119; // 2AB                           
            m_Tablero[15 - 1] = 138; // QKN                           
            m_Tablero[16 - 1] = 155; // QKB                                       

            byte[,] tab = Obtener_Tablero_64_Casillas(this);
        }
        #endregion Constructor

        #region Métodos

        #region SET GET Cod Ficha

        public bool SET_CodFicha(byte NRO_Casilla_64, byte cod)
        {
            bool inserto = false;
            if (NRO_Casilla_64 % 2 == 1) // DERECHA DEL BYTE
                inserto = SET_CodFicha_DER_Byte(NRO_Casilla_64, cod);
            else
                inserto = SET_CodFicha_IZQ_Byte(NRO_Casilla_64, cod);
            return inserto;
        }

        public bool SET_CodFicha(byte x, byte y, byte cod)
        {

            byte NRO_Casilla = Convertir_PosXY_NROcasilla_64(x, y);
            bool inserto = SET_CodFicha(NRO_Casilla, cod);
            return inserto;
        }

        public byte GET_CodFicha(byte nro)
        {
            if (nro % 2 == 1)
                return GET_CodFicha_DER_Byte(nro);
            else
                return GET_CodFicha_IZQ_Byte(nro);
        }

        public byte GET_CodFicha(byte x, byte y)
        {

            byte NRO_Casilla_64 = Convertir_PosXY_NROcasilla_64(x, y);
            return GET_CodFicha(NRO_Casilla_64);

        }

        private bool SET_CodFicha_DER_Byte(byte NRO_Casilla_64_, byte cod)
        {

            byte UN_BYTE = m_Tablero[NRO_Casilla_64_ / 2];
            byte cod_4_der = InvertirCod_DER_4_Bits(UN_BYTE);
            cod_4_der = InvertirCod_DER_4_Bits(cod_4_der);

            if (!Valida_Sean_Distinto_Color(cod, cod_4_der) || (!VerificaNO_Sea_REY(cod_4_der)&& cod != 0))
                return false;
            else
            {
                byte cod_invertido = InvertirCod_DER_4_Bits(cod);
                UN_BYTE = (byte)(UN_BYTE >> 4);
                UN_BYTE = Insertar_DER_con_COD_invertido(UN_BYTE, cod_invertido);
                m_Tablero[NRO_Casilla_64_ / 2] = UN_BYTE;
                return true;
            }
        }

        private bool SET_CodFicha_IZQ_Byte(byte NRO_Casilla_64, byte cod)
        {
            byte UN_BYTE = m_Tablero[NRO_Casilla_64 / 2];
            byte cod_IZQ = (byte)(UN_BYTE >> 4);

            if (!Valida_Sean_Distinto_Color(cod, cod_IZQ) || (!VerificaNO_Sea_REY(cod_IZQ) && cod != 0))
                return false;
            else
            {
                byte cod_DER_invertido = InvertirCod_DER_4_Bits(UN_BYTE);
                byte cod_IZQ_invertido = InvertirCod_DER_4_Bits(cod);
                UN_BYTE = (byte)(UN_BYTE >> 8);
                UN_BYTE = InvertirCod_DER_4_Bits(cod_IZQ_invertido);
                UN_BYTE = Insertar_DER_con_COD_invertido(UN_BYTE, cod_DER_invertido);
                m_Tablero[NRO_Casilla_64 / 2] = UN_BYTE;
                return true;
            }
        }

        private byte GET_CodFicha_IZQ_Byte(byte nro)
        {
            byte cod = m_Tablero[nro / 2];
            return (byte)(cod >> 4);
        }

        private byte GET_CodFicha_DER_Byte(byte nro)
        {
            byte cod = m_Tablero[nro / 2];
            byte new_cod = 0;
            //OBTENEMOS LA CODIFICACION VOLCADA
            new_cod = InvertirCod_DER_4_Bits(cod);
            cod = new_cod;
            new_cod = 0;
            //LA INVERTIMOS AHORA
            new_cod = InvertirCod_DER_4_Bits(cod);

            return new_cod;
        }

        #endregion SET GET Cod Ficha

        #region Verifica SI Hay Jaque En El Tablero

        public bool Verifica_SI_Hay_Jaque_En_El_Tablero(byte Cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            byte Piezas_A_Verificar_32 = 255;
            bool jaque = false;
            for (byte y = 0; y <= 7 && jaque == false; y++)
            {
                for (byte x = 0; x <= 7 && jaque == false; x++)
                {
                    Piezas_A_Verificar_32 = this.GET_CodFicha(x, y);
                    if (Obtener_Color_Pieza(Cod) == Obtener_Color_Pieza(Piezas_A_Verificar_32))
                    {
                        if (Piezas_A_Verificar_32 != 0)
                        {
                            if (Piezas_A_Verificar_32 == cod_piezas.TN() || Piezas_A_Verificar_32 == cod_piezas.TB())
                                jaque = Verifica_SI_Torre_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.CN() || Piezas_A_Verificar_32 == cod_piezas.CB())
                                jaque = Verifica_SI_Caballo_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.AN() || Piezas_A_Verificar_32 == cod_piezas.AB())
                                jaque = Verifica_SI_Alfil_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.QN() || Piezas_A_Verificar_32 == cod_piezas.QB())
                                jaque = Verifica_SI_Reina_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.PN() || Piezas_A_Verificar_32 == cod_piezas.PB())
                                jaque = Verifica_SI_Peon_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.KN() || Piezas_A_Verificar_32 == cod_piezas.KB())
                                jaque = Verifica_SI_Rey_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                        }

                    }
                }
            }

            return jaque;

        }

        public bool Verifica_SI_Hay_Jaque_En_El_Tablero__Distinto_Color(byte Cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            byte Piezas_A_Verificar_32 = 255;
            bool jaque = false;
            for (byte y = 0; y <= 7 && jaque == false; y++)
            {
                for (byte x = 0; x <= 7 && jaque == false; x++)
                {
                    Piezas_A_Verificar_32 = this.GET_CodFicha(x, y);
                    if (Piezas_A_Verificar_32 != 0)
                    {
                        if (Obtener_Color_Pieza(Cod) != Obtener_Color_Pieza(Piezas_A_Verificar_32))
                        {
                            if (Piezas_A_Verificar_32 == cod_piezas.TN() || Piezas_A_Verificar_32 == cod_piezas.TB())
                                jaque = Verifica_SI_Torre_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.CN() || Piezas_A_Verificar_32 == cod_piezas.CB())
                                jaque = Verifica_SI_Caballo_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.AN() || Piezas_A_Verificar_32 == cod_piezas.AB())
                                jaque = Verifica_SI_Alfil_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.QN() || Piezas_A_Verificar_32 == cod_piezas.QB())
                                jaque = Verifica_SI_Reina_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.PN() || Piezas_A_Verificar_32 == cod_piezas.PB())
                                jaque = Verifica_SI_Peon_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                            else if (Piezas_A_Verificar_32 == cod_piezas.KN() || Piezas_A_Verificar_32 == cod_piezas.KB())
                                jaque = Verifica_SI_Rey_Hace_Jaque(x, y, Piezas_A_Verificar_32);
                        }

                    }
                }
            }

            return jaque;

        }

        private bool Verifica_SI_Torre_Hace_Jaque(byte x, byte y, byte cod)
        {

            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            bool libre = true;

            //HORIZONTAL DER
            for (sbyte i = (sbyte)(x + 1); i <= 7 && jaque == false && libre == true; i++)
            {
                if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.KN())
                        jaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.KB())
                        jaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
            }

            libre = true;
            //HORIZONTAL IZQ
            for (sbyte i = (sbyte)(x - 1); i >= 0 && jaque == false && libre == true; i--)
            {
                if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.KN())
                        jaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.KB())
                        jaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
            }

            libre = true;
            //VERTICAL DOWN
            for (sbyte i = (sbyte)(y + 1); i <= 7 && jaque == false && libre == true; i++)
            {
                if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KN())
                        jaque = true;
                    else if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KB())
                        jaque = true;
                    else if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
            }

            libre = true;
            //VERTICAL UP
            for (sbyte i = (sbyte)(y - 1); i >= 0 && jaque == false && libre == true; i--)
            {
                if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KN())
                        jaque = true;
                    else if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KB())
                        jaque = true;
                    else if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
            }

            return jaque;
        }

        private bool Verifica_SI_Caballo_Hace_Jaque(byte x, byte y, byte cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;

            //HORIZONTAL DER VERTICAL Y HORIZONTAL IZQ VERTICAL
            for (sbyte z = -1; z <= 1; z = (sbyte)(z + 2))
            {
                byte dos_o_cuatro = 4;
                for (sbyte i = 1; i <= 2 && jaque == false; i++)
                {
                    for (sbyte j = (sbyte)(-(dos_o_cuatro / 2)); j <= dos_o_cuatro / 2 && jaque == false ; 
                            j = (sbyte)(j + dos_o_cuatro))
                    {
                        if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                        {
                            if ((x + (i * z)) <= 7 && (x + (i * z)) >= 0 && (y + j) <= 7 && (y + j) >= 0)
                                if (this.GET_CodFicha((byte)(x + (i * z)), (byte)(y + j)) == cod_piezas.KN())
                                    jaque = true;
                        }
                        else
                        {
                            if ((x + (i * z)) <= 7 && (x + (i * z)) >= 0 && (y + j) <= 7 && (y + j) >= 0)
                                if (this.GET_CodFicha((byte)(x + (i * z)), (byte)(y + j)) == cod_piezas.KB())
                                    jaque = true;
                        }
                    }
                    dos_o_cuatro = (byte)(dos_o_cuatro / 2);
                }
            }
            return jaque;
        }

        private bool Verifica_SI_Alfil_Hace_Jaque(byte x, byte y, byte cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            bool libre = true;

            //IZQ UP Y DER DOWN
            for (sbyte z = -1; z <= 1 && jaque == false; z = (sbyte)(z + 2))
            {
                byte veces = x;
                if (z == 1) veces = (byte)(7 - x);
                libre = true;
                for (sbyte i = z; i <= veces && jaque == false && libre == true && (x + i) >= 0 && (x + i) <= 7 && (y + i) >= 0 && (y + i) <= 7; i = (sbyte)(i + (1 * z)))
                {
                    if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                    {
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.KN())
                            jaque = true;
                        else if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != 0)
                            libre = false;
                    }
                    else
                    {
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.KB())
                            jaque = true;
                        else if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != 0)
                            libre = false;
                    }
                }
                veces = (byte)(7 - x);
            }

            //DER UP Y IZQ DOWN
            for (sbyte z = -1; z <= 1 && jaque == false; z = (sbyte)(z + 2))
            {
                sbyte i = (sbyte)(x - (1 * z));
                libre = true;
                for (sbyte j = (sbyte)(y + (1 * z)); i >= 0 && i <= 7 && j <= 7 && j >= 0 && jaque == false && libre == true; j = (sbyte)(j + (1 * z)), i = (sbyte)(i - (1 * z)))
                {

                    if (Obtener_Color_Pieza(cod) == 1) //BLANCO
                    {
                        if (this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.KN())
                            jaque = true;
                        else if (this.GET_CodFicha((byte)i, (byte)j) != 0)
                            libre = false;
                    }
                    else
                    {
                        if (this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.KB())
                            jaque = true;
                        else if (this.GET_CodFicha((byte)i, (byte)j) != 0)
                            libre = false;
                    }

                }
            }
            return jaque;

        }

        private bool Verifica_SI_Reina_Hace_Jaque(byte x, byte y, byte cod)
        {
            bool jaque = false;
            jaque = Verifica_SI_Torre_Hace_Jaque(x, y, cod);
            if (jaque == false)
                jaque = Verifica_SI_Alfil_Hace_Jaque(x, y, cod);
            return jaque;
        }

        private bool Verifica_SI_Rey_Hace_Jaque(byte x, byte y, byte cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            for (sbyte i = -1; i <= 1 && jaque == false; i++)
            {
                for (sbyte j = -1; j <= 1 && jaque == false ; j++)
                {
                    if (Obtener_Color_Pieza(cod) == 1) //BLANCO 
                    {
                        if((x + i) >= 0 && (x + i) <= 7 && (y + j) >= 0 && (y + j) <= 7)
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + j)) == cod_piezas.KN())
                                jaque = true;
                    }
                    else
                    {
                        if ((x + i) >= 0 && (x + i) <= 7 && (y + j) >= 0 && (y + j) <= 7)
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + j)) == cod_piezas.KB())
                                jaque = true;
                    }

                }
            }
            return jaque;
        }

        private bool Verifica_SI_Peon_Hace_Jaque(byte x, byte y, byte cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            if (Obtener_Color_Pieza(cod) == 1) //BLANCO
            {
                for (sbyte i = -1; i <= 1 && jaque == false && x + i >= 0 && x + i <= 7 && y - 1 >= 0; i = (sbyte)(i + 2))
                {
                    if (this.GET_CodFicha((byte)(x + i), (byte)(y - 1)) == cod_piezas.KN())
                        jaque = true;
                }
            }
            else
            {
                for (sbyte i = -1; i <= 1 && jaque == false && x + i >= 0 && x + i <= 7 && y + 1 <= 7; i = (sbyte)(i + 2))
                {
                    if (this.GET_CodFicha((byte)(x + i), (byte)(y + 1)) == cod_piezas.KB())
                        jaque = true;
                }
            }
            return jaque;
        }
        
        #endregion Verifica SI Hay Jaque En El Tablero


        #region Verifica Jaque Mate En Tablero

        public bool Verifica_SI_Hay_Jaque_Mate_En_Tablero(byte Pieza_Que_Hace_Jaque_Cod)
        {
            bool jaque_mate = false;
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            for (int y = 0; y <= 7 && jaque_mate == false; y++)
            {
                for (int x = 0; x <= 7 && jaque_mate == false; x++)
                {
                    byte cod_ficha = this.GET_CodFicha((byte)x, (byte)y);
                    if (cod_ficha == cod_piezas.KN() || cod_ficha == cod_piezas.KB())
                    {
                        if (this.Valida_Sean_Distinto_Color(cod_ficha, Pieza_Que_Hace_Jaque_Cod))
                            jaque_mate = Verifica_SI_Hay_Jaque_Mate_En_REY((byte)x, (byte)y, Pieza_Que_Hace_Jaque_Cod);
                    }
                }

            }
            return jaque_mate;

        }

        private bool Verifica_SI_Hay_Jaque_Mate_En_REY(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {
            bool jaque_alrededor_rey = false;
            bool Hay_Ficha_DesJaque = true;
            byte cod_ficha_a_moverse = this.GET_CodFicha((byte)x, (byte)y);
            jaque_alrededor_rey = Verifica_Haya_Jaque_Alrededor_Rey(x, y, cod_ficha_a_moverse);

            if (jaque_alrededor_rey == true)
                Hay_Ficha_DesJaque = Verifica_SI_Hay_Ficha_Que_DesJaque(x, y, cod_ficha_a_moverse, Pieza_Que_Hace_Jaque_Cod);
            
            if (jaque_alrededor_rey == true && Hay_Ficha_DesJaque == false)
                return true;
            else
                return false;

        }

        private bool Verifica_Haya_Jaque_Alrededor_Rey(byte x, byte y, byte cod_ficha_a_moverse)
        {
            bool jaque_alrededor_rey = false;
            byte jaques_9 = 0;
            byte nro_casillas_a_verificar = 0;
            for (sbyte i = -1; i <= 1 ; i++)
            {
                for (sbyte j = -1; j <= 1; j++)
                {
                    if (x + i >= 0 && x + i <= 7 && y + j >= 0 && y + j <= 7)
                        if (this.VerificaNO_Sea_REY((byte)(x + i), (byte)(y + j)) == true && this.Valida_Sean_Distinto_Color(cod_ficha_a_moverse, this.GET_CodFicha((byte)(x + i), (byte)(y + j))) == true)
                                nro_casillas_a_verificar++;
                }
            }

            //bool pieza_mueve_mismo_color_hace_jaque = false;
            for (sbyte i = -1; i <= 1 ; i++)
            {
                for (sbyte j = -1; j <= 1 ; j++)
                {
                    if (x + i >= 0 && x + i <= 7 && y + j >= 0 && y + j <= 7 )
                    {
                        if (this.VerificaNO_Sea_REY((byte)(x + i), (byte)(y + j)) == true && this.Valida_Sean_Distinto_Color(cod_ficha_a_moverse, this.GET_CodFicha((byte)(x + i), (byte)(y + j))) == true)
                        {
                            Movimiento CXYIF = new Movimiento();
                            CXYIF.X_Start = (sbyte)x;
                            CXYIF.X_End = (sbyte)(x + i);
                            CXYIF.Y_Start = (sbyte)y;
                            CXYIF.Y_End = (sbyte)(y + j);
                            if (Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(this, CXYIF, cod_ficha_a_moverse) == true 
                                && this.Valida_Sean_Distinto_Color(this.GET_CodFicha(x, y), this.GET_CodFicha((byte)(x + i), 
                                (byte)(y + j))) == true)
                                jaques_9++;
                        }
                    }
                }
            }

            if (jaques_9 == nro_casillas_a_verificar)
                jaque_alrededor_rey = true;

            return jaque_alrededor_rey;
        }

        private bool Verifica_SI_Hay_Ficha_Que_DesJaque(byte x, byte y, byte cod_ficha_a_moverse, byte Pieza_Que_Hace_Jaque_Cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            bool Hay_Ficha_Que_DesJaque = false;
            for (sbyte j = 0; j <= 7 && jaque == false; j++)
            {
                for (sbyte i = 0; i <= 7 && jaque == false; i++)
                {
                    //BUSCA LA POS X Y DE LA PIEZA QUE HIZO JAQUE
                    if (this.GET_CodFicha((byte)(i), (byte)(j)) == Pieza_Que_Hace_Jaque_Cod)
                    {
                        if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.TN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.TB())
                            jaque = Verifica_SI_Torre_Hace_Jaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                        else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.AN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.AB())
                            jaque = Verifica_SI_Alfil_Hace_Jaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                        else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.QN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.QB())
                            jaque = Verifica_SI_Reina_Hace_Jaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                        else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.CN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.CB())
                            jaque = Verifica_SI_Caballo_Hace_Jaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                        else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.PN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.PB())
                            jaque = Verifica_SI_Peon_Hace_Jaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                        else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.KN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.KB()) //////////////
                            jaque = Verifica_SI_Rey_Hace_Jaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);

                        if (jaque == true)
                        {
                            if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.TN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.TB())
                                Hay_Ficha_Que_DesJaque = Verifica_SI_En_Jaque_De_TORRE_Hay_DesJaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                            else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.AN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.AB())
                                Hay_Ficha_Que_DesJaque = Verifica_SI_En_Jaque_De_ALFIL_Hay_DesJaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                            else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.QN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.QB())
                                Hay_Ficha_Que_DesJaque = Verifica_SI_En_Jaque_De_REINA_Hay_DesJaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                            else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.CN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.CB())
                                Hay_Ficha_Que_DesJaque = Verifica_SI_En_Jaque_De_CABALLO_Hay_DesJaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                            else if (Pieza_Que_Hace_Jaque_Cod == cod_piezas.PN() || Pieza_Que_Hace_Jaque_Cod == cod_piezas.PB())
                                Hay_Ficha_Que_DesJaque = Verifica_SI_En_Jaque_De_PEON_Hay_DesJaque((byte)i, (byte)j, Pieza_Que_Hace_Jaque_Cod);
                        }
                    }
                }
            }

            return Hay_Ficha_Que_DesJaque;
        }

        private bool Verifica_SI_Alguna_Pieza_DesJaque(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool Hay_Pieza_DesJaque = false;
            bool libre = true;

            //HORIZONTAL DER
            for (sbyte i = (sbyte)(x + 1); i <= 7 && Hay_Pieza_DesJaque == false && libre == true; i++)
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.TN() || this.GET_CodFicha((byte)i, y) == cod_piezas.QN())
                        Hay_Pieza_DesJaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.TB() || this.GET_CodFicha((byte)i, y) == cod_piezas.QB())
                        Hay_Pieza_DesJaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
            }

            libre = true;
            //HORIZONTAL IZQ
            for (sbyte i = (sbyte)(x - 1); i >= 0 && Hay_Pieza_DesJaque == false && libre == true; i--)
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.TN() || this.GET_CodFicha((byte)i, y) == cod_piezas.QN())
                        Hay_Pieza_DesJaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha((byte)i, y) == cod_piezas.TB() || this.GET_CodFicha((byte)i, y) == cod_piezas.QB())
                        Hay_Pieza_DesJaque = true;
                    else if (this.GET_CodFicha((byte)i, y) != 0)
                        libre = false;
                }
            }

            libre = true;
            //VERTICAL DOWN
            for (sbyte i = (sbyte)(y + 1); i <= 7 && Hay_Pieza_DesJaque == false && libre == true; i++)
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.TN() || this.GET_CodFicha(x, (byte)i) == cod_piezas.QN())
                        Hay_Pieza_DesJaque = true;
                    else if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.TB() || this.GET_CodFicha(x, (byte)i) == cod_piezas.QB())
                        Hay_Pieza_DesJaque = true;
                    else if (y + 1 <= 7)
                    {
                        if (this.GET_CodFicha((byte)(x + 1), (byte)(y + 1)) == cod_piezas.PB() || this.GET_CodFicha((byte)(x - 1), (byte)(y + 1)) == cod_piezas.PB())
                            Hay_Pieza_DesJaque = true;
                    }
                    if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
            }

            libre = true;
            //VERTICAL UP
            for (sbyte i = (sbyte)(y - 1); i >= 0 && Hay_Pieza_DesJaque == false && libre == true; i--)
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.TN() || this.GET_CodFicha(x, (byte)i) == cod_piezas.QN())
                        Hay_Pieza_DesJaque = true;
                    else if (y - 1 >= 0)
                    {
                        if (this.GET_CodFicha((byte)(x + 1), (byte)(y - 1)) == cod_piezas.PN() || this.GET_CodFicha((byte)(x - 1), (byte)(y - 1)) == cod_piezas.PN())
                            Hay_Pieza_DesJaque = true;
                    }
                    if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
                else
                {
                    if (this.GET_CodFicha(x, (byte)i) == cod_piezas.TB() || this.GET_CodFicha(x, (byte)i) == cod_piezas.QB())
                        Hay_Pieza_DesJaque = true;
                    else if (this.GET_CodFicha(x, (byte)i) != 0)
                        libre = false;
                }
            }

            //IZQ UP Y DER DOWN
            for (sbyte z = -1; z <= 1 && Hay_Pieza_DesJaque == false; z = (sbyte)(z + 2))
            {
                byte veces = x;
                if (z == 1) veces = (byte)(7 - x);
                libre = true;
                for (sbyte i = z; i <= veces && Hay_Pieza_DesJaque == false && libre == true && (x + i) >= 0 && (x + i) <= 7 && (y + i) >= 0 && (y + i) <= 7; i = (sbyte)(i + (1 * z)))
                {
                    if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                    {
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.QN() || this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.AN())
                            Hay_Pieza_DesJaque = true;
                        else if (y + i - 1 >= 0)
                        {
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == 0)
                            {
                                if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.PN())
                                    Hay_Pieza_DesJaque = true;
                            }
                        }
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != 0)
                            libre = false;
                    }
                    else
                    {
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.QB() || this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.AB())
                            Hay_Pieza_DesJaque = true;
                        else if (this.GET_CodFicha((byte)(x + i), (byte)(y + i + 1)) == cod_piezas.PN())
                            Hay_Pieza_DesJaque = true;
                        else if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != 0)
                            libre = false;
                    }
                }
                veces = (byte)(7 - x);
            }

            //DER UP Y IZQ DOWN
            for (sbyte z = -1; z <= 1 && Hay_Pieza_DesJaque == false; z = (sbyte)(z + 2))
            {
                sbyte i = (sbyte)(x - (1 * z));
                libre = true;
                for (sbyte j = (sbyte)(y + (1 * z)); i >= 0 && i <= 7 && j <= 7 && j >= 0 && Hay_Pieza_DesJaque == false && libre == true; j = (sbyte)(j + (1 * z)), i = (sbyte)(i - (1 * z)))
                {

                    if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                    {
                        if (this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.QN() || this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.AN())
                            Hay_Pieza_DesJaque = true;
                        else if (this.GET_CodFicha((byte)i, (byte)j) != 0)
                            libre = false;
                    }
                    else
                    {
                        if (this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.QB() || this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.AB())
                            Hay_Pieza_DesJaque = true;
                        else if (j + 1 <= 7)
                        {
                            if(this.GET_CodFicha((byte)i, (byte)(j)) == 0)
                                if (this.GET_CodFicha((byte)i, (byte)(j + 1)) == cod_piezas.PB())
                                    Hay_Pieza_DesJaque = true;
                        }
                        if (this.GET_CodFicha((byte)i, (byte)j) != 0)
                            libre = false;
                    }

                }
            }

            //HORIZONTAL DER VERTICAL Y HORIZONTAL IZQ VERTICAL
            for (sbyte z = -1; z <= 1; z = (sbyte)(z + 2))
            {
                byte dos_o_cuatro = 4;
                for (sbyte i = 1; i <= 2 && Hay_Pieza_DesJaque == false; i++)
                {
                    for (sbyte j = (sbyte)(-(dos_o_cuatro / 2)); j <= dos_o_cuatro / 2 && Hay_Pieza_DesJaque == false ; j = (sbyte)(j + dos_o_cuatro))
                    {
                        if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                        {
                            if ((x + (i * z)) <= 7 && (x + (i * z)) >= 0 && (y + j) <= 7 && (y + j) >= 0)
                            {
                                if (this.GET_CodFicha((byte)(x + (i * z)), (byte)(y + j)) == cod_piezas.CN())
                                    Hay_Pieza_DesJaque = true;
                            }
                        }
                        else
                        {
                            if ((x + (i * z)) <= 7 && (x + (i * z)) >= 0 && (y + j) <= 7 && (y + j) >= 0)
                            {
                                if (this.GET_CodFicha((byte)(x + (i * z)), (byte)(y + j)) == cod_piezas.CB())
                                    Hay_Pieza_DesJaque = true;
                            }
                        }
                    }
                    dos_o_cuatro = (byte)(dos_o_cuatro / 2);
                }
            }
            return Hay_Pieza_DesJaque;
        }

        private bool Verifica_SI_En_Jaque_De_TORRE_Hay_DesJaque(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {

            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            bool libre = true;
            sbyte signo = 1;
            bool Hay_DesJaque = false;


            //HORIZONTAL DER
            for (sbyte i = (sbyte)(x + 1); i <= 7 && i >= 0 && Hay_DesJaque == false && libre == true; i = (sbyte)(i + (signo * 1)))
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha((byte)i, y) != cod_piezas.KN())
                    {
                        if (this.GET_CodFicha((byte)i, y) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(i), (byte)y, Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha((byte)i, y) == cod_piezas.KN())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
                else
                {
                    if (this.GET_CodFicha((byte)i, y) != cod_piezas.KB())
                    {
                        if (this.GET_CodFicha((byte)i, y) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(i), (byte)y, Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {

                        if (this.GET_CodFicha((byte)i, y) == cod_piezas.KB())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
            }

            libre = true;
            //HORIZONTAL IZQ
            for (sbyte i = (sbyte)(x - 1); i >= 0 && i <= 7 && Hay_DesJaque == false && libre == true; i = (sbyte)(i - (signo * 1)))
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha((byte)i, y) != cod_piezas.KN())
                    {
                        if (this.GET_CodFicha((byte)i, y) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(i), (byte)y, Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha((byte)i, y) == cod_piezas.KN())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
                else
                {
                    if (this.GET_CodFicha((byte)i, y) != cod_piezas.KB())
                    {
                        if (this.GET_CodFicha((byte)i, y) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(i), (byte)y, Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha((byte)i, y) == cod_piezas.KB())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
            }

            libre = true;
            //VERTICAL DOWN
            for (sbyte i = (sbyte)(y + 1); i <= 7 && i >= 0 && Hay_DesJaque == false && libre == true; i = (sbyte)(i + (signo * 1)))
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha(x, (byte)i) != cod_piezas.KN())
                    {
                        if (this.GET_CodFicha(x, (byte)i) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)x, (byte)(i), Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KN())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
                else
                {
                    if (this.GET_CodFicha(x, (byte)i) != cod_piezas.KB())
                    {
                        if (this.GET_CodFicha(x, (byte)i) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)x, (byte)(i), Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KB())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
            }

            libre = true;
            //VERTICAL UP
            for (sbyte i = (sbyte)(y - 1); i >= 0 && i <= 7 && Hay_DesJaque == false && libre == true; i = (sbyte)(i - (signo * 1)))
            {
                if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                {
                    if (this.GET_CodFicha(x, (byte)i) != cod_piezas.KN())
                    {
                        if (this.GET_CodFicha(x, (byte)i) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)x, (byte)(i), Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KN())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }

                }
                else
                {
                    if (this.GET_CodFicha(x, (byte)i) != cod_piezas.KB())
                    {
                        if (this.GET_CodFicha(x, (byte)i) != 0)
                            libre = false;
                    }
                    if (jaque == true )
                    {
                        Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)x, (byte)(i), Pieza_Que_Hace_Jaque_Cod);
                    }
                    else if (jaque == false)
                    {
                        if (this.GET_CodFicha(x, (byte)i) == cod_piezas.KB())
                        {
                            signo = -1;
                            jaque = true;
                        }
                    }
                    
                }
            }

            return Hay_DesJaque;
        }

        private bool Verifica_SI_En_Jaque_De_ALFIL_Hay_DesJaque(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            bool jaque = false;
            bool libre = true;
            bool Hay_DesJaque = false;
            sbyte signo2 = 1;

            //IZQ UP Y DER DOWN
            for (sbyte z = -1; z <= 1 && Hay_DesJaque == false && jaque == false; z = (sbyte)(z + 2))
            {
                byte veces = x;
                if (z == 1) veces = (byte)(7 - x);
                libre = true;
                sbyte signo = 1;
                for (sbyte i = z; i <= veces && Hay_DesJaque == false && libre == true && (x + i) >= 0 && (x + i) <= 7 && (y + i) >= 0 && (y + i) <= 7; i = (sbyte)(i + (signo * signo2 * (1 * z))))
                {
                    if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                    {
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != cod_piezas.KN())
                        {
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != 0)
                                libre = false;
                        }
                        if (jaque == true)
                        {
                                Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(x + i), (byte)(y + i), Pieza_Que_Hace_Jaque_Cod);
                        }
                        else if (jaque == false)
                        {
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.KN())
                            {
                                signo2 = -1;
                                jaque = true;
                            }
                        }

                    }
                    else
                    {
                        if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != cod_piezas.KB())
                        {
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) != 0)
                                libre = false;
                        }
                        if (jaque == true)
                        {
                            Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(x + i ), (byte)(y + i ), Pieza_Que_Hace_Jaque_Cod);
                        }
                        else if (jaque == false)
                        {
                            if (this.GET_CodFicha((byte)(x + i), (byte)(y + i)) == cod_piezas.KB())
                            {
                                signo2 = -1;
                                jaque = true;
                            }
                        }

                    }
                }
                veces = (byte)(7 - x);
            }

            //DER UP Y IZQ DOWN
            for (sbyte z = -1; z <= 1 && Hay_DesJaque == false && jaque == false; z = (sbyte)(z + 2))
            {
                sbyte i = (sbyte)(x - (1 * z));
                libre = true;
                sbyte signo = 1;
                for (sbyte j = (sbyte)(y + (1 * z)); i >= 0 && i <= 7 && j <= 7 && j >= 0 && Hay_DesJaque == false && libre == true; j = (sbyte)(j + (signo *signo2 * (1 * z))), i = (sbyte)(i - (signo * signo2 * (1 * z))))
                {

                    if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque_Cod) == 1) //BLANCO
                    {
                        if (this.GET_CodFicha((byte)(i), (byte)(j)) != cod_piezas.KN())
                        {
                            if (this.GET_CodFicha((byte)i, (byte)j) != 0)
                                libre = false;
                        }
                        if (jaque == true)
                        {
                            Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(i), (byte)(j), Pieza_Que_Hace_Jaque_Cod);
                        }
                        else if (jaque == false)
                        {
                            if (this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.KN())
                            {
                                signo2 = -1;
                                jaque = true;
                            }
                        }

                    }
                    else
                    {
                        if (this.GET_CodFicha((byte)(i), (byte)(j)) != cod_piezas.KB())
                        {
                            if (this.GET_CodFicha((byte)i, (byte)j) != 0)
                                libre = false;
                        }
                        if (jaque == true)
                        {
                            Hay_DesJaque = Verifica_SI_Alguna_Pieza_DesJaque((byte)(i), (byte)(j), Pieza_Que_Hace_Jaque_Cod);
                        }
                        else if (jaque == false)
                        {
                            if (this.GET_CodFicha((byte)i, (byte)j) == cod_piezas.KB())
                            {
                                signo2 = -1;
                                jaque = true;
                            }
                        }

                    }

                }
            }
            return Hay_DesJaque;
        }

        private bool Verifica_SI_En_Jaque_De_REINA_Hay_DesJaque(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {
            bool Hay_DesJaque = false;
            Hay_DesJaque = Verifica_SI_En_Jaque_De_TORRE_Hay_DesJaque(x, y, Pieza_Que_Hace_Jaque_Cod);
            if (Hay_DesJaque == false)
                Hay_DesJaque = Verifica_SI_En_Jaque_De_ALFIL_Hay_DesJaque(x, y, Pieza_Que_Hace_Jaque_Cod);
            return Hay_DesJaque;
        }

        private bool Verifica_SI_En_Jaque_De_CABALLO_Hay_DesJaque(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {
            return Verifica_SI_Alguna_Pieza_DesJaque(x, y, Pieza_Que_Hace_Jaque_Cod);
        }

        private bool Verifica_SI_En_Jaque_De_PEON_Hay_DesJaque(byte x, byte y, byte Pieza_Que_Hace_Jaque_Cod)
        {
            return Verifica_SI_Alguna_Pieza_DesJaque(x, y, Pieza_Que_Hace_Jaque_Cod);
        }

        #endregion Verifica Jaque Mate En Tablero

        #region Funciones Otras

        int Obtener_Valor_Por_Atacar(byte Cod)
        {
            return 0;
        }

        public bool Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(Tablero TTablero, Movimiento CXYIF, byte cod_4)
        {
            //byte Pieza_Que_Hace_Jaque = 0;
            //pieza_mueve_mismo_color_hace_jaque = false;
            bool jaque = false;
            byte Ficha_Q_hAY_En_Pos_A_Moverse = TTablero.GET_CodFicha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End));

            TTablero.SET_CodFicha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End), cod_4);
            TTablero.SET_CodFicha((byte)(CXYIF.X_Start), (byte)(CXYIF.Y_Start), 0);

            jaque = TTablero.Verifica_SI_Hay_Jaque_En_El_Tablero__Distinto_Color(cod_4);

            //if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque) == Obtener_Color_Pieza(cod_4)) pieza_mueve_mismo_color_hace_jaque = true;

            //byte [,]tab2 = Obtener_Tablero_64_Casillas(TTablero);

            TTablero.SET_CodFicha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End), 0);
            TTablero.SET_CodFicha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End), Ficha_Q_hAY_En_Pos_A_Moverse);
            TTablero.SET_CodFicha((byte)(CXYIF.X_Start), (byte)(CXYIF.Y_Start), cod_4);

            //byte [,]tab3 = Obtener_Tablero_64_Casillas(TTablero);

            return jaque;
        }
        
        public bool EsCasillaVacia(byte x, byte y)
        {
            
            byte cod = GET_CodFicha(x, y);
            if (cod == 0)
                return true;
            else
                return false;
        }

        private byte InvertirCod_DER_4_Bits(byte COD_a_Invertir)
        {
            byte new_cod = 0;
            for (int i = 0; i < 4; i++)
            {
                new_cod = (byte)(new_cod | (byte)(COD_a_Invertir & 1));
                COD_a_Invertir = (byte)(COD_a_Invertir >> 1);
                if (i != 3)
                    new_cod = (byte)(new_cod << 1);
            }
            return new_cod;
        }

        private byte Insertar_DER_con_COD_invertido(byte UN_BYTE, byte cod_invertido)
        {
            for (byte i = 0; i < 4; i++)
            {
                UN_BYTE = (byte)(UN_BYTE << 1);
                UN_BYTE = (byte)(UN_BYTE | (byte)(cod_invertido & 1));
                cod_invertido = (byte)(cod_invertido >> 1);
            }
            return UN_BYTE;
        }

        public bool Valida_Sean_Distinto_Color(byte cod1, byte cod2)
        {
            if (cod1 == 0 || cod2 == 0)
                return true;
            else
            {
                cod1 = Obtener_Color_Pieza(cod1);
                cod2 = Obtener_Color_Pieza(cod2);
                if (cod1 != cod2) return true;
                else return false;
            }

        }

        public bool VerificaNO_Sea_REY(byte Cod)
        {
            CodificacionPiezas Cod_Piezas;
            if (Cod == Cod_Piezas.KB() || Cod == Cod_Piezas.KN())
                return false;
            else
                return true;

        }

        public bool VerificaNO_Sea_REY(byte x, byte y)
        {

            byte cod = GET_CodFicha(x, y);
            bool respuesta = VerificaNO_Sea_REY(cod);
            return respuesta;
        }

        public Tablero CopyTo()
        {
            Tablero copia = new Tablero();
            copia = (Tablero)this.MemberwiseClone();
            copia.m_Tablero = (byte[])this.m_Tablero.Clone();
            return copia;
        }

        public byte Ahogado_Tablas_Jaque_Mate { set { m_Ahogado_Tablas_Jaque_mate = value; } get { return m_Ahogado_Tablas_Jaque_mate; } }

        #endregion Funciones Otras

        #region Movimientos Posibles Y Validos

        public List<Movimiento> Obtener_Movimientos_Validos(Tablero TTablero, bool ES_Color_BLANCO, Torre TTorre)
        {
            List<Movimiento> Movimientos_Validos_List = new List<Movimiento>();
            List<Movimiento> Movimientos_Validos_List_PiezaX = new List<Movimiento>();
            for (byte Y_Start = 0; Y_Start <= 7; Y_Start++)
            {
                for (byte X_Start = 0; X_Start <= 7; X_Start++)
                {
                    //byte[,] tab4 = Obtener_Tablero_64_Casillas(this); 
                    if (TTablero.Obtener_Color_Pieza(TTablero.GET_CodFicha(X_Start, Y_Start)) ==
                        ((ES_Color_BLANCO) ? 1 : 0))
                    {
                        //byte[,] tab1 = Obtener_Tablero_64_Casillas(this); 
                        Movimientos_Validos_List_PiezaX =  Obtener_List_Movimientos_Validos_PIEZA_X(X_Start,
                            Y_Start, TTorre, TTablero);
                        //byte[,] tab3 = Obtener_Tablero_64_Casillas(this); 
                        for (byte i = 0; i < Movimientos_Validos_List_PiezaX.Count; i++)
                        {
                            Movimientos_Validos_List.Add(Movimientos_Validos_List_PiezaX[i]);
                        }
                        //byte[,] tab2 = Obtener_Tablero_64_Casillas(this); 

                    }
                }
            }
            return Movimientos_Validos_List;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_PIEZA_X(byte X_Start, byte Y_Start, Torre TTorre, Tablero TTablero)
        {
            byte Cod = this.GET_CodFicha(X_Start, Y_Start);
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            List<Movimiento> List_Movimientos_Validos = new List<Movimiento>();

            List<Movimiento> List_Movimientos_VALIDOS = new List<Movimiento>();

            if (Cod == cod_piezas.PN() || Cod == cod_piezas.PB())
            {
                Peon_struct TPeon_Struct = new Peon_struct();
                List_Movimientos_Validos = Obtener_List_Movimientos_Validos_PEON(TPeon_Struct.
                    GET_Movimientos_Posibles(X_Start, Y_Start, Cod), X_Start, Y_Start, Cod, TTablero);
            }
            else if (Cod == cod_piezas.TN() || Cod == cod_piezas.TB())
            {
                Torre_struct TTorre_Struct = new Torre_struct();
                List_Movimientos_Validos = Obtener_List_Movimientos_Validos_TORRE(TTorre_Struct.
                    GET_Movimientos_Posibles(X_Start, Y_Start, Cod), X_Start, Y_Start, Cod, TTablero);
            }
            else if (Cod == cod_piezas.CN() || Cod == cod_piezas.CB())
            {
                Caballo_struct TCaballo_Struct = new Caballo_struct();
                List_Movimientos_Validos = Obtener_List_Movimientos_Validos_CABALLO(TCaballo_Struct.
                    GET_Movimientos_Posibles(X_Start, Y_Start, Cod), X_Start, Y_Start, Cod, TTablero);
            }
            else if (Cod == cod_piezas.AN() || Cod == cod_piezas.AB())
            {
                Alfil_struct TAlfil_Struct = new Alfil_struct();
                List_Movimientos_Validos = Obtener_List_Movimientos_Validos_ALFIL(TAlfil_Struct.
                    GET_Movimientos_Posibles(X_Start, Y_Start, Cod), X_Start, Y_Start, Cod, TTablero);
            }
            else if (Cod == cod_piezas.QN() || Cod == cod_piezas.QB())
            {
                Reina_struct TReina_Struct = new Reina_struct();
                List_Movimientos_Validos = Obtener_List_Movimientos_Validos_REINA(TReina_Struct.
                    GET_Movimientos_Posibles(X_Start, Y_Start, Cod), X_Start, Y_Start, Cod, TTablero);
            }
            else if (Cod == cod_piezas.KN() || Cod == cod_piezas.KB())
            {
                Rey_struct TRey_Struct = new Rey_struct();
                List_Movimientos_Validos = Obtener_List_Movimientos_Validos_REY(TRey_Struct.
                    GET_Movimientos_Posibles(X_Start, Y_Start, Cod), X_Start, Y_Start, Cod, TTorre, TTablero);
            }
            return List_Movimientos_Validos;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_PEON(List<Movimiento_Posible>
            List_Posibles_Movimientos_Peon, byte X_Start, byte Y_Start, byte Cod, Tablero TTablero)
        {
            List<Movimiento> List_Movimientos_Validos_Peon = new List<Movimiento>();
            for (byte i = 0; i < List_Posibles_Movimientos_Peon.Count; i++)
            {
                Peon TPeon = new Peon();
                Movimiento CXYIF = new Movimiento();
                CXYIF.X_Start = (sbyte)X_Start;
                CXYIF.Y_Start = (sbyte)Y_Start;
                CXYIF.X_End = (sbyte)List_Posibles_Movimientos_Peon[i].X_End;
                CXYIF.Y_End = (sbyte)List_Posibles_Movimientos_Peon[i].Y_End;
                Tablero copia_Tablero = TTablero.CopyTo();
                Movimiento Es_Movimiento_Valido = TPeon.ValidaMovimiento(CXYIF, copia_Tablero, Cod);
                CXYIF.COMIO = Es_Movimiento_Valido.COMIO;
                CXYIF.MOVIO = Es_Movimiento_Valido.MOVIO;
                CXYIF.Cod = Cod;
                /*if (CXYIF.VALOR_TABLERO > MAX_VALOR)
                {
                    MEJOR_MOVIDA = CXYIF;
                    MAX_VALOR = MEJOR_MOVIDA.VALOR_TABLERO;
                }
                 */

                if (Es_Movimiento_Valido.MOVIO || Es_Movimiento_Valido.COMIO)
                {
                    CXYIF.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero);
                    List_Movimientos_Validos_Peon.Add(CXYIF);
                    
                }

            }

            return List_Movimientos_Validos_Peon;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_TORRE(List<Movimiento_Posible>
            List_Posibles_Movimientos_TORRE, byte X_Start, byte Y_Start, byte Cod, Tablero TTablero)
        {
            List<Movimiento> List_Movimientos_Validos_TORRE = new List<Movimiento>();

            for (byte i = 0; i < List_Posibles_Movimientos_TORRE.Count; i++)
            {
                Torre TTorre = new Torre();
                Movimiento CXYIF = new Movimiento();
                CXYIF.X_Start = (sbyte)X_Start;
                CXYIF.Y_Start = (sbyte)Y_Start;
                CXYIF.X_End = (sbyte)List_Posibles_Movimientos_TORRE[i].X_End;
                CXYIF.Y_End = (sbyte)List_Posibles_Movimientos_TORRE[i].Y_End;
                Tablero copia_Tablero = TTablero.CopyTo() ;
                Movimiento Es_Movimiento_Valido = TTorre.ValidaMovimiento(CXYIF, copia_Tablero, Cod);
                CXYIF.COMIO = Es_Movimiento_Valido.COMIO;
                CXYIF.MOVIO = Es_Movimiento_Valido.MOVIO;
                CXYIF.Cod = Cod;

                /*
                if (CXYIF.VALOR_TABLERO > MAX_VALOR)
                {
                    MEJOR_MOVIDA = CXYIF;
                    MAX_VALOR = MEJOR_MOVIDA.VALOR_TABLERO;
                }
                 */

                if (Es_Movimiento_Valido.MOVIO || Es_Movimiento_Valido.COMIO)
                {
                    CXYIF.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero);
                    List_Movimientos_Validos_TORRE.Add(CXYIF);
                }

            }

            return List_Movimientos_Validos_TORRE;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_CABALLO(List<Movimiento_Posible>
            List_Posibles_Movimientos_DEL_CABALLO, byte X_Start, byte Y_Start, byte Cod, Tablero TTablero)
        {
            List<Movimiento> List_Movimientos_Validos_Del_CABALLO = new List<Movimiento>();
            for (byte i = 0; i < List_Posibles_Movimientos_DEL_CABALLO.Count; i++)
            {
                Caballo TCaballo = new Caballo();
                Movimiento CXYIF = new Movimiento();
                CXYIF.X_Start = (sbyte)X_Start;
                CXYIF.Y_Start = (sbyte)Y_Start;
                CXYIF.X_End = (sbyte)List_Posibles_Movimientos_DEL_CABALLO[i].X_End;
                CXYIF.Y_End = (sbyte)List_Posibles_Movimientos_DEL_CABALLO[i].Y_End;
                Tablero copia_Tablero = TTablero.CopyTo();
                Movimiento Es_Movimiento_Valido = TCaballo.ValidaMovimiento(CXYIF, copia_Tablero, Cod);
                CXYIF.COMIO = Es_Movimiento_Valido.COMIO;
                CXYIF.MOVIO = Es_Movimiento_Valido.MOVIO;
                CXYIF.Cod = Cod;
                /*
                if (CXYIF.VALOR_TABLERO > MAX_VALOR)
                {
                    MEJOR_MOVIDA = CXYIF;
                    MAX_VALOR = MEJOR_MOVIDA.VALOR_TABLERO;
                }
                 */
                if (Es_Movimiento_Valido.MOVIO || Es_Movimiento_Valido.COMIO)
                {
                    CXYIF.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero);
                    List_Movimientos_Validos_Del_CABALLO.Add(CXYIF);
                    
                }
                 

            }

            return List_Movimientos_Validos_Del_CABALLO;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_ALFIL(List<Movimiento_Posible>
            List_Posibles_Movimientos_Del_ALFIL, byte X_Start, byte Y_Start, byte Cod, Tablero TTablero)
        {
            List<Movimiento> List_Movimientos_Validos_Del_ALFIL = new List<Movimiento>();

            for (byte i = 0; i < List_Posibles_Movimientos_Del_ALFIL.Count; i++)
            {
                Alfil TAlfil = new Alfil();
                Movimiento CXYIF = new Movimiento();
                CXYIF.X_Start = (sbyte)X_Start;
                CXYIF.Y_Start = (sbyte)Y_Start;
                CXYIF.X_End = (sbyte)List_Posibles_Movimientos_Del_ALFIL[i].X_End;
                CXYIF.Y_End = (sbyte)List_Posibles_Movimientos_Del_ALFIL[i].Y_End;
                Tablero copia_Tablero = TTablero.CopyTo();
                Movimiento Es_Movimiento_Valido = TAlfil.ValidaMovimiento(CXYIF, copia_Tablero, Cod);
                CXYIF.COMIO = Es_Movimiento_Valido.COMIO;
                CXYIF.MOVIO = Es_Movimiento_Valido.MOVIO;
                CXYIF.Cod = Cod;
                

                /*if (CXYIF.VALOR_TABLERO > MAX_VALOR)
                {
                    MEJOR_MOVIDA = CXYIF;
                    MAX_VALOR = MEJOR_MOVIDA.VALOR_TABLERO;
                }
                 */

                if (Es_Movimiento_Valido.MOVIO || Es_Movimiento_Valido.COMIO)
                {
                    CXYIF.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero);
                    List_Movimientos_Validos_Del_ALFIL.Add(CXYIF);
                }
            }
            return List_Movimientos_Validos_Del_ALFIL;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_REINA(List<Movimiento_Posible>
            List_Posibles_Movimientos_De_La_REINA, byte X_Start, byte Y_Start, byte Cod, Tablero TTablero)
        {

            List<Movimiento> List_Movimientos_Validos_De_La_REINA = new List<Movimiento>();
            for (byte i = 0; i < List_Posibles_Movimientos_De_La_REINA.Count; i++)
            {
                Reina TReina = new Reina();
                Movimiento CXYIF = new Movimiento();
                CXYIF.X_Start = (sbyte)X_Start;
                CXYIF.Y_Start = (sbyte)Y_Start;
                CXYIF.X_End = (sbyte)List_Posibles_Movimientos_De_La_REINA[i].X_End;
                CXYIF.Y_End = (sbyte)List_Posibles_Movimientos_De_La_REINA[i].Y_End;
                Tablero copia_Tablero = TTablero.CopyTo();
                Movimiento Es_Movimiento_Valido = TReina.ValidaMovimiento(CXYIF, copia_Tablero, Cod);
                CXYIF.COMIO = Es_Movimiento_Valido.COMIO;
                CXYIF.MOVIO = Es_Movimiento_Valido.MOVIO;
                CXYIF.Cod = Cod;


                /*if (CXYIF.VALOR_TABLERO > MAX_VALOR)
                {
                    MEJOR_MOVIDA = CXYIF;
                    MAX_VALOR = MEJOR_MOVIDA.VALOR_TABLERO;
                }

                 */

                if (Es_Movimiento_Valido.MOVIO || Es_Movimiento_Valido.COMIO)
                {
                    CXYIF.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero);
                    List_Movimientos_Validos_De_La_REINA.Add(CXYIF);
                    
                }
            }
            return List_Movimientos_Validos_De_La_REINA;
        }

        private List<Movimiento> Obtener_List_Movimientos_Validos_REY(List<Movimiento_Posible>
            List_Posibles_Movimientos_DEL_REY, byte X_Start, byte Y_Start, byte Cod, Torre TTorre, Tablero TTablero)
        {
            List<Movimiento> List_Movimientos_Validos_Del_REY = new List<Movimiento>();

            for (byte i = 0; i < List_Posibles_Movimientos_DEL_REY.Count; i++)
            {
                Rey TRey = new Rey();
                Movimiento CXYIF = new Movimiento();
                CXYIF.X_Start = (sbyte)X_Start;
                CXYIF.Y_Start = (sbyte)Y_Start;
                CXYIF.X_End = (sbyte)List_Posibles_Movimientos_DEL_REY[i].X_End;
                CXYIF.Y_End = (sbyte)List_Posibles_Movimientos_DEL_REY[i].Y_End;
                Tablero copia_Tablero = TTablero.CopyTo();
                Movimiento Es_Movimiento_Valido = TRey.ValidaMovimiento(CXYIF, copia_Tablero, TTorre, Cod);
                CXYIF.COMIO = Es_Movimiento_Valido.COMIO;
                CXYIF.MOVIO = Es_Movimiento_Valido.MOVIO;
                CXYIF.ENROQUE = Es_Movimiento_Valido.ENROQUE;
                CXYIF.Cod = Cod;
                    

                /*
                if (CXYIF.VALOR_TABLERO > MAX_VALOR)
                {
                    MEJOR_MOVIDA = CXYIF;
                    MAX_VALOR = MEJOR_MOVIDA.VALOR_TABLERO;
                }
                */

                if (Es_Movimiento_Valido.MOVIO || Es_Movimiento_Valido.COMIO)
                {
                    CXYIF.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero);
                    List_Movimientos_Validos_Del_REY.Add(CXYIF);
                    
                }

            }

            return List_Movimientos_Validos_Del_REY;
        }

        #endregion Movimientos Posibles Y Validos

        #endregion Métodos
    }

}
 
