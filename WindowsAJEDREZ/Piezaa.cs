﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace WindowsAJEDREZ
{
    class Pieza
    {

        public virtual byte ObtenereCodificación(bool es_blanca) { return 0; }
        public virtual CoordenadaXY ValidarMovimiento(int x_inicial, int y_inicial, int x_destino, int y_destino,  byte separ_piezas, byte Codificacion) 
        {
            CoordenadaXY xy = new CoordenadaXY();
            xy.x = 0;
            xy.y = 0;
            return xy;
        }
        public virtual CoordenadaXY ValidarComer(int x_inicial, int y_inicial, int x_destino, int y_destino, byte separ_piezas, byte Codificacion)
        {
            CoordenadaXY xy = new CoordenadaXY();
            xy.x = 0;
            xy.y = 0;
            return xy;
        }
        public virtual void MoverPieza(System.Windows.Forms.Panel PanelFichaX, int PanelFichas_Left, int PanelFichas_Top, int mouseX_inicial, int mouseY_inicial, CoordenadaXY DesplazamientoValido, byte Codificacion) { }
        public virtual void EstablecerValoresImportantes(bool BlancasAbajo, byte separ_piezas){}
        public byte ObtenerColorPieza(byte Codificacion) 
        {
            Codificacion = (byte)(Codificacion & 1);
            return Codificacion;
        }


        
    }
}
