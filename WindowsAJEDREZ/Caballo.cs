﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    class Caballo: Pieza
    {

        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte Caballo = m_CodificacionPiezas.CN();
            if (es_blanca == true)
                Caballo = m_CodificacionPiezas.CB();

            return Caballo;
        }

        public override Movimiento ValidaMovimiento(Movimiento CXYIF, 
            Tablero TTablero, byte Cod)
        {
            Movimiento valido = CXYIF;
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UN CABALLO A TRAVES DE LA CODIFICACION            
            if (Cod == ObtenereCodificación(true) || Cod == ObtenereCodificación(false))
            {
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;
                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTOS ((((Y = 1 || Y = -1) && (X = 2 || x = -2)) || ((Y = 2 || y = -2) && (X = 1 || X = -1)))    &&   ( A || B))
                if (((((dif_Y == 1) || (dif_Y == -1)) && ((dif_X == 2) || (dif_X == -2))) || (((dif_Y == 2) || (dif_Y == -2)) && ((dif_X == 1) || (dif_X == -1)))) 
                    && (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(Cod) 
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                    && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false))
                {
                    valido.MOVIO = true;
                    //COMIO LA FICHA           
                    byte Cod_Ficha_A_Comer = PosFichaAComer(CXYIF, TTablero);
                    //byte[,]Tablero_64_Casilla = Obtener_Tablero_64_Casillas(TTablero);
                    if (Cod_Ficha_A_Comer != 0)
                        valido.COMIO = true;
                }
            }
            return valido;
        }

        public override Movimiento ValidaMovimientoYMueve(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, Tablero_Index TTablero_Index)
        {
            Movimiento valido = CXYIF;
            Cod_Nro dato = new Cod_Nro();

            dato.Cod = (byte)PanelX.Tag;
            dato.Nro = Convert.ToByte(PanelX.Name);
            
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UN CABALLO A TRAVES DE LA CODIFICACION            
            if (dato.Cod == ObtenereCodificación(true) || dato.Cod == ObtenereCodificación(false))
            {
                
                byte codificacion = (byte)PanelX.Tag;

                
                valido.MOVIO = false;
                valido.COMIO = false;
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;
                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTOS ((((Y = 1 || Y = -1) && (X = 2 || x = -2)) || ((Y = 2 || y = -2) && (X = 1 || X = -1)))    &&   ( A || B))
                if (((((dif_Y == 1) || (dif_Y == -1)) && ((dif_X == 2) || (dif_X == -2))) || (((dif_Y == 2) || (dif_Y == -2)) 
                    && ((dif_X == 1) || (dif_X == -1)))) 
                    && (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(codificacion) 
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                    && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false))
                {
                    //MUEVE PIEZAS
                    Mover_Solo_Piezas_Paneles(PanelX, CXYIF);
                    CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                    valido.MOVIO = true;
                    
                    //COMIO LA FICHA           
                    byte Cod_Ficha_A_Comer = PosFichaAComer(CXYIF, TTablero);
                    //byte[,]Tablero_64_Casilla = Obtener_Tablero_64_Casillas(TTablero);
                    if (Cod_Ficha_A_Comer != 0  )
                    {
                        valido.COMIO = true;
                        valido.Index_Del_Panel = Index_Ficha_A_Comer(CXYIF, TTablero_Index);
                    }

                    //MODIFICA EL TABLERO
                    ModificaClaseTablero(CXYIF, dato, TTablero,TTablero_Index);

                }
                else //MOVIMIENTO NOOOOOOOOO VALIDO
                {
                    CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);
                    
                }
            }
            return valido;
        }

    }
}
