﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;


namespace Chess
{
    public class Interfaz : Funciones
    {
        #region Variables Privadas

        Chess m_Form1;
        Tablero m_Tablero;
        Tablero m_Tablero_copia;
        Tablero_Index m_Tablero_Index;
        ListBox m_ListBox_Player_A;
        ListBox m_ListBox_Player_B;
        //PictureBox[] m_ImgPiezas;
        Panel[] m_Panel; //Crea un panel para contener al tablero
        short m_dificultad;
        String m_ruta_imagenes;
        String m_extension_imgs;
        Form2 m_Form2;
        Form_Coronacion m_Form_Coronacion;
        
        
        Motor m_Motor = new Motor();

        #region PIEZAS
        Pieza m_TPeon;
        Torre m_TTorre;
        Pieza m_TCaballo;
        Pieza m_TAlfil;
        Rey m_TRey;
        Pieza m_TReina;
        #endregion PIEZAS

        bool m_already_disposed;
        bool m_jaque_mate;
        bool m_jaque;
        bool m_empate;
        byte m_cont_panel;
        bool m_Dragging;
        int m_mouseX, m_mouseY;
        int m_panel_left, m_panel_top; //Posición inicial de las Piezas cuando CLICK
        int m_mouseX_final; //Coordenada X cuando MouseUp
        int m_mouseY_final; //Coordenada Y cuando MouseUp   
        int m_mouseX_inicial; //Coordenada X cuando MouseDown
        int m_mouseY_inicial; //Coordenada Y cuando MouseDown
        bool m_turno_blancas_o_negras = true;
        bool m_Blancas_Down;
        bool m_Desface_Incoherente_Y_Start;

        sbyte m_nro_de_coronacion = -1;
        int m_cursor_position_X_Start;
        int m_cursor_position_Y_Start;
        int m_cursor_position_X_End;
        int m_cursor_position_Y_End;
        bool m_click;
        bool m_arrastrar;
        byte m_Panel_Tag_Start = 0;
        string m_Panel_Name_Start = "-1";
        byte m_Panel_Tag_End = 0;
        string m_Panel_Name_End = "-1";
        Movimiento m_Movimiento_Actual = new Movimiento();

        #endregion Variables Privadas

        #region Constructor
        public Interfaz()
        {
            
            m_cursor_position_X_Start = 0;
            m_cursor_position_Y_Start = 0;
            m_cursor_position_X_End = 0;
            m_cursor_position_Y_End = 0;

            m_Desface_Incoherente_Y_Start = false;
            m_already_disposed = false;
            m_extension_imgs = "bmp";
            m_dificultad = 1;
            m_cont_panel = 0;
            m_turno_blancas_o_negras = true;
            m_empate = false;
            m_jaque_mate = false;
            m_jaque = false;
            m_click = true;
            m_arrastrar = false;

            m_Tablero_copia = new Tablero();
            m_TPeon = new Peon();
            m_TTorre = new Torre();
            m_TCaballo = new Caballo();
            m_TAlfil = new Alfil();
            m_TRey = new Rey();
            m_TReina = new Reina();
        }
        #endregion Constructor
        
        #region Destructor

        ~Interfaz()
        {

        }

        public void Dispose()
        {
            if (!m_already_disposed)
            {
                m_already_disposed = true;
                for (byte i = 0; i < m_Panel.Length; i++)
                    m_Panel[i].Dispose();
                m_Tablero = null;
                m_Form2.Dispose();
                m_Tablero_Index = null;
                m_TPeon = null;
                m_TTorre = null;
                m_TCaballo = null;
                m_TAlfil = null;
                m_TRey = null;
                m_TReina = null;
                System.GC.Collect(System.GC.GetGeneration((object)(m_Form1)), GCCollectionMode.Forced);
            }
             
        }

        #endregion Destructor

        #region Properties
        public short SET_GET_Dificultad{set{m_dificultad = Obtener_Dificultad(value);} get{return m_dificultad;}}
        public bool Es_Juagador_Blanco { set { m_turno_blancas_o_negras = value; } get { return m_turno_blancas_o_negras; } }
        public bool Blancas_Down { set { m_Blancas_Down = value; } get { return Blancas_Down; } }
        #endregion Properties

        #region Métodos


        private short Obtener_Dificultad(Int16 dificultad)
        {
            short valor = 1;
            for (short i = 1; i < dificultad; i++)
                valor = (short)(valor + 2);
            return valor;
        }

        public void Crear_Interfaz(Chess form1, short pos_x_tablero, short pos_y_tablero, bool P_Blancas_Abajo, 
                                    String ruta_imagenes, ListBox ListBox_Player_A, ListBox ListBox_Player_B)
        {
            m_ListBox_Player_A = ListBox_Player_A;
            m_ListBox_Player_B = ListBox_Player_B;
            m_Form2 = new Form2();
            m_Blancas_Down = P_Blancas_Abajo;  //Fichas de ajedrez Blancas están arriba o abajo del tablero de ajedrez
            m_Tablero = new Tablero();
            
            m_Tablero_Index = new Tablero_Index(m_Blancas_Down);
            m_Form1 = form1;
            m_Form1.lblBlancasONegras.Text = "BLANCAS";
            m_ruta_imagenes = ruta_imagenes;
            //m_ImgPiezas = new PictureBox[2];
            m_pos_X_Tablero = pos_x_tablero;
            m_pos_Y_Tablero = pos_y_tablero;

            m_Panel = new Panel[66];
            CrearSoloTableroSinFichas();
            Crear_Piezas_Todas();

        }

        #region Creación De Todo El Tablero Dinámico
        public bool Es_NULL() 
        { 
            if (m_cont_panel == 0) return true; 
            else return false; 
        }

        private void Crear_Piezas_Todas()
        {
            CrearPeones();
            CrearLineaDeTorreATorre();
            
            for (byte y = 3; y <= 6; y++)
            {
                for (byte x = 1;x <= 8; x++)
                {
                    Crear_Casillas_Vacias_Paneles(x, y);
                }
            }
            
        }

        private void Crear_Casillas_Vacias_Paneles(byte x, byte y)
        {
            /*Bitmap BitmapX = new Bitmap(ruta_img_final);
            BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
            */ 
            m_Panel[m_cont_panel] = new Panel();

            //m_Panel[m_cont_panel].BackgroundImage = BitmapX;
            m_Panel[m_cont_panel].Left = (x - 1) * Separacion_Piezas_X();
            m_Panel[m_cont_panel].Top = (y - 1) * Separacion_Piezas_Y();
            m_Panel[m_cont_panel].Size = new Size(60, 60); ;
            m_Panel[m_cont_panel].BackColor = Color.Transparent;
            m_Panel[m_cont_panel].Parent = m_Panel[0];
            m_Panel[m_cont_panel].BringToFront();
            m_Panel[m_cont_panel].Tag = (byte)0;
            m_Panel[m_cont_panel].Name = Convert.ToString(m_cont_panel - 1);

            m_Panel[m_cont_panel].MouseDown += new MouseEventHandler(m_PanelFichas_MouseDown);
            m_Panel[m_cont_panel].MouseMove += new MouseEventHandler(m_PanelFichas_MouseMove);
            m_Panel[m_cont_panel].MouseUp += new MouseEventHandler(m_PanelFichas_MouseUp);
            m_Panel[m_cont_panel].MouseClick += new MouseEventHandler(m_PanelFichas_MouseClick);

            m_cont_panel++;
            
        }

        private void CrearPeones()
        {
            
            int blancas, negras;
            if(m_Blancas_Down == true){blancas = 1; negras= 2;} ///Su valor es de 1 o 2 porque es el nombre 
                                                                ///con lo que se buscará la ruta de la imagen 
                                                                ///ya sea x1y.extension o x2y.extension
            else{blancas = 2; negras = 1;}

            ////int i = 1;
            String ruta_img = Obtener_ruta_img_v(negras, 6);
            for (int i=1; i<=8;i++)
            {
                CreaPanelesDinamicos(i, 2, ruta_img);
                //CrearPiezaYa(i, 2, ruta_img, true, -43, 0);
            }

            ruta_img = Obtener_ruta_img_v(blancas, 6);
            for (int i=1; i<=8;i++)
            {

                CreaPanelesDinamicos(i, 7, ruta_img);
                //CrearPiezaYa(i, 7, ruta_img, true, -43, 117);
            }
              
        }

        private void CrearLineaDeTorreATorre()
        {
            CrearTorreCaballoAlfil();
            CrearReyReina();
        }

        private String Obtener_ruta_img_v(int pos1, int pos0)
        {
            String a_string = Convert.ToString(pos1);
            String b_string = Convert.ToString(pos0);
            String ruta_img = m_ruta_imagenes + a_string + b_string + "." + m_extension_imgs;
            return ruta_img;
        }

        private void CreaPanelesDinamicos(int x, int y, String ruta_img_final)
        {
            Bitmap BitmapX = new Bitmap(ruta_img_final);
            BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
            m_Panel[m_cont_panel] = new Panel();
            
            m_Panel[m_cont_panel].BackgroundImage = BitmapX;
            m_Panel[m_cont_panel].Left = (x-1) * Separacion_Piezas_X();
            m_Panel[m_cont_panel].Top = (y-1) * Separacion_Piezas_Y();
            m_Panel[m_cont_panel].Size = BitmapX.Size;
            m_Panel[m_cont_panel].BackColor = Color.Transparent;
            m_Panel[m_cont_panel].Parent = m_Panel[0];
            m_Panel[m_cont_panel].BringToFront();
            m_Panel[m_cont_panel].Tag = NroToCodificacion(m_cont_panel, m_Blancas_Down);
            m_Panel[m_cont_panel].Name = Convert.ToString(m_cont_panel-1);

            m_Panel[m_cont_panel].MouseDown +=new MouseEventHandler(m_PanelFichas_MouseDown);
            m_Panel[m_cont_panel].MouseMove +=new MouseEventHandler(m_PanelFichas_MouseMove);
            m_Panel[m_cont_panel].MouseUp +=new MouseEventHandler(m_PanelFichas_MouseUp);
            m_Panel[m_cont_panel].MouseClick += new MouseEventHandler(m_PanelFichas_MouseClick);

            m_cont_panel++;
            
        }
        
        private void CrearTorreCaballoAlfil()
        {
            int blancas, negras;
            if (m_Blancas_Down == true) { blancas = 1; negras = 2; }
            else{blancas = 2; negras = 1;}

            //BLANCAS O NEGRAS
            for (int x =1; x<=3;x++)
            {
                for(int y =1;y<=2;y++)
                {
                        int x_aux = x;
                        if (y == 2) x_aux = 9-x;
                        String ruta_img = Obtener_ruta_img_v(negras, x);
                        CreaPanelesDinamicos(x_aux, 1, ruta_img);
                }
            }

            //NEGRAS O BLANCAS
            for (int x =1; x<=3;x++)
            {
                for(int y =1;y<=2;y++)
                {
                        int x_aux = x;
                        if (y == 2) x_aux = 9-x;
                        String ruta_img = Obtener_ruta_img_v(blancas, x);
                        CreaPanelesDinamicos(x_aux, 8, ruta_img);
                        //CrearPiezaYa(x_aux - 1, 7, ruta_img, true, 23, 183);
                }
            }
        }

        private void CrearReyReina()
        {
            int blancas, negras;
            if (m_Blancas_Down == true) { blancas = 1; negras = 2; }
            else{blancas = 2; negras = 1;}

            if (m_Blancas_Down == true)
            {
                int i = 4;
                for(int y=4;y<=5;y++) //crea Rey Reyna con este for
                {
                        String ruta_img = Obtener_ruta_img_v(negras, y);
                        CreaPanelesDinamicos(i, 1, ruta_img);
                        //CrearPiezaYa(i - 1, 0, ruta_img, true, 23, 17);
                        i++;
                }

                i=4;
                for(int y=4;y<=5;y++)
                {
                        String ruta_img = Obtener_ruta_img_v(blancas, y);
                        CreaPanelesDinamicos(i, 8, ruta_img);
                        //CrearPiezaYa(i - 1, 7, ruta_img, true, 23, 183);
                        i++;
                }
            }
            else
            {
                int i=5;
                for(int y=4;y<=5;y++)
                {
                        String ruta_img = Obtener_ruta_img_v(negras, y);
                        CreaPanelesDinamicos(i, 1, ruta_img);
                        //CrearPiezaYa(i - 1, 0, ruta_img, true, 23, 17);
                        i--;

                }
                i=5;
                for(int y=4;y<=5;y++)
                {
                        String ruta_img = Obtener_ruta_img_v(blancas, y);
                        CreaPanelesDinamicos(i, 8, ruta_img);
                        //CrearPiezaYa(i - 1, 7, ruta_img, true, 23, 183);
                        i--;
                }
            }
        }

        private void CrearSoloTableroSinFichas()
        {
            //CREA LA PARTE EXTERIOR DEL TABLERO LOS NUMEROS Y LETRAS
            m_Panel[65] = new Panel();
            Bitmap BitmapX2 = new Bitmap(m_ruta_imagenes + "01" + "." + m_extension_imgs);

            m_Panel[65].BackgroundImage = BitmapX2;
            m_Panel[65].Left = m_pos_X_Tablero;
            m_Panel[65].Top = m_pos_Y_Tablero;
            m_Panel[65].Size = BitmapX2.Size;
            m_Panel[65].Parent = m_Form1;
            m_Panel[65].SendToBack();

            int tamano = m_Panel[65].Height;
            
            
            //CREA EL TABLERO
            m_Panel[m_cont_panel] = new Panel();
            Bitmap BitmapX = new Bitmap(m_ruta_imagenes + "00" + "." + m_extension_imgs);
            m_Panel[m_cont_panel].BackgroundImage = BitmapX;
            m_Panel[m_cont_panel].Left = m_pos_X_Tablero + 22;
            m_Panel[m_cont_panel].Top = m_pos_Y_Tablero + 16;
            m_Panel[m_cont_panel].Size = BitmapX.Size;
            m_Panel[m_cont_panel].Parent = m_Form1;
            m_Panel[m_cont_panel].BringToFront();

            //m_Panel[m_cont_panel].Show();
            
            
            m_cont_panel++;

        }
        #endregion Creación De Todo El Tablero Dinámico

        #region EVENTOS
        private void m_PanelFichas_MouseClick(object sender, MouseEventArgs e)
        {
            if (m_click == true)
            {
                //Reanudar_Timer();
                Panel PanelX = (Panel)sender;
                PanelX.BringToFront();
                if (m_Form1.Cursor == Cursors.Default)
                {
                    m_Panel_Tag_Start = Convert.ToByte(PanelX.Tag);
                    m_Panel_Name_Start = PanelX.Name;
                    m_Form1.Cursor = Cursors.Hand;
                    m_panel_left = PanelX.Left;
                    m_panel_top = PanelX.Top;
                    Point P = new Point();

                    P = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition);
                    m_mouseX = -e.X;
                    m_mouseY = -e.Y;
                    P.Offset(m_mouseX, m_mouseY);
                    m_mouseX_inicial = P.X - 272;
                    m_mouseY_inicial = P.Y - 76;

                    PanelX.Invalidate();
                }
                else
                {
                    //ASUME QUE MOVIO
                    m_Panel_Tag_End = Convert.ToByte(PanelX.Tag);
                    m_Panel_Name_End = PanelX.Name;
                    m_Form1.Cursor = Cursors.Default;
                    PanelX.Tag = m_Panel_Tag_Start;
                    m_PanelFichas_MouseMove(sender, e);
                    Mover(PanelX);
                }
            }
            //m_PanelFichas_MouseDown(sender, e);
        }

        private void m_PanelFichas_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_arrastrar == true)
            {
                Panel PanelX = (Panel)sender;
                int clipLeft, clipTop, clipWidth, clipHeight;
                m_panel_left = PanelX.Left;
                m_panel_top = PanelX.Top;
                //int xxx = e.X;
                PanelX.BringToFront();

                if (e.Button == MouseButtons.Left)
                {
                    Point P = new Point();

                    P = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition);
                    m_Dragging = true;
                    m_mouseX = -e.X;
                    m_mouseY = -e.Y;
                    P.Offset(m_mouseX, m_mouseY);

                    m_cursor_position_X_Start = Cursor.Position.X;
                    m_cursor_position_Y_Start = Cursor.Position.Y;

                    m_mouseX_inicial = P.X - 272;

                    m_mouseY_inicial = P.Y - 76;

                    clipLeft = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition).X - PanelX.Location.X;
                    clipTop = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition).Y - PanelX.Location.Y;
                    clipWidth = m_Form1.ClientSize.Width - (PanelX.Width - clipLeft);
                    clipHeight = m_Form1.ClientSize.Height - (PanelX.Height - clipTop);
                    Cursor.Clip = m_Form1.RectangleToScreen(new Rectangle(clipLeft + 2, clipTop + 2, clipWidth, clipHeight));
                    PanelX.Invalidate();
                    m_Form1.Cursor = Cursors.Hand;
                }
            }
          
        }
        
        private void m_PanelFichas_MouseMove(object sender, MouseEventArgs e)
        {

            if (m_click == true)
            {
                //Panel PanelX = (Panel)sender;
                //PanelX.BringToFront();
                //if (m_Dragging)
                //{
                if (m_nro_de_coronacion != -1)
                {
                    Actualizar_Interfaz_De_Acuerdo_Al_Tablero_Por_Coronacion();
                    m_nro_de_coronacion = -1;

                    Modifica_Clase_Tablero(m_Movimiento_Actual);
                    Actualizar_Tags_De_Acuerdo_Al_Tablero();
                    Verifica_Jaque_Mate(m_Movimiento_Actual.Cod);
                    Mostrar_En_Data_Grid(m_Movimiento_Actual);
                    m_Form_Coronacion.Close();
                }
                        
                    Point nextPosition = new Point();
                    nextPosition = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition);

                    nextPosition.X -= m_pos_X_Tablero + 24;//24
                    nextPosition.Y -= m_pos_Y_Tablero + 18;//18
                    nextPosition.Offset(m_mouseX, m_mouseY);

                    //PanelX.Location = nextPosition;
                    //PanelX.Location = nextPosition;
                    m_Form1.Text = nextPosition.ToString();

                    //m_cursor_position_X_End = Cursor.Position.X;
                    //m_cursor_position_Y_End = Cursor.Position.Y;

                    m_mouseX_final = nextPosition.X;
                    m_mouseY_final = nextPosition.Y;
                //}
            }
            if (m_arrastrar == true)
            {
                Panel PanelX = (Panel)sender;
                PanelX.BringToFront();
                if (m_Dragging)
                {
                    Point nextPosition = new Point();
                    nextPosition = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition);

                    nextPosition.X -= m_pos_X_Tablero + 24;//24
                    nextPosition.Y -= m_pos_Y_Tablero + 18;//18
                    nextPosition.Offset(m_mouseX, m_mouseY);

                    PanelX.Location = nextPosition;
                    //PanelX.Location = nextPosition;
                    m_Form1.Text = nextPosition.ToString();

                    m_cursor_position_X_End = Cursor.Position.X;
                    m_cursor_position_Y_End = Cursor.Position.Y;

                    m_mouseX_final = nextPosition.X;
                    m_mouseY_final = nextPosition.Y;
                }
            }
        }
 
        private void m_PanelFichas_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_arrastrar == true)
            {
                Panel PanelX = (Panel)sender;
                PanelX.BringToFront();
                Mover(PanelX);
            }
        }

        private void m_Form_Coronacion_Ficha_MouseClick(object sender, MouseEventArgs e)
        {
            //Thread.CurrentThread.Resume();
            m_Form1.Enabled = true;
            Panel PanelX = (Panel)sender;
            byte Tag = Convert.ToByte(PanelX.Tag);
            if (Tag == 0) //CABALLO
            {
                this.m_nro_de_coronacion = 0;
            }
            else if (Tag == 1) //ALFIL
            {
                this.m_nro_de_coronacion = 1;
            }
            else if (Tag == 2) //TORRE
            {
                this.m_nro_de_coronacion = 2;
            }
            else if (Tag == 3) //REINA
            {
                this.m_nro_de_coronacion = 3;
            }

            m_Form1.BringToFront();

        }
        #endregion EVENTOS

        //private void Reanudar_Timer() { m_Form1.timer1.Enabled = true; m_Form1.timer1.Start(); }
        //private void Pausar_Timer() { m_Form1.timer1.Stop(); m_Form1.timer1.Enabled = false; }

        public void Girar_Tablero_RePintando_Imagenes()
        {
            
            if (m_Blancas_Down) 
            {
                m_Blancas_Down = false;
                m_turno_blancas_o_negras = true;
            }
            else
            {
                m_Blancas_Down = true;
                m_turno_blancas_o_negras = false;

            }
             
            for (byte y = 0; y <= 7; y++)
            {
                for (byte x = 0; x <= 7; x++)
                {
                    if (m_Tablero.GET_CodFicha(x,y) != 0)
                    {
                        string ruta_imagen_parcial = Obtener_Ruta_Imagen_Pieza_X(m_Tablero.GET_CodFicha(x, y)); 
                        Bitmap BitmapX = new Bitmap(ruta_imagen_parcial+ "." + m_extension_imgs);
                        BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                        m_Panel[Convertir_PosXY_NROcasilla_64(x, y) + 1].BackgroundImage = BitmapX;
                    }

                }
            }



            //CAMBIA TURNOS
            /*
            if (m_turno_blancas_o_negras == true)
            {
                m_Form1.lblBlancasONegras.Text = "NEGRAS";
            }
            else
            {
                m_Form1.lblBlancasONegras.Text = "BLANCAS";
            }
            */
        }

        private Movimiento Valida_Movimientos_Y_Modifica_Tablero(Panel PanelX, byte codificacion)
        {
            Movimiento CXYIF = new Movimiento();
            //Point PosActual = new Point();
            //PosActual = System.Windows.Forms.Control.MousePosition;
            CXYIF.jaque = m_jaque;
            m_Desface_Incoherente_Y_Start = false;
            CXYIF.X_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseX_inicial);
            CXYIF.Y_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseY_inicial);
            CXYIF.X_End = (sbyte)RedondearValor_SeparPiezas(m_mouseX_final);
            CXYIF.Y_End = (sbyte)RedondearValor_SeparPiezas(m_mouseY_final);
            

            Movimiento movio = new Movimiento();
            if (m_arrastrar == true)
            {
                if (m_Dragging == true && CXYIF.X_End <= 7.5 && CXYIF.X_End >= 0 && CXYIF.Y_End <= 7.5 && CXYIF.Y_End >= 0)
                {

                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TPeon.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form1, m_Panel[0], m_Tablero, m_Tablero_Index);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TTorre.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form1, m_Panel[0], m_Tablero, m_Tablero_Index);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TCaballo.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form1, m_Panel[0], m_Tablero, m_Tablero_Index);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TAlfil.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form1, m_Panel[0], m_Tablero, m_Tablero_Index);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TRey.ValidaMovimientoYMueveRey(ref m_Dragging, PanelX, CXYIF, m_Form1, m_Panel[0], m_Tablero, m_TTorre, m_Tablero_Index);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TReina.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form1, m_Panel[0], m_Tablero, m_Tablero_Index);
                }
                else
                {
                    CuandoMovimientoPiezasNOValido(PanelX);
                }

                if (movio.COMIO == true)
                    Eliminar_Ficha_Comida(movio.Index_Del_Panel);

                if (movio.ENROQUE == true)
                {
                    MueveTorreXEnroque(codificacion);
                    ModificaClaseTablero_LaTorre(codificacion);
                }
            }
            else
            {
                if (CXYIF.X_End <= 7.5 && CXYIF.X_End >= 0 && CXYIF.Y_End <= 7.5 && CXYIF.Y_End >= 0)
                {

                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TPeon.ValidaMovimiento(CXYIF, m_Tablero, codificacion);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TTorre.ValidaMovimiento(CXYIF, m_Tablero, codificacion);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TCaballo.ValidaMovimiento(CXYIF, m_Tablero, codificacion);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TAlfil.ValidaMovimiento(CXYIF, m_Tablero, codificacion);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TRey.ValidaMovimiento(CXYIF, m_Tablero, m_TTorre,codificacion);
                    if (movio.MOVIO == false && movio.COMIO == false)
                        movio = m_TReina.ValidaMovimiento(CXYIF, m_Tablero, codificacion);

                    movio.Cod = codificacion;
                    if (movio.MOVIO == true)
                    {
                        //byte[,] tab = Obtener_Tablero_64_Casillas(m_Tablero);
                        Modifica_Clase_Tablero(movio);
                        //byte[,] tab2 = Obtener_Tablero_64_Casillas(m_Tablero);
                        Actualizar_Intertaz_De_Acuerdo_Al_Tablero(movio);
                        //Pausar_Timer();

                    }
                    if (movio.ENROQUE == true)
                    {
                        movio = Modifica_Clase_Tablero_Torre_X_Enroque(codificacion);
                        Actualizar_Intertaz_De_Acuerdo_Al_Tablero(movio);
                    }
                    if (movio.CORONO == true)
                    {
                        m_Movimiento_Actual = movio.CopyTo();
                        Mostrar_Form_Para_Coronar();
                    }
                }
            }
            return movio;
        }

        private void Desbloquea()
        {
            Thread.Sleep(1000);
            m_Form_Coronacion.BringToFront();
            if (m_nro_de_coronacion != -1)
            {
                Actualizar_Interfaz_De_Acuerdo_Al_Tablero_Por_Coronacion();
                m_nro_de_coronacion = -1;
            }
            
        }

        private void Lock()
        {
            MessageBox.Show("A punto de lock");
            lock (m_Form1)
            {
                MessageBox.Show("Locked");
                while (m_Form1.Enabled == false)
                {
                    Monitor.Exit(m_Form1);
                    Monitor.Enter(m_Form1);
                }
            }
            MessageBox.Show("Salio");
        }

        private void Modifica_Clase_Tablero(Movimiento TMovimiento)
        {
            //INSERTA LA NUEVA FICHA EN LA NUEVA POSICION
            m_Tablero.SET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End, 0);
            m_Tablero.SET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End, TMovimiento.Cod);
            //INSERTA "0" O VACIO EN LA POSICIÓN QUE DEJÓ LA FICHA
            m_Tablero.SET_CodFicha((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start, 0);
        }

        private void Mover(Panel PanelX)
        {
            
            m_Tablero_copia = m_Tablero.CopyTo();
            PanelX.BringToFront();
            bool click_blanca_o_negra = new bool();
            //DETERMINA EL COLOR DE LA PIEZA HECHA CLICK
            byte codificacion = (byte)PanelX.Tag;
            //DETERMINA EL COLOR DE LA PIEZA HECHA CLICK
            if (Obtener_Color_Pieza(codificacion) == 1) //ES BLANCA
                click_blanca_o_negra = true;
            else
                click_blanca_o_negra = false;


            //TODO EL AMBITO DE MOVIMIENTOS DE TODAS LAS PIEZAS
            Movimiento movio = new Movimiento();
            
            if (click_blanca_o_negra == m_turno_blancas_o_negras && m_jaque_mate == false && m_empate == false)
            {
                movio = Valida_Movimientos_Y_Modifica_Tablero(PanelX, codificacion);
                
                if (movio.MOVIO == true || movio.COMIO == true)
                {
                    Verifica_Jaque_Mate(codificacion);
                    Insertar_Movimientos_A_ListBox(movio, codificacion);
                    Cambia_Turnos();
                    Movimientos_De_La_PC();
                    Mostrar_En_Data_Grid(movio);
                    Verifica_Empate();

                }
            }
            else
            {
                if(m_arrastrar == true)
                    CuandoMovimientoPiezasNOValido(PanelX);
            }
            if (movio.MOVIO == false)
                Actualizar_Tags_De_Acuerdo_Al_Tablero();

            PanelX.Parent = m_Panel[0];
        }

        private void Movimientos_De_La_PC()
        {
            if (m_jaque_mate == false)
            {
                if (m_Form1.facilToolStripMenuItem.Checked == true || m_Form1.medioToolStripMenuItem.Checked == true
                    || m_Form1.dificilToolStripMenuItem.Checked == true)
                {
                    m_turno_blancas_o_negras = false;
                    //Reanudar_Timer();
                    m_Panel[0].Enabled = false;
                    //byte[,] tab2 = Obtener_Tablero_64_Casillas(m_Tablero); 
                    Movimiento Nuevo_Movimiento = new Movimiento();
                    Nuevo_Movimiento = Pensar();
                    if (m_jaque == false)
                        Verifica_Jaque_Mate(Nuevo_Movimiento.Cod);
                    m_Panel[0].Enabled = true;
                    if (Nuevo_Movimiento.Cod == new CodificacionPiezas().PN() && Nuevo_Movimiento.Y_End == 7)
                        Coronacion_De_La_PC(Nuevo_Movimiento);
                    Actualizar_Intertaz_De_Acuerdo_Al_Tablero(Nuevo_Movimiento);
                    Insertar_Movimientos_A_ListBox(Nuevo_Movimiento, Nuevo_Movimiento.Cod);
                    //Pausar_Timer();
                    m_turno_blancas_o_negras = true;
                    

                }
            }
        
        }

        private void Coronacion_De_La_PC(Movimiento TMovimiento)
        {
            byte cod_pieza = new byte();
            Thread.Sleep(1);
            Random ran = new Random();
            byte nro_rand = (byte)ran.Next(0, 3);
            if (nro_rand == 0) cod_pieza = new CodificacionPiezas().CN();
            else if (nro_rand == 1) cod_pieza = new CodificacionPiezas().AN();
            else if (nro_rand == 2) cod_pieza = new CodificacionPiezas().TN();
            else if (nro_rand == 3) cod_pieza = new CodificacionPiezas().QN();

            TMovimiento.Cod = cod_pieza;
            Modifica_Clase_Tablero(TMovimiento);
            m_Panel[Convertir_PosXY_NROcasilla_64((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End)].Tag = cod_pieza;

            Bitmap BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza) + "." + m_extension_imgs);
            BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
            m_Panel[Convertir_PosXY_NROcasilla_64((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End)].BackgroundImage = BitmapX;
            Verifica_Jaque_Mate(cod_pieza);
            Mostrar_En_Data_Grid(TMovimiento);

        }

        private Movimiento Pensar()
        {
            Movimiento movio = new Movimiento();
            byte[,] tab = Obtener_Tablero_64_Casillas(m_Tablero);
            movio = m_Motor.Pensar(m_dificultad, m_Tablero, m_TTorre);
            Modifica_Clase_Tablero(movio);
            return movio;
        }


        private void Verifica_Jaque_Mate(byte cod_pieza_que_mueve)
        {
            
            m_jaque = m_Tablero.Verifica_SI_Hay_Jaque_En_El_Tablero(cod_pieza_que_mueve);

            if (m_jaque == true)
            {
                
                m_jaque_mate = m_Tablero.Verifica_SI_Hay_Jaque_Mate_En_Tablero(cod_pieza_que_mueve);
                if (m_jaque_mate)
                    MessageBox.Show("¡JAQUE MATE!", "¡JAQUE MATE!");
                else
                    MessageBox.Show("¡Jaque!", "¡Jaque!");
            }
        }

        private bool Verifica_Empate() 
        {
            List<byte[]> Lista_De_Empates = Cargar_Lista_Piezas_Para_Verificar_Empate();
            List<byte>List_Piezas = new List<byte>();
            //bool empate = false;
            bool encontro_otra_pieza = false;
            for (byte x = 0; x <= 7 && encontro_otra_pieza == false; x++)
            {
                for (byte y = 0; y <= 7 && encontro_otra_pieza == false; y++)
                {
                    if (m_Tablero.GET_CodFicha(x, y) != 0)
                    {
                        byte[,] TableroPruebax = Obtener_Tablero_64_Casillas(m_Tablero);
                        if (m_Tablero.GET_CodFicha(x, y) == new CodificacionPiezas().KB() || m_Tablero.GET_CodFicha(x, y) == new CodificacionPiezas().KN()
                            || m_Tablero.GET_CodFicha(x, y) == new CodificacionPiezas().AB() || m_Tablero.GET_CodFicha(x, y) == new CodificacionPiezas().AN()
                            || m_Tablero.GET_CodFicha(x, y) == new CodificacionPiezas().CB() || m_Tablero.GET_CodFicha(x, y) == new CodificacionPiezas().CN()
                            )
                        {
                            List_Piezas.Add(m_Tablero.GET_CodFicha(x, y));
                            byte[,] TableroPrueba = Obtener_Tablero_64_Casillas(m_Tablero);
                        }
                        else
                            encontro_otra_pieza = true;
                    }
                }
            }

            byte [,]TableroPrueba2 = Obtener_Tablero_64_Casillas(m_Tablero);
            byte cont = 0;
            if (encontro_otra_pieza == false)
            {
                //ORDENA ASCENDENTEMENTE LA LISTA
                List_Piezas.Sort(delegate(byte m1, byte m2) { return m1.CompareTo(m2); });
                byte[] Piezas2 = new byte[List_Piezas.Count];
                //for (byte i = 0; i <= List_Piezas.Count; i++)
                //{
                //    Piezas2[cont] = List_Piezas[cont];
                    Piezas2 = List_Piezas.ToArray();
                //}

                //COMPARA CON LA LISTA DE PIEZAS
                for (byte i = 0; i < 7 && m_empate == false; i++)
                {
                    byte[] Tablero_A_Comparar_7 = Lista_De_Empates[i];
                    byte veces = 0;
                    for (byte j = 0; j < Tablero_A_Comparar_7.Length; j++)
                    {
                        if (Piezas2[j] == Tablero_A_Comparar_7[j])
                        {
                            veces++;
                        }
                    }
                    if (veces == Piezas2.Length)
                    {
                        m_empate = true;
                        MessageBox.Show("¡GAME OVER!", "¡EMPATE!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }

            return m_empate;
        }

        List<byte[]> Cargar_Lista_Piezas_Para_Verificar_Empate()
        {
            List<byte []> Lista_De_Empates = new List<byte[]>(7);
            byte[] Rey_Contra_Rey = new byte[2];
            Rey_Contra_Rey[0] = new CodificacionPiezas().KN();
            Rey_Contra_Rey[1] = new CodificacionPiezas().KB();

            byte[] Rey_Contra_Rey_Y_Alfil_Blanco = new byte[3];
            Rey_Contra_Rey_Y_Alfil_Blanco[0] = new CodificacionPiezas().AB();
            Rey_Contra_Rey_Y_Alfil_Blanco[1] = new CodificacionPiezas().KN();
            Rey_Contra_Rey_Y_Alfil_Blanco[2] = new CodificacionPiezas().KB();

            byte[] Rey_Contra_Rey_Y_Alfil_Negro = new byte[3];
            Rey_Contra_Rey_Y_Alfil_Negro[0] = new CodificacionPiezas().AN();
            Rey_Contra_Rey_Y_Alfil_Negro[1] = new CodificacionPiezas().KN();
            Rey_Contra_Rey_Y_Alfil_Negro[2] = new CodificacionPiezas().KB();

            byte[] Rey_Contra_Rey_Y_Caballo_Blanco = new byte[3];
            Rey_Contra_Rey_Y_Caballo_Blanco[0] = new CodificacionPiezas().CB();
            Rey_Contra_Rey_Y_Caballo_Blanco[1] = new CodificacionPiezas().KN();
            Rey_Contra_Rey_Y_Caballo_Blanco[2] = new CodificacionPiezas().KB();

            byte[] Rey_Contra_Rey_Y_Caballo_Negro = new byte[3];
            Rey_Contra_Rey_Y_Caballo_Negro[0] = new CodificacionPiezas().CN();
            Rey_Contra_Rey_Y_Caballo_Negro[1] = new CodificacionPiezas().KN();
            Rey_Contra_Rey_Y_Caballo_Negro[2] = new CodificacionPiezas().KB();

            byte[] Rey_Contra_Rey_Y_Dos_Caballos_Negros = new byte[4];
            Rey_Contra_Rey_Y_Dos_Caballos_Negros[0] = new CodificacionPiezas().CN();
            Rey_Contra_Rey_Y_Dos_Caballos_Negros[1] = new CodificacionPiezas().CN();
            Rey_Contra_Rey_Y_Dos_Caballos_Negros[2] = new CodificacionPiezas().KN();
            Rey_Contra_Rey_Y_Dos_Caballos_Negros[3] = new CodificacionPiezas().KB();

            byte[] Rey_Contra_Rey_Y_Dos_Caballos_Blancos = new byte[4];
            Rey_Contra_Rey_Y_Dos_Caballos_Blancos[0] = new CodificacionPiezas().CB();
            Rey_Contra_Rey_Y_Dos_Caballos_Blancos[1] = new CodificacionPiezas().CB();
            Rey_Contra_Rey_Y_Dos_Caballos_Blancos[2] = new CodificacionPiezas().KN();
            Rey_Contra_Rey_Y_Dos_Caballos_Blancos[3] = new CodificacionPiezas().KB();

            Lista_De_Empates.Add(Rey_Contra_Rey);
            Lista_De_Empates.Add(Rey_Contra_Rey_Y_Alfil_Blanco);
            Lista_De_Empates.Add(Rey_Contra_Rey_Y_Alfil_Negro);
            Lista_De_Empates.Add(Rey_Contra_Rey_Y_Caballo_Blanco);
            Lista_De_Empates.Add(Rey_Contra_Rey_Y_Caballo_Negro);
            Lista_De_Empates.Add(Rey_Contra_Rey_Y_Dos_Caballos_Blancos);
            Lista_De_Empates.Add(Rey_Contra_Rey_Y_Dos_Caballos_Negros);
            return Lista_De_Empates;
        }

        private void Cambia_Turnos()
        {
            if (m_turno_blancas_o_negras == true)
            {
                if (m_Form1.facilToolStripMenuItem.Checked == false && m_Form1.medioToolStripMenuItem.Checked == false
                    && m_Form1.dificilToolStripMenuItem.Checked == false)
                {
                    m_turno_blancas_o_negras = false;
                    m_Form1.lblBlancasONegras.Text = "NEGRAS";
                }
            }
            else
            {
                m_turno_blancas_o_negras = true;
                m_Form1.lblBlancasONegras.Text = "BLANCAS";
            }
        }

        private void Mostrar_En_Data_Grid(Movimiento CXYIF)
        {
            byte click_x = (byte)CXYIF.X_Start;
            byte click_y = (byte)CXYIF.Y_Start;
            byte x_final = (byte)CXYIF.X_End;
            byte y_final = (byte)CXYIF.Y_End;

            byte[,] Tablero_Real = Obtener_Tablero_64_Casillas(m_Tablero);


            m_Form2.Show();
            m_Form2.Top = 0;
            m_Form2.Left = 0;
            //m_Form2.WindowState = FormWindowState.Minimized;


            if (m_Form2.dataGridView1.RowCount == 9)
            {
                for (byte y = 0; y <= 7; y++)
                    m_Form2.dataGridView1.Rows.RemoveAt(0);
            }

            for (byte y = 0; y <= 7; y++)
            {
                string[] Fila = { Convert.ToString(Tablero_Real[0, y]), Convert.ToString(Tablero_Real[1, y]), 
                                            Convert.ToString(Tablero_Real[2, y]), Convert.ToString(Tablero_Real[3, y]), 
                                            Convert.ToString(Tablero_Real[4, y]), Convert.ToString(Tablero_Real[5, y]), 
                                            Convert.ToString(Tablero_Real[6, y]), Convert.ToString(Tablero_Real[7, y]) };
                m_Form2.dataGridView1.Rows.Add(Fila);
            }
            
            /*
            if (m_Form2.dataGridView2.RowCount == 9)
            {
                for (byte y = 0; y <= 7; y++)
                    m_Form2.dataGridView2.Rows.RemoveAt(0);
            }

            for (byte y = 0; y <= 7; y++)
            {
                string[] Fila = { Convert.ToString(m_Tablero_Index.GET_Ficha(0, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(1, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(2, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(3, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(4, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(5, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(6, y)), 
                                            Convert.ToString(m_Tablero_Index.GET_Ficha(7, y)) };
                m_Form2.dataGridView2.Rows.Add(Fila);
            }

            System.Windows.Forms.TextBox txt_jaque = new TextBox();
            txt_jaque.Parent = m_Form2;
            txt_jaque.BringToFront();
            txt_jaque.Text = Convert.ToString(m_jaque);
            */

            if (m_Form2.dataGridView2.RowCount == 9)
            {
                for (byte y = 0; y <= 7; y++)
                    m_Form2.dataGridView2.Rows.RemoveAt(0);
            }

            for (byte y = 0; y <= 7; y++)
            {
                string[] Fila = { m_Panel[Convertir_PosXY_NROcasilla_64(0,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(1,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(2,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(3,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(4,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(5,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(6,y)+1].Name, 
                                            m_Panel[Convertir_PosXY_NROcasilla_64(7,y)+1].Name
                                };
                m_Form2.dataGridView2.Rows.Add(Fila);
            }

        }

        public void Mostrar_Form_Para_Coronar()
        {
            Panel[] PanelX = Genera_Paneles_Coronacion();
            CodificacionPiezas cod_pieza = new CodificacionPiezas();
            if (Obtener_Color_Pieza(m_Movimiento_Actual.Cod) == 1)
            {
                Bitmap BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.AB()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0,0));
                PanelX[1].BackgroundImage = BitmapX;
                BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.CB()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[0].BackgroundImage = BitmapX;
                BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.TB()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[2].BackgroundImage = BitmapX;
                BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.QB()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[3].BackgroundImage = BitmapX;
            }
            else
            {
                Bitmap BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.AN()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[1].BackgroundImage = BitmapX;
                BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.CN()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[0].BackgroundImage = BitmapX;
                BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.TN()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[2].BackgroundImage = BitmapX;
                BitmapX = new Bitmap(Obtener_Ruta_Imagen_Pieza_X(cod_pieza.QN()) + "." + m_extension_imgs);
                BitmapX.MakeTransparent(BitmapX.GetPixel(0, 0));
                PanelX[3].BackgroundImage = BitmapX;
            }
            m_Form_Coronacion.Show();
            m_Form1.Enabled = false;

            /*
            while (m_nro_de_coronacion == -1)
            {
                m_Form_Coronacion.BringToFront();
                m_Form1.Enabled = false;
            }
             */ 
            
        }

        //BORRAR ES SOLO PRUEBA****************************************************************************************************
        public void SET_Movimiento_Actual()
        {
            m_Movimiento_Actual = new Movimiento();
            m_Movimiento_Actual.CORONO = true;
            m_Movimiento_Actual.Cod = 15;
            m_Movimiento_Actual.MOVIO = true;
            m_Movimiento_Actual.Y_End = 0;
        }
        //BORRAR ES SOLO PRUEBA****************************************************************************************************

        private Panel[] Genera_Paneles_Coronacion()
        {
            m_Form_Coronacion = new Form_Coronacion();
            Panel[] Panel_Ficha = new Panel[4];
            //CABALLO
            Panel_Ficha[0] = new Panel();
            Panel_Ficha[0].Left = 10;
            Panel_Ficha[0].Top = 10;
            Panel_Ficha[0].Size = new Size(60, 60);
            Panel_Ficha[0].Parent = m_Form_Coronacion.panel1;
            Panel_Ficha[0].BringToFront();
            Panel_Ficha[0].Tag = 0;
            Panel_Ficha[0].MouseClick += new MouseEventHandler(m_Form_Coronacion_Ficha_MouseClick);
            //ALFIL
            Panel_Ficha[1] = new Panel();
            Panel_Ficha[1].Left = 80;
            Panel_Ficha[1].Top = 10;
            Panel_Ficha[1].Size = new Size(60, 60);
            Panel_Ficha[1].Parent = m_Form_Coronacion.panel1;
            Panel_Ficha[1].BringToFront();
            Panel_Ficha[1].Tag = 1;
            Panel_Ficha[1].MouseClick += new MouseEventHandler(m_Form_Coronacion_Ficha_MouseClick);
            //TORRE
            Panel_Ficha[2] = new Panel();
            Panel_Ficha[2].Left = 10;
            Panel_Ficha[2].Top = 80;
            Panel_Ficha[2].Size = new Size(60, 60);
            Panel_Ficha[2].Parent = m_Form_Coronacion.panel1;
            Panel_Ficha[2].BringToFront();
            Panel_Ficha[2].Tag = 2;
            Panel_Ficha[2].MouseClick += new MouseEventHandler(m_Form_Coronacion_Ficha_MouseClick);
            //REINA
            Panel_Ficha[3] = new Panel();
            Panel_Ficha[3].Left = 80;
            Panel_Ficha[3].Top = 80;
            Panel_Ficha[3].Size = new Size(60, 60);
            Panel_Ficha[3].Parent = m_Form_Coronacion.panel1;
            Panel_Ficha[3].BringToFront();
            Panel_Ficha[3].Tag = 3;
            Panel_Ficha[3].MouseClick += new MouseEventHandler(m_Form_Coronacion_Ficha_MouseClick);

            return Panel_Ficha;
        }

        private void Actualizar_Interfaz_De_Acuerdo_Al_Tablero_Por_Coronacion()
        {
            string ruta_img_Pieza_X = "-1";
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            byte cod = new byte();
            if (m_nro_de_coronacion == 0)
            {
                if (Obtener_Color_Pieza(m_Movimiento_Actual.Cod) == 1) cod = cod_piezas.CB();
                else cod = cod_piezas.CN();
                ruta_img_Pieza_X = Obtener_Ruta_Imagen_Pieza_X(cod);
                Pintar_Pieza_X_Form_Coronacion(ruta_img_Pieza_X);
                byte Casilla_64 = Convertir_PosXY_NROcasilla_64((byte)m_Movimiento_Actual.X_End, (byte)m_Movimiento_Actual.Y_End);
                m_Panel[Casilla_64].Tag = cod;

            }
            else if (m_nro_de_coronacion == 1)
            {
                if (Obtener_Color_Pieza(m_Movimiento_Actual.Cod) == 1) cod = cod_piezas.AB();
                else cod = cod_piezas.AN();
                ruta_img_Pieza_X = Obtener_Ruta_Imagen_Pieza_X(cod);
                Pintar_Pieza_X_Form_Coronacion(ruta_img_Pieza_X);
                byte Casilla_64 = Convertir_PosXY_NROcasilla_64((byte)m_Movimiento_Actual.X_End, (byte)m_Movimiento_Actual.Y_End);
                m_Panel[Casilla_64].Tag = cod;
            }
            else if (m_nro_de_coronacion == 2)
            {
                if (Obtener_Color_Pieza(m_Movimiento_Actual.Cod) == 1) cod = cod_piezas.TB();
                else cod = cod_piezas.TN();
                ruta_img_Pieza_X = Obtener_Ruta_Imagen_Pieza_X(cod);
                Pintar_Pieza_X_Form_Coronacion(ruta_img_Pieza_X);
                byte Casilla_64 = Convertir_PosXY_NROcasilla_64((byte)m_Movimiento_Actual.X_End, (byte)m_Movimiento_Actual.Y_End);
                m_Panel[Casilla_64].Tag = cod;
            }
            else if (m_nro_de_coronacion == 3)
            {
                if (Obtener_Color_Pieza(m_Movimiento_Actual.Cod) == 1) cod = cod_piezas.QB();
                else cod = cod_piezas.QN();
                ruta_img_Pieza_X = Obtener_Ruta_Imagen_Pieza_X(cod);
                Pintar_Pieza_X_Form_Coronacion(ruta_img_Pieza_X);
                byte Casilla_64 = Convertir_PosXY_NROcasilla_64((byte)m_Movimiento_Actual.X_End, (byte)m_Movimiento_Actual.Y_End);
                m_Panel[Casilla_64].Tag = cod;
            }
            m_Movimiento_Actual.Cod = cod;

        }

        private void Actualizar_Tags_De_Acuerdo_Al_Tablero()
        {
            byte[,] Tablero_Virtual_64 = Obtener_Tablero_64_Casillas(m_Tablero);
            for (byte y = 0; y <= 7; y++)
            {
                for (byte x = 0; x <= 7; x++)
                {
                    m_Panel[Convertir_PosXY_NROcasilla_64(x, y) + 1].Tag = Tablero_Virtual_64[x, y];
                }
            }
        }

        private void Pintar_Pieza_X_Form_Coronacion(string ruta_img_Pieza_X__)
        {
            ruta_img_Pieza_X__ = ruta_img_Pieza_X__ + "." + m_extension_imgs;
            Bitmap BitmapX_End = new Bitmap(ruta_img_Pieza_X__);
            BitmapX_End.MakeTransparent(BitmapX_End.GetPixel(0, 0));
            byte Casilla_X_64 = Convertir_PosXY_NROcasilla_64((byte)m_Movimiento_Actual.X_End, (byte)m_Movimiento_Actual.Y_End);
            m_Panel[Casilla_X_64 + 1].BackgroundImage = BitmapX_End;
            //m_Panel[Casilla_X_64 + 1].Tag = BitmapX_End;
        }

        private void Actualizar_Intertaz_De_Acuerdo_Al_Tablero(Movimiento TMovimiento)
        {

            if (TMovimiento.MOVIO == true)
            {
                string ruta_img_Pieza_X = Obtener_Ruta_Imagen_Pieza_X(TMovimiento.Cod);
                ruta_img_Pieza_X = ruta_img_Pieza_X + "." + m_extension_imgs;

                Bitmap BitmapX_Start = new Bitmap(ruta_img_Pieza_X);
                Bitmap BitmapX_End = new Bitmap(ruta_img_Pieza_X);
                BitmapX_End.MakeTransparent(BitmapX_End.GetPixel(0, 0));
                byte Casilla_X_64_End = Convertir_PosXY_NROcasilla_64((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End);
                byte Casilla_X_64_Start = Convertir_PosXY_NROcasilla_64((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start);
                m_Panel[Casilla_X_64_End + 1].BackgroundImage = BitmapX_End;
                m_Panel[Casilla_X_64_End + 1].Tag = TMovimiento.Cod;
                m_Panel[Casilla_X_64_Start + 1].Tag = 0;

                for (byte y = 0; y < 60; y++)
                    for (byte x = 0; x < 60; x++)
                        BitmapX_Start.MakeTransparent(BitmapX_Start.GetPixel(x, y));

                m_Panel[Convertir_PosXY_NROcasilla_64((byte)TMovimiento.X_Start, 
                    (byte)TMovimiento.Y_Start) + 1].BackgroundImage = BitmapX_Start;
            }
        }



        private string Obtener_Ruta_Imagen_Pieza_X(byte Cod)
        {
            string nombre_archivo;
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            byte nro_nombre_archivo_bmp = 0;
            if (m_Blancas_Down)
            {
                if (Cod == cod_piezas.TB()) nro_nombre_archivo_bmp = 11;
                else if (Cod == cod_piezas.CB()) nro_nombre_archivo_bmp = 12;
                else if (Cod == cod_piezas.AB()) nro_nombre_archivo_bmp = 13;
                else if (Cod == cod_piezas.QB()) nro_nombre_archivo_bmp = 14;
                else if (Cod == cod_piezas.KB()) nro_nombre_archivo_bmp = 15;
                else if (Cod == cod_piezas.PB()) nro_nombre_archivo_bmp = 16;
                //NEGRAS 
                else if (Cod == cod_piezas.TN()) nro_nombre_archivo_bmp = 21;
                else if (Cod == cod_piezas.CN()) nro_nombre_archivo_bmp = 22;
                else if (Cod == cod_piezas.AN()) nro_nombre_archivo_bmp = 23;
                else if (Cod == cod_piezas.QN()) nro_nombre_archivo_bmp = 24;
                else if (Cod == cod_piezas.KN()) nro_nombre_archivo_bmp = 25;
                else if (Cod == cod_piezas.PN()) nro_nombre_archivo_bmp = 26;
            }
            else
            {
                if (Cod == cod_piezas.TB()) nro_nombre_archivo_bmp = 21;
                else if (Cod == cod_piezas.CB()) nro_nombre_archivo_bmp = 22;
                else if (Cod == cod_piezas.AB()) nro_nombre_archivo_bmp = 23;
                else if (Cod == cod_piezas.QB()) nro_nombre_archivo_bmp = 24;
                else if (Cod == cod_piezas.KB()) nro_nombre_archivo_bmp = 25;
                else if (Cod == cod_piezas.PB()) nro_nombre_archivo_bmp = 26;
                //NEGRAS 
                else if (Cod == cod_piezas.TN()) nro_nombre_archivo_bmp = 11;
                else if (Cod == cod_piezas.CN()) nro_nombre_archivo_bmp = 12;
                else if (Cod == cod_piezas.AN()) nro_nombre_archivo_bmp = 13;
                else if (Cod == cod_piezas.QN()) nro_nombre_archivo_bmp = 14;
                else if (Cod == cod_piezas.KN()) nro_nombre_archivo_bmp = 15;
                else if (Cod == cod_piezas.PN()) nro_nombre_archivo_bmp = 16;
            }

            return nombre_archivo = m_ruta_imagenes + nro_nombre_archivo_bmp.ToString();

        }

        private void Eliminar_Ficha_Comida(byte nro) { m_Panel[nro + 1].Dispose(); /*Index = PanelX.Name+1;*/ }

        private void MueveTorreXEnroque(byte CodificacionColor)
        {
            if (CodificacionColor == 1) //ES BLANCO EL REY QUE SE HIZO CLICK
            {
                if (m_TRey.m_valid_enroqu_cort_blan)
                {
                    m_Panel[24].Left = Separacion_Piezas_X() * 5;
                    m_TTorre.m_torre_movio_blan_der = true;

                }
                else if (m_TRey.m_valid_enroqu_larg_blan)
                {
                    m_Panel[23].Left = Separacion_Piezas_X() * 3;
                    m_TTorre.m_torre_movio_blan_izq = true;
                }
            }
            else //ES NEGRO EL REY QUE SE HIZO CLICK
            {
                if (m_TRey.m_valid_enroqu_cort_negr)
                {
                    m_Panel[18].Left = Separacion_Piezas_X() * 5;
                    m_TTorre.m_torre_movio_negr_der = true;
                }
                else if (m_TRey.m_valid_enroqu_larg_negr)
                {
                    m_Panel[17].Left = Separacion_Piezas_X() * 3;
                    m_TTorre.m_torre_movio_negr_izq = true;
                }
            }
        }

        private void CuandoMovimientoPiezasNOValido(Panel PanelX)
        {
            if (m_Dragging == true)
            {
                PanelX.Parent = m_Panel[0];
                PanelX.Left = m_panel_left;
                PanelX.Top = m_panel_top;
                Cursor.Clip = System.Drawing.Rectangle.Empty;
                PanelX.Invalidate();
                m_Form1.Cursor = Cursors.Default;
            }
            m_Dragging = false;
        }

        private Movimiento Modifica_Clase_Tablero_Torre_X_Enroque(byte Cod)
        {
            Movimiento Movio = new Movimiento();
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            if (Obtener_Color_Pieza(Cod) == 1) //ES BLANCO EL REY QUE SE HIZO CLICK
            {
                if (m_TRey.m_valid_enroqu_cort_blan)
                {
                    Movio.Cod = cod_piezas.TB();
                    Movio.X_Start = 7;
                    Movio.Y_Start = 7;
                    Movio.X_End = 5;
                    Movio.Y_End = 7;
                    Movio.MOVIO = true;
                    Movio.ENROQUE = true;
                    Modifica_Clase_Tablero(Movio);
                }
                else if (m_TRey.m_valid_enroqu_larg_blan)
                {
                    Movio.Cod = cod_piezas.TB();
                    Movio.X_Start = 0;
                    Movio.Y_Start = 7;
                    Movio.X_End = 3;
                    Movio.Y_End = 7;
                    Movio.MOVIO = true;
                    Movio.ENROQUE = true;
                    Modifica_Clase_Tablero(Movio);
                }
            }
            else //ES NEGRO EL REY QUE SE HIZO CLICK
            {
                if (m_TRey.m_valid_enroqu_cort_negr)
                {
                    Movio.Cod = cod_piezas.TN();
                    Movio.X_Start = 7;
                    Movio.Y_Start = 0;
                    Movio.X_End = 5;
                    Movio.Y_End = 0;
                    Movio.MOVIO = true;
                    Movio.ENROQUE = true;
                    Modifica_Clase_Tablero(Movio);
                }
                else if (m_TRey.m_valid_enroqu_larg_negr)
                {
                    Movio.Cod = cod_piezas.TN();
                    Movio.X_Start = 0;
                    Movio.Y_Start = 0;
                    Movio.X_End = 3;
                    Movio.Y_End = 0;
                    Movio.MOVIO = true;
                    Movio.ENROQUE = true;
                    Modifica_Clase_Tablero(Movio);
                }
            }

            return Movio;
        }
        
        private void ModificaClaseTablero_LaTorre(byte CodificacionColor)
        {
            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            Cod_Nro dato = new Cod_Nro();
            Cod_Nro dato_vacio = new Cod_Nro();
            dato_vacio.Cod = 0;
            dato_vacio.Nro = 0;


            if (CodificacionColor == 1) //ES BLANCO EL REY QUE SE HIZO CLICK
            {
                if (m_TRey.m_valid_enroqu_cort_blan)
                {
                    dato.Cod = cod_piezas.TB() ;
                    dato.Nro = 23;
                    m_Tablero.SET_CodFicha(5, 7, dato.Cod);
                    m_Tablero.SET_CodFicha(7, 7, 0);
                    m_Tablero_Index.SET_Ficha(5, 7, dato.Nro);
                    m_Tablero_Index.SET_Ficha(7, 7, 0);
                }
                else if (m_TRey.m_valid_enroqu_larg_blan)
                {
                    dato.Cod = cod_piezas.TB();
                    dato.Nro = 22;
                    m_Tablero.SET_CodFicha(3, 7, dato.Cod);
                    m_Tablero.SET_CodFicha(0, 7, 0);
                    m_Tablero_Index.SET_Ficha(3, 7, dato.Nro);
                    m_Tablero_Index.SET_Ficha(0, 7, 0);
                }
            }
            else //ES NEGRO EL REY QUE SE HIZO CLICK
            {
                if (m_TRey.m_valid_enroqu_cort_negr)
                {
                    dato.Cod = cod_piezas.TN();
                    dato.Nro = 17;
                    m_Tablero.SET_CodFicha(5, 0, dato.Cod);
                    m_Tablero.SET_CodFicha(7, 0, 0);
                    m_Tablero_Index.SET_Ficha(5, 0, dato.Nro);
                    m_Tablero_Index.SET_Ficha(7, 0, 0);
                }
                else if (m_TRey.m_valid_enroqu_larg_negr)
                {
                    dato.Cod = cod_piezas.TN();
                    dato.Nro = 16;
                    m_Tablero.SET_CodFicha(3, 0, dato.Cod);
                    m_Tablero.SET_CodFicha(0, 0, 0);
                    m_Tablero_Index.SET_Ficha(3, 0, dato.Nro);
                    m_Tablero_Index.SET_Ficha(0, 0, 0);
                }
            }
        }

        private int RedondearValor_SeparPiezas(float CoordXY)
        {
            float CoordXY_separ_piezas = CoordXY / Separacion_Piezas_X();
            //REDONDEA POSIBLES MOVIMIENTOS > 0
            if (CoordXY_separ_piezas >= -0.5 && CoordXY_separ_piezas <= 0.5)
                CoordXY_separ_piezas = 0;
            else if (CoordXY_separ_piezas > 0.5 && CoordXY_separ_piezas < 1.5)
                CoordXY_separ_piezas = 1;
            else if (CoordXY_separ_piezas >= 1.5 && CoordXY_separ_piezas < 2.5)
                CoordXY_separ_piezas = 2;
            else if (CoordXY_separ_piezas >= 2.5 && CoordXY_separ_piezas < 3.5)
                CoordXY_separ_piezas = 3;
            else if (CoordXY_separ_piezas >= 3.5 && CoordXY_separ_piezas < 4.5)
                CoordXY_separ_piezas = 4;
            else if (CoordXY_separ_piezas >= 4.5 && CoordXY_separ_piezas < 5.5)
                CoordXY_separ_piezas = 5;
            else if (CoordXY_separ_piezas >= 5.5 && CoordXY_separ_piezas < 6.5)
                CoordXY_separ_piezas = 6;
            else if (CoordXY_separ_piezas >= 6.5 && CoordXY_separ_piezas < 7.5)
                CoordXY_separ_piezas = 7;

            //REDONDEA POSIBLES MOVIMIENTOS < 0
            else if ((CoordXY_separ_piezas < -0.5 && CoordXY_separ_piezas >= -1.5))
                CoordXY_separ_piezas = -1;
            else if ((CoordXY_separ_piezas < -1.5 && CoordXY_separ_piezas >= -2.5))
                CoordXY_separ_piezas = -2;
            else if ((CoordXY_separ_piezas < -2.5 && CoordXY_separ_piezas >= -3.5))
                CoordXY_separ_piezas = -3;
            else if ((CoordXY_separ_piezas < -3.5 && CoordXY_separ_piezas >= -4.5))
                CoordXY_separ_piezas = -4;
            else if ((CoordXY_separ_piezas < -4.5 && CoordXY_separ_piezas >= -5.5))
                CoordXY_separ_piezas = -5;
            else if ((CoordXY_separ_piezas < -5.5 && CoordXY_separ_piezas >= -6.5))
                CoordXY_separ_piezas = -6;
            else if ((CoordXY_separ_piezas < -6.5 && CoordXY_separ_piezas >= -7.5))
                CoordXY_separ_piezas = -7;

            return (byte)(CoordXY_separ_piezas);
        }
        
        protected char Convertir_Coordenada_X_a_Notación(byte X)
        {
            switch (X)
            {
                case 0:
                    return 'a';
                case 1:
                    return 'b';
                case 2:
                    return 'c';
                case 3:
                    return 'd';
                case 4:
                    return 'e';
                case 5:
                    return 'f';
                case 6:
                    return 'g';
                case 7:
                    return 'h';
                default:
                    return 'Z';

            }

        }

        private void Insertar_Movimientos_A_ListBox(Movimiento CXYIF, byte Codificacion_4)
        {
            
            //GUARDA MOVIMIENTOS EN COORDENADAS X_START Y_START X_END Y_END
            System.IO.FileStream Archivo_Lista_Movimientos_XY_FS = new FileStream("C:\\Lista_Movimientos\\Lista_Movimientos_XY.txt", FileMode.Append, FileAccess.Write);
            StreamWriter Archivo_Lista_Movimientos_XY = new StreamWriter(Archivo_Lista_Movimientos_XY_FS);
            string Linea_A_Insertar_Player_A_o_B_XY = Convert.ToString(m_cursor_position_X_Start) + " " 
                    + Convert.ToString(m_cursor_position_Y_Start) + " " + Convert.ToString(m_cursor_position_X_End) 
                    + " " + Convert.ToString(m_cursor_position_Y_End);
            Archivo_Lista_Movimientos_XY.WriteLine(Linea_A_Insertar_Player_A_o_B_XY);
            Archivo_Lista_Movimientos_XY.Close();

            //GUARDA MOVIMIENTOS EN NOTACIÓN DE AJEDREZ EJEMPLO E2 - E4
            System.IO.FileStream Archivo_Lista_Movimientos_FS = new FileStream("C:\\Lista_Movimientos\\Lista_Movimientos.txt", FileMode.Append, FileAccess.Write);
            StreamWriter Archivo_Lista_Movimientos = new StreamWriter(Archivo_Lista_Movimientos_FS);
            char x_inicial = Convertir_Coordenada_X_a_Notación((byte)CXYIF.X_Start);
            char x_final = Convertir_Coordenada_X_a_Notación((byte)CXYIF.X_End);
            string Linea_A_Insertar_Player_A_o_B = Convert.ToString(x_inicial) + (8 - CXYIF.Y_Start) + " - " + Convert.ToString(x_final) + (8-CXYIF.Y_End);
            Archivo_Lista_Movimientos.WriteLine(Linea_A_Insertar_Player_A_o_B);
            Archivo_Lista_Movimientos.Close();

            if (Obtener_Color_Pieza(Codificacion_4) == 1)
                m_ListBox_Player_A.Items.Add(Linea_A_Insertar_Player_A_o_B);
            else
                m_ListBox_Player_B.Items.Add(Linea_A_Insertar_Player_A_o_B);
        }

        public void SET_Desface_Incoherente(bool estado){ m_Desface_Incoherente_Y_Start = true ;}

        public int GET_Panel_0_Left() { return m_Panel[0].Left; }
        
        public int GET_Panel_0_Top() { return m_Panel[0].Top; }

        public bool SET_Click { set { m_click = value; } get { return m_click; }}

        public bool SET_Arrastrar{set { m_arrastrar = value; } get { return m_arrastrar; }}


        #endregion Métodos

    }
    
}

//CoordXYInicioFinal CXYIF = new CoordXYInicioFinal();

//CXYIF.X_Start = (sbyte)Redondea-rValor_SeparPiezas(m_mouseX_inicial);

/*
if (m_Desface_Incoherente_Y_Start == true)
{
    if (codificacion == 1)
    {
        CXYIF.X_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseX_inicial - 6);
        CXYIF.Y_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseY_inicial + 137);
        CXYIF.X_End = (sbyte)RedondearValor_SeparPiezas(m_mouseX_final + 2);
        CXYIF.Y_End = (sbyte)RedondearValor_SeparPiezas(m_mouseY_final + 2);
    }
    else
    {
        CXYIF.X_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseX_inicial + 3);
        CXYIF.Y_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseY_inicial - 131);
        CXYIF.X_End = (sbyte)RedondearValor_SeparPiezas(m_mouseX_final + 2);
        CXYIF.X_End = (sbyte)RedondearValor_SeparPiezas(m_mouseY_final + 2);
    }
}

else
{
*/

//}

/*
Point nextPosition = new Point();
nextPosition = m_Form1.PointToClient(System.Windows.Forms.Control.MousePosition);

nextPosition.X -= m_pos_X_Tablero + 24;//24
nextPosition.Y -= m_pos_Y_Tablero + 18;//18
nextPosition.Offset(m_mouseX, m_mouseY);

PanelX.Location = nextPosition;
//PanelX.Location = nextPosition;
m_Form1.Text = nextPosition.ToString();

m_cursor_position_X_End = Cursor.Position.X;
m_cursor_position_Y_End = Cursor.Position.Y;

m_mouseX_final = nextPosition.X;
m_mouseY_final = nextPosition.Y;
*/
/*
CXYIF.X_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseX_inicial);
CXYIF.Y_Start = (sbyte)RedondearValor_SeparPiezas(m_mouseY_inicial);
CXYIF.X_End = (sbyte)RedondearValor_SeparPiezas(m_mouseX_final);
CXYIF.Y_End = (sbyte)RedondearValor_SeparPiezas(m_mouseY_final);
*/