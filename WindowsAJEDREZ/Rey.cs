﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    class Rey : Pieza
    {
 
        private bool m_rey_ya_se_movio_blanco;
        private bool m_rey_ya_se_movio_negro;
        private bool m_jakeado_blanco;
        private bool m_jakeado_negro;
        public bool m_valid_enroqu_cort_blan;
        public bool m_valid_enroqu_larg_blan;
        public bool m_valid_enroqu_cort_negr;
        public bool m_valid_enroqu_larg_negr;

        public Rey() 
        {
            m_valid_enroqu_cort_blan = false;
            m_valid_enroqu_larg_blan = false;
            m_valid_enroqu_cort_negr = false;
            m_valid_enroqu_larg_negr = false;
            m_rey_ya_se_movio_blanco = false;
            m_rey_ya_se_movio_negro = false;
            m_jakeado_blanco = false;
            m_jakeado_negro = false;
        }

        public Movimiento ValidaMovimiento(Movimiento CXYIF, Tablero TTablero, 
            Torre TTorre, byte Cod)
        {
            Movimiento valido = CXYIF;
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UN REY A TRAVES DE LA CODIFICACION
            if (Cod == ObtenereCodificación(true) || Cod == ObtenereCodificación(false))
            {
                
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;
                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTO ( || || || || ) && (  || || ) && && && && 
                if ((dif_X == 1 || dif_X == -1 || dif_Y == 1 || dif_Y == -1) 
                    && (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(Cod) 
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                    && dif_X >= -1 && dif_X <= 1 && dif_Y >= -1 && dif_Y <= 1 
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                    && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false))
                {
                    //if (CXYIF.jaque == true) CXYIF.jaque = false;
                    CXYIF.jaque = false;
                    if (Obtener_Color_Pieza(Cod) == 1)
                        m_rey_ya_se_movio_blanco = true;
                    else
                        m_rey_ya_se_movio_negro = true;
                    valido.MOVIO = true;
                    //COMIO LA FICHA PORQUE LA POSICION EN QUE SE MOVIO HABÍA UNA FICHA ES DECIR ERA DISTINTO DE CERO
                    if (PosFichaAComer(CXYIF, TTablero) != 0)
                        valido.COMIO = true;
                } //VALIDA ENROQUE
                else if (ValidaEnroque(TTorre, CXYIF, TTablero, Cod))
                {
                    if (Obtener_Color_Pieza(Cod) == 1)
                        m_rey_ya_se_movio_blanco = true;
                    else
                        m_rey_ya_se_movio_negro = true;
                    valido.MOVIO = true;
                    valido.ENROQUE = true;
                }
            }
            return valido;
        }
        
        public Movimiento ValidaMovimientoYMueveRey(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, Torre TTorre, Tablero_Index TTablero_Index)
        {
            Movimiento valido = CXYIF;

            byte PanelXTagbyte = (byte)PanelX.Tag;

            ////VERIFICA QUE LA FICHA CLICKEADA SEA UN REY A TRAVES DE LA CODIFICACION
            if (PanelXTagbyte == ObtenereCodificación(true) || PanelXTagbyte == ObtenereCodificación(false))
            {
                Cod_Nro dato = new Cod_Nro();
                dato.Cod = (byte)PanelX.Tag;
                dato.Nro = Convert.ToByte(PanelX.Name);

                
                
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;

                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTO ( || || || || ) && (  || || ) && && && && 
                if ((dif_X == 1 || dif_X == -1 || dif_Y == 1 || dif_Y == -1) && 
                    (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(dato.Cod) 
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                    && dif_X >= -1 && dif_X <= 1 && dif_Y >= -1 && dif_Y <= 1 
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                    && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero,CXYIF,dato.Cod) == false))
                {
                    //if (CXYIF.jaque == true) CXYIF.jaque = false;
                    //MUEVE PIEZA
                    CXYIF.jaque = false;
                    Mover_Solo_Piezas_Paneles(PanelX, CXYIF);

                    if (Obtener_Color_Pieza(dato.Cod) == 1)
                        m_rey_ya_se_movio_blanco = true;
                    else
                        m_rey_ya_se_movio_negro = true;

                    CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                    valido.MOVIO = true;

                    //COMIO LA FICHA PORQUE LA POSICION EN QUE SE MOVIO HABÍA UNA FICHA ES DECIR ERA DISTINTO DE CERO
                    if (PosFichaAComer(CXYIF, TTablero) != 0)
                    {
                        valido.COMIO = true;
                        valido.Index_Del_Panel = Index_Ficha_A_Comer(CXYIF, TTablero_Index);
                    }

                    //MODIFICA EL TABLERO
                    ModificaClaseTablero(CXYIF, dato, TTablero, TTablero_Index);

                } //VALIDA ENROQUE
                else if (ValidaEnroque(TTorre,CXYIF,TTablero, dato.Cod))
                {
                    Mover_Solo_Piezas_Paneles(PanelX, CXYIF);

                    if (Obtener_Color_Pieza(dato.Cod) == 1)
                        m_rey_ya_se_movio_blanco = true;
                    else
                        m_rey_ya_se_movio_negro = true;

                    CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                    ModificaClaseTablero(CXYIF, dato, TTablero,TTablero_Index);

                    valido.MOVIO = true;
                    valido.ENROQUE = true;
                }
                else //MOVIMIENTO NOOOOOOOOO VALIDO
                {
                    CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);
                }
            }
            return valido;
        }

        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte Rey = m_CodificacionPiezas.KN();
            if (es_blanca ==true)
                Rey = m_CodificacionPiezas.KB();

            return Rey;
        }

        public override bool VerificarCaminoOcupado(Movimiento CXYIF, Tablero TTablero)
        {
            bool ocupado = false;
            sbyte x_inicial = (sbyte)CXYIF.X_Start;
            sbyte x_final = (sbyte)CXYIF.X_End;
            if (x_final - x_inicial > 0)
            {
                for (byte i = (byte)(x_inicial + 1); i <= x_final && ocupado == false; i++)
                    if (TTablero.GET_CodFicha(i, (byte)CXYIF.Y_End) != 0)
                        ocupado = true;
            }
            else if (x_final - x_inicial < 0)
            {
                for (byte i = (byte)(x_inicial - 1); i >= x_final && ocupado == false; i--)
                    if (TTablero.GET_CodFicha(i, (byte)CXYIF.Y_End) != 0)
                        ocupado = true;
            }
            return ocupado;

        }

        public bool ValidaEnroque(Torre TTorre, Movimiento CXYIF, Tablero TTablero, byte Codificacion)
        {
            bool valida = false;
            float dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
            float dif_Y = CXYIF.Y_End - CXYIF.Y_Start;
            if (dif_Y == 0)
            {
                if (Obtener_Color_Pieza(Codificacion) == 1) //ES BLANCO 
                {
                    if (m_rey_ya_se_movio_blanco == false && m_jakeado_blanco == false && VerificarCaminoOcupado(CXYIF, TTablero) == false)
                    {
                        if (dif_X == 2 && TTorre.m_torre_movio_blan_der == false && CXYIF.Y_Start == 7)
                        {
                            m_valid_enroqu_cort_blan = true;
                            valida = true;
                        }
                        else if (dif_X == -2 && TTorre.m_torre_movio_blan_izq == false && CXYIF.Y_Start == 7)
                        {
                            m_valid_enroqu_larg_blan = true;
                            valida = true;
                        }
                    }
                }
                else //ES NEGRO
                {
                    if (m_rey_ya_se_movio_negro == false && m_jakeado_negro == false && VerificarCaminoOcupado(CXYIF, TTablero) == false)
                    {
                        if (dif_X == -2 && TTorre.m_torre_movio_negr_izq == false && CXYIF.Y_Start == 0)
                        {
                            m_valid_enroqu_larg_negr = true;
                            valida = true;
                        }
                        else if (dif_X == 2 && TTorre.m_torre_movio_negr_der == false && CXYIF.Y_Start == 0)
                        {
                            m_valid_enroqu_cort_negr = true;
                            valida = true;
                        }
                    }
                }
            }
             
            
            return valida;
        }

        

    }
}
