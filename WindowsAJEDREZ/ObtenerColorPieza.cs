﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chess
{
    struct ObtenerColorPieza
    {
        public byte Obtener_Color_Pieza(byte Codificacion)
        {
            return (byte)(Codificacion & 1);
            
        }
    }
}
