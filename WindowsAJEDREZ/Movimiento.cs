﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    public class Movimiento :Funciones
    {
        public sbyte X_Start;
        public sbyte Y_Start;
        public sbyte X_End;
        public sbyte Y_End;

        public byte Cod;
        public byte Pieza_Comida = 0;
        //public byte Color_Pieza_Movida;

        public bool COMIO;
        public bool MOVIO;
        public bool ENROQUE;
        public int VALOR_TABLERO;
        public bool CORONO = false;

        public bool jaque;

        public byte Index_Del_Panel;

        public Movimiento CopyTo()
        {
            Movimiento copia = new Movimiento();
            return copia = (Movimiento)this.MemberwiseClone();
        }
    }
}
