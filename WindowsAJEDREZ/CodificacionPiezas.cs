﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chess
{
    public struct CodificacionPiezas
    {        

        ///FICHAS BLANCAS
        public byte PB() { return 15 ; } //1111
        public byte CB() { return 3; } //0011
        public byte TB() { return 5; } //0101
        public byte AB() { return 7; } //0111
        public byte QB() { return 9; } //1001
        public byte KB() { return 11; }  //1011
        // FICHAS NEGRAS
        public byte PN() { return 14; } //1110
        public byte CN() { return 2; } //0010
        public byte TN() { return 4; } //0100
        public byte AN() { return 6; } //0110
        public byte QN() { return 8; } //1000
        public byte KN() { return 10; }   //1010
    }

    
}
