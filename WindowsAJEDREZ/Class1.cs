using System;
using System.Collections.Generic;
using System.Text;

namespace Chess
{
    class Class1
    {

        public byte PeonB()
        {

            byte pb = new byte();

            pb = (byte)(pb | 1); //0000 0001
            pb = (byte)(pb << 1); //0000 0010
            pb = (byte)(pb | 1); //0000 0011
            pb = (byte)(pb << 1); // 0000 0110
            pb = (byte)(pb | 1); // 0000 0111
            pb = (byte)(pb << 1); // 0000 1110
            pb = (byte)(pb | 1); //0000 1111
            return pb;

        }

        public byte CaballoB()
        {
            byte CB = new byte();
            CB = (byte)(CB |1); //  0000 0001
            CB = (byte)(CB <<1); // 0000 0010
            CB = (byte)(CB |1); //0000 0011
            return CB;
        }


        public byte TorreB()
        {
            byte TB = new byte();
            TB = (byte)(TB |1);  // 0000 0001
            TB = (byte)(TB <<2); // 0000 0100
            TB = (byte)(TB |1); // 0000 0101
            return TB;
        }


        public byte AlfilB()
        {
            byte AB = new byte();
            AB = (byte)(AB |1); //  0000 0001
            AB = (byte)(AB <<1); // 0000 0010
            AB = (byte)(AB |1); // 0000 0011
            AB = (byte)(AB <<1); // 0000 0110
            AB = (byte)(AB |1); // 0000 0111
            return AB;
        }


        public byte ReinaB()
        {
            byte QB = new byte();
            QB = (byte)(QB |1);  //  0000 0001
            QB = (byte)(QB <<3); // 0000 1000
            QB = (byte)(QB |1); // 0000 1001
            return QB;
        }


        public byte ReyB()
        {
            byte KB = new byte();
            KB = (byte)(KB |1); //  0000 0001
            KB = (byte)(KB <<2); // 0000 0100
            KB = (byte)(KB |1); // 0000 0101
            KB = (byte)(KB <<1); // 0000 1010
            KB = (byte)(KB |1); // 0000 1011
            return KB;
        }

        // FICHAS NEGRAS

        public byte PeonN()
        {
            byte PN = new byte();
            PN = (byte)(PN |1); //  0000 0001
            PN = (byte)(PN <<1);// 00000010
            PN = (byte)(PN |1);//0000 0011
            PN = (byte)(PN <<1); //0000 0110
            PN = (byte)(PN |1); //0000 0111
            PN = (byte)(PN <<1);  //0000 1110
            return PN;
        }


        public byte CaballoN()
        {
            byte CN = new byte();
            CN = (byte)(CN |1); //  0000 0001
            CN = (byte)(CN <<1); // 0000 0010
            return CN;
        }


        public byte TorreN()
        {
            byte TN = new byte();
            TN = (byte)(TN |1);  // 0000 0001
            TN = (byte)(TN <<2); // 0000 0100
            return TN;
        }


        public byte AlfilN()
        {
            byte AN = new byte();
            AN = (byte)(AN |1); //  0000 0001
            AN = (byte)(AN <<1); // 0000 0010
            AN = (byte)(AN |1); // 0000 0011
            AN = (byte)(AN <<1); // 0000 0110
            return AN;
        }


        public byte ReinaN()
        {
            byte QN = new byte();
            QN = (byte)(QN |1);  //  0000 0001
            QN = (byte)(QN <<3); // 0000 1000
            return QN;
        }


        public byte ReyN()
        {
            byte KN = new byte();
            KN = (byte)(KN |1); //  0000 0001
            KN = (byte)(KN << 2); // 0000 0100
            KN = (byte)(KN | 1); // 0000 0101
            KN = (byte)(KN << 1); // 0000 1010
            return KN;
}
    }
}
