﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    class Peon : Pieza
    {

        public override Movimiento ValidaMovimiento(Movimiento CXYIF, Tablero TTablero, 
            byte Cod)
        {
            Movimiento valido = CXYIF;
            //VERIFICA QUE LA FICHA CLICKEADA SEA UN PEON A TRAVES DE LA CODIFICACION
            if (Cod == ObtenereCodificación(false) || Cod == ObtenereCodificación(true))
            {
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;
                if (Obtener_Color_Pieza(Cod) == 0) //NEGRA
                {
                    int PosY_AComer = (CXYIF.Y_Start) + 1;
                    if (PosY_AComer <= 7)
                    {
                        //bool pieza_mueve_mismo_color_hace_jaque = false;
                        //VALIDA MOVIMIENTOS PARA AVANZAR
                        if ((((dif_Y == 1 && dif_X == 0) || (dif_Y == 2 && dif_X == 0 
                            && CXYIF.Y_Start == 1))) 
                            && Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF) 
                            && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false)
                            && !VerificarCaminoOcupado(CXYIF, TTablero))
                        {
                            //Verifica_SI_Hace_Jaque_Al_Rey(CXYIF, TTablero);
                            valido.MOVIO = true;
                            if (CXYIF.Y_End == 7)
                                valido.CORONO = true;
                        } //VALIDA MOVIMIENTOS PARA COMER
                        else if (((dif_X == 1 || dif_X == -1) && dif_Y == 1 
                            && Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) == 1) 
                            && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                            && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false))
                        {
                            valido.COMIO = true;
                            valido.MOVIO = true;
                            if (CXYIF.Y_End == 7)
                                valido.CORONO = true;
                        }
                    }
                } //VALIDA MOVIMIENTOS PARA COMER
                else //ES BLANCA
                {
                    int PosY_AComer = CXYIF.Y_Start - 1;
                    if (PosY_AComer >= 0)
                    {
                        byte y = (byte)((int)PosY_AComer);
                        //bool pieza_mueve_mismo_color_hace_jaque = false;
                        //VALIDA MOVIMIENTOS PARA AVANZAR
                        if (((dif_Y == -1 && dif_X == 0) || (dif_Y == -2 && dif_X == 0 
                            && CXYIF.Y_Start == 6)) 
                            && Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF) 
                            && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false) 
                            && !VerificarCaminoOcupado(CXYIF, TTablero))
                        {
                            valido.MOVIO = true;
                            if (CXYIF.Y_End == 0)
                                valido.CORONO = true;
                        } //VALIDA PARA COMER PIEZAS
                        else if (((dif_X == 1 || dif_X == -1) && dif_Y == -1 
                            && Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) == 0 
                            && PosFichaAComer(CXYIF, TTablero) != 0) 
                            && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                            && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false))
                        {
                            if (CXYIF.Y_End == 0)
                                valido.CORONO = true;
                            valido.COMIO = true;
                            valido.MOVIO = true;
                        }
                    }
                }
            }
            return valido;
        }

        public override Movimiento ValidaMovimientoYMueve(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, 
            Tablero_Index TTablero_Index)
        {
            Movimiento valido = CXYIF;
            Cod_Nro dato = new Cod_Nro();

            dato.Cod = (byte)PanelX.Tag;
            dato.Nro = Convert.ToByte(PanelX.Name);
            
            //VERIFICA QUE LA FICHA CLICKEADA SEA UN PEON A TRAVES DE LA CODIFICACION
            if (dato.Cod == ObtenereCodificación(false) || dato.Cod == ObtenereCodificación(true))
            {

                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;

                if (Obtener_Color_Pieza(dato.Cod) == 0) //NEGRA
                {
                    int PosY_AComer = (CXYIF.Y_Start) + 1;
                    if (PosY_AComer <= 7)
                    {
                        //bool pieza_mueve_mismo_color_hace_jaque = false;
                        //VALIDA MOVIMIENTOS PARA AVANZAR
                        if ((((dif_Y == 1 && dif_X == 0) || (dif_Y == 2 && dif_X == 0 && CXYIF.Y_Start == 1))) 
                            && Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF) 
                            && TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false && !VerificarCaminoOcupado(CXYIF,TTablero))
                        {
                            //MUEVE LA PIEZA
                            //Verifica_SI_Hace_Jaque_Al_Rey(CXYIF, TTablero);
                            Mover_Solo_Piezas_Paneles(PanelX, CXYIF);
                            CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                            valido.MOVIO = true;
                        } //VALIDA MOVIMIENTOS PARA COMER
                        else if (((dif_X == 1 || dif_X == -1) && dif_Y == 1 && 
                            Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) == 1) && 
                            VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) && 
                            (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false))
                        {
                            //COME LA PIEZA
                            Mover_Solo_Piezas_Paneles(PanelX, CXYIF);
                            CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                            valido.COMIO = true;
                            valido.MOVIO = true;
                            valido.Index_Del_Panel = Index_Ficha_A_Comer(CXYIF, TTablero_Index);
                        }
                        else //MOVIMIENTO NOOOOOOOOO VALIDO
                        {
                            CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);

                        }
                    }
                    else
                    {
                        CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);
                    }
                    
                } //VALIDA MOVIMIENTOS PARA COMER
                else //ES BLANCA
                {
                    
                    int PosY_AComer = CXYIF.Y_Start - 1;
                    if (PosY_AComer >= 0)
                    {
                        byte y = (byte)((int)PosY_AComer);
                        //bool pieza_mueve_mismo_color_hace_jaque = false;
                        //VALIDA MOVIMIENTOS PARA AVANZAR
                        if (((dif_Y == -1 && dif_X == 0) || (dif_Y == -2 && dif_X == 0 && CXYIF.Y_Start == 6)) 
                            && Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF) 
                            && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false)
                            && !VerificarCaminoOcupado(CXYIF, TTablero))
                        {
                            //MUEVE LA PIEZA
                            Mover_Solo_Piezas_Paneles(PanelX, CXYIF);
                            CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                            valido.MOVIO = true;
                            
                        } //VALIDA PARA COMER PIEZAS
                        else if (((dif_X == 1 || dif_X == -1) && dif_Y == -1 && Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) == 0 && PosFichaAComer(CXYIF, TTablero) != 0) && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false))
                        {
                            //COME LA PIEZA
                            Mover_Solo_Piezas_Paneles(PanelX, CXYIF);
                            CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                            valido.COMIO = true;
                            valido.MOVIO = true;
                            valido.Index_Del_Panel = Index_Ficha_A_Comer(CXYIF, TTablero_Index);

                        }
                        else //MOVIMIENTO NOOOOOOOOO VALIDO
                            CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);
                    }
                    else
                    {
                        CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);                    
                    }
                }



            }

            //MODIFICA EL TABLERO
            if (valido.MOVIO == true || valido.COMIO == true)
            {
                ModificaClaseTablero(CXYIF, dato, TTablero, TTablero_Index);

            }

            return valido;
        }

        public override bool VerificarCaminoOcupado(Movimiento CXYIF, Tablero TTablero)
        {
            bool ocupado = false;
            if (Obtener_Color_Pieza(TTablero.GET_CodFicha((byte)CXYIF.X_Start, (byte)CXYIF.Y_Start)) == 0) //NEGRAS
            {
                if (CXYIF.Y_Start == 1)
                {
                    if (!TTablero.EsCasillaVacia((byte)CXYIF.X_Start, (byte)(CXYIF.Y_Start + 1)))
                        ocupado = true;
                }
            }
            else
            {
                if (CXYIF.Y_Start == 6)
                {
                    if (!TTablero.EsCasillaVacia((byte)CXYIF.X_Start, (byte)(CXYIF.Y_Start - 1)))
                        ocupado = true;
                }
            }

            return ocupado;
        }

        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte peon = m_CodificacionPiezas.PN();
            if (es_blanca == true)
                peon = m_CodificacionPiezas.PB();

            return peon;
        }


    }
}

