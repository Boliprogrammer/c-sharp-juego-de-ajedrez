﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;


namespace Chess
{
    public class Torre: Pieza
    {

        public bool m_torre_movio_negr_der;
        public bool m_torre_movio_negr_izq;
        public bool m_torre_movio_blan_izq;
        public bool m_torre_movio_blan_der;

        public Torre()
        { 
            m_torre_movio_negr_der = false;
            m_torre_movio_negr_izq = false; 
            m_torre_movio_blan_izq = false; 
            m_torre_movio_blan_der = false; 
        }
        public override Movimiento ValidaMovimiento(Movimiento CXYIF, Tablero TTablero, 
                byte Cod)
        {
            Movimiento valido = CXYIF;
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UNA TORRE A TRAVES DE LA CODIFICACION
            if (Cod == ObtenereCodificación(true) || Cod == ObtenereCodificación(false))
            {
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start;// y_destino - y_inicial;
                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTO (a||b)&&(c||d)
                if (((dif_Y != 0 && dif_X == 0) || (dif_X != 0 && dif_Y == 0)) &&
                    (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(Cod)
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF)))
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero))
                    && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false))
                {
                    if (VerificarCaminoOcupado(CXYIF, TTablero) == false)
                    {
                        ///VERIFICA SI LAS TORRES YA SE MOVIO AL MENOS UNA VEZ
                        ///PARA PERMITIR O NO ENROQUE
                        Ya_Se_Movio(CXYIF);
                        valido.MOVIO = true;
                        if (PosFichaAComer(CXYIF, TTablero) != 0)
                            valido.COMIO = true;
                    }
                }
            }
            return valido;
        }

        public override Movimiento ValidaMovimientoYMueve(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, Tablero_Index TTablero_Index)
        {
            Movimiento valido = CXYIF;
            Cod_Nro dato = new Cod_Nro();
            dato.Cod = (byte)PanelX.Tag;
            dato.Nro = Convert.ToByte(PanelX.Name);
            
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UNA TORRE A TRAVES DE LA CODIFICACION
            if (dato.Cod == ObtenereCodificación(true) || dato.Cod == ObtenereCodificación(false))
            {

                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start;// y_destino - y_inicial;

                // VALIDA QUE EL MOVIMIENTO NO SE SALGA DEL TABLERO (e&&f)
                if ((CXYIF.Y_End <= 7 && CXYIF.X_End <= 7))
                {
                    byte Codificacion = (byte)PanelX.Tag;
                    //bool pieza_mueve_mismo_color_hace_jaque = false;
                    //VALIDA MOVIMIENTO (a||b)&&(c||d)
                    if (((dif_Y != 0 && dif_X == 0) || (dif_X != 0 && dif_Y == 0)) && 
                        (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(Codificacion) 
                        || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                        && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                        && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false))
                    {
                        //VERIFICA SI EL CAMINO ESTÁ OCUPADO PARA PODER AVANZAR
                        if (VerificarCaminoOcupado(CXYIF, TTablero) == false)
                        {
                            //MUEVE PIEZA
                            MoverPieza(PanelX, CXYIF, valido, dato.Cod);

                            //VERIFICA SI LAS TORRES YA SE MOVIO AL MENOS UNA VEZ
                            Ya_Se_Movio(CXYIF);
                            CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                            valido.MOVIO = true;
                            
                            if (PosFichaAComer(CXYIF,TTablero) != 0)
                            {
                                valido.COMIO = true;
                                valido.Index_Del_Panel = Index_Ficha_A_Comer(CXYIF, TTablero_Index);
                            }

                            //MODIFICA LA CLASE  TABLERO
                            if (PanelX.Name == Convert.ToString(16) || PanelX.Name == Convert.ToString(17) || PanelX.Name == Convert.ToString(22) || PanelX.Name == Convert.ToString(23))
                                ModificaClaseTablero(CXYIF, dato, TTablero, TTablero_Index);
                        }
                        else
                        {
                            if (PanelX.Name == Convert.ToString(16) || PanelX.Name == Convert.ToString(17) || PanelX.Name == Convert.ToString(22) || PanelX.Name == Convert.ToString(23))
                                CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);

                        }
                    }

                    else //MOVIMIENTO NOOOOOOOOO VALIDO
                    {
                        if (PanelX.Name == Convert.ToString(16) || PanelX.Name == Convert.ToString(17) || PanelX.Name == Convert.ToString(22) || PanelX.Name == Convert.ToString(23))
                            CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);

                    }
                }
                else //MOVIMIENTO NOOOOOOOOO VALIDO
                {
                    if (PanelX.Name == Convert.ToString(16) || PanelX.Name == Convert.ToString(17) || PanelX.Name == Convert.ToString(22) || PanelX.Name == Convert.ToString(23))
                        CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);

                }

                
            }
            return valido;
        }

        public void MoverPieza(System.Windows.Forms.Panel PanelFichaX, Movimiento CXYIF, 
            Movimiento DesplazamientoValido, byte Codificacion)
        {
            sbyte click_x = CXYIF.X_Start;
            sbyte click_y = CXYIF.Y_Start;
            sbyte desplazamiento_X = (sbyte)(CXYIF.X_End - CXYIF.X_Start);
            sbyte desplazamiento_Y = (sbyte)(CXYIF.Y_End - CXYIF.Y_Start);

            Mover_Solo_Piezas_Paneles(PanelFichaX, CXYIF);
            
            //AYUDA PARA EL ENROQUE Y NOS DICE CUAL TORRE SE MOVIO
            if (Convertir_PosXY_NROcasilla_64((byte)click_x, (byte)click_y) == 23)
                m_torre_movio_blan_der = true;
            if (Convertir_PosXY_NROcasilla_64((byte)click_x, (byte)(click_y)) == 22)
                m_torre_movio_blan_izq = true;
            if (Convertir_PosXY_NROcasilla_64((byte)click_x, (byte)click_y) == 16)
                m_torre_movio_negr_izq = true;
            if (Convertir_PosXY_NROcasilla_64((byte)click_x, (byte)click_y) == 17)
                m_torre_movio_negr_der = true;
        }

        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte Torre = m_CodificacionPiezas.TN();
            if (es_blanca == true)
                Torre = m_CodificacionPiezas.TB();

            return Torre;
        }

        public override bool VerificarCaminoOcupado(Movimiento CXYIF, Tablero TTablero) 
        {
            bool ocupado = false;
            if (CXYIF.X_End - CXYIF.X_Start >= 0)
            {
                for (sbyte i = (sbyte)(CXYIF.X_Start + 1); i <= (sbyte)(CXYIF.X_End - 1) && ocupado == false; i++)
                {
                    if (TTablero.EsCasillaVacia((byte)i, (byte)CXYIF.Y_End) == false)
                        ocupado = true;
                }
            }
            else
            {
                for (sbyte i = (sbyte)(CXYIF.X_Start -1); i >= (sbyte)(CXYIF.X_End + 1) && ocupado == false; i--)
                {
                    if (TTablero.EsCasillaVacia((byte)i, (byte)CXYIF.Y_End) == false)
                        ocupado = true;
                }
            }
            if (CXYIF.Y_End - CXYIF.Y_Start >= 0)
            {
                for (sbyte i = (sbyte)(CXYIF.Y_Start + 1); i <= (sbyte)(CXYIF.Y_End - 1) && ocupado == false; i++)
                {
                    if (TTablero.EsCasillaVacia((byte)CXYIF.X_End, (byte)i) == false)
                        ocupado = true;
                }
            }
            else
            {
                for (sbyte i = (sbyte)(CXYIF.Y_Start - 1); i >= (sbyte)(CXYIF.Y_End + 1) && ocupado == false; i--)
                {
                    if (TTablero.EsCasillaVacia((byte)CXYIF.X_End, (byte)i) == false)
                        ocupado = true;
                }
            }
            return ocupado;
        }

        public void Ya_Se_Movio(Movimiento CXYIF)
        {
            byte x = (byte)CXYIF.X_Start;
            byte y = (byte)CXYIF.Y_Start;
            if (Convertir_PosXY_NROcasilla_64(x,y) == 23)
                m_torre_movio_blan_der = true;
            else if (Convertir_PosXY_NROcasilla_64(x, y) ==22)
                m_torre_movio_blan_izq = true;
            else if (Convertir_PosXY_NROcasilla_64(x, y) == 23)
                m_torre_movio_negr_der = true;
            else if (Convertir_PosXY_NROcasilla_64(x, y) == 16)
                m_torre_movio_negr_izq = true;
        }

    }
}
