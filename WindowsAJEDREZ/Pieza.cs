﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    public class Pieza:Funciones
    {
        #region Variables
        protected CodificacionPiezas m_CodificacionPiezas;
        #endregion Variables

        #region Métodos

        public virtual byte ObtenereCodificación(bool es_blanca) { return 0; }

        public virtual Movimiento ValidaMovimiento(Movimiento CXYIF, Tablero TTablero, byte Cod)
        {
            Movimiento sobrecargado = new Movimiento();
            return sobrecargado;
        }

        public virtual Movimiento ValidaMovimientoYMueve(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, Tablero_Index TTablero_Index) 
        {
            Movimiento xy = new Movimiento() ;
            return xy;
        }

        public virtual bool VerificarCaminoOcupado(Movimiento CXYIF, Tablero TTablero)
        {
            bool ocupado = false;
            if (CXYIF.X_End - CXYIF.X_Start >= 0)
            {
                for (sbyte i = (sbyte)(CXYIF.X_Start + 1); i <= (CXYIF.X_End - 1) && ocupado == false; i++)
                {
                    if (TTablero.EsCasillaVacia((byte)i, (byte)CXYIF.Y_End) == false)
                        ocupado = true;
                }
            }
            else
            {
                for (sbyte i = (sbyte)(CXYIF.X_Start - 1); i >= (CXYIF.X_End + 1) && ocupado == false; i--)
                {
                    if (TTablero.EsCasillaVacia((byte)i, (byte)CXYIF.Y_End) == false)
                        ocupado = true;
                }
            }
            if (CXYIF.Y_End - CXYIF.Y_Start >= 0)
            {
                for (sbyte i = (sbyte)(CXYIF.Y_Start + 1); i <= (CXYIF.Y_End - 1) && ocupado == false; i++)
                {
                    if (TTablero.EsCasillaVacia((byte)CXYIF.X_End, (byte)i) == false)
                        ocupado = true;
                }
            }
            else
            {
                for (sbyte i = (sbyte)(CXYIF.Y_Start - 1); i >= (CXYIF.Y_End + 1) && ocupado == false; i--)
                {
                    if (TTablero.EsCasillaVacia((byte)CXYIF.X_End, (byte)i) == false)
                        ocupado = true;
                }
            }
            return ocupado;
        }

        public void Mover_Solo_Piezas_Paneles(System.Windows.Forms.Panel PanelFichaX, Movimiento CXYIF)
        {
            PanelFichaX.Top = Separacion_Piezas_Y() * CXYIF.Y_End;
            PanelFichaX.Left = Separacion_Piezas_X() * CXYIF.X_End;
        }
        public void CuandoMovimientoPiezasNOValido(ref bool m_Dragging, System.Windows.Forms.Panel PanelX, 
            System.Windows.Forms.Panel Panel_0, System.Windows.Forms.Form Form, Movimiento CXYIF)
        {
            m_Dragging = false;
            PanelX.Parent = Panel_0;
            PanelX.Left = CXYIF.X_Start*66;
            PanelX.Top = CXYIF.Y_Start*66;
            System.Windows.Forms.Cursor.Clip = System.Drawing.Rectangle.Empty;
            PanelX.Invalidate();
            Form.Cursor = System.Windows.Forms.Cursors.Default;
        }
        public void CambioInterfCuandoMovValido(ref bool m_Dragging, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel PanelX)
        {
            m_Dragging = false;
            System.Windows.Forms.Cursor.Clip = System.Drawing.Rectangle.Empty;
            PanelX.Invalidate();
            m_Form.Cursor = System.Windows.Forms.Cursors.Default;
        }
                
        public void ModificaClaseTablero(Movimiento CXYIF, Cod_Nro Codificacion, Tablero TTablero, 
            Tablero_Index  TTablero_Index)
        {
            //MODIFICA EL TABLERO
            
            byte x_final = (byte)CXYIF.X_End;
            byte y_final = (byte)CXYIF.Y_End;
            byte x_incial =  (byte)CXYIF.X_Start;
            byte y_incial = (byte)CXYIF.Y_Start;

            //INSERTA LA NUEVA FICHA EN LA NUEVA POSICION
            TTablero.SET_CodFicha(x_final, y_final, Codificacion.Cod);
            TTablero_Index.SET_Ficha(x_final, y_final, Codificacion.Nro);             
            
            //INSERTA "0" O VACIO EN LA POSICIÓN QUE DEJÓ LA FICHA
            TTablero.SET_CodFicha(x_incial,y_incial, 0);
            TTablero_Index.SET_Ficha(x_incial, y_incial, 0);


        }


        public bool VerificaQueNOSeaRey(byte FichaAComer) 
        {
            if (FichaAComer != m_CodificacionPiezas.KN() && FichaAComer != m_CodificacionPiezas.KB())
                return true;
            else
                return false;
        }
        public bool Verifica_SIEsCasillaVacia_EnPosAMoverse(Tablero TTablero, Movimiento CXYIF)
        {
            if (TTablero.EsCasillaVacia((byte)CXYIF.X_End, (byte)CXYIF.Y_End))
                return true;
            else
                return false;

        }
        public byte PosFichaAComer(Movimiento CXYIF, Tablero TTablero){return TTablero.GET_CodFicha((byte)CXYIF.X_End, (byte)CXYIF.Y_End);}

        public byte Index_Ficha_A_Comer(Movimiento CXYIF, Tablero_Index TTablero_Index){return TTablero_Index.GET_Ficha((byte)CXYIF.X_End, 
            (byte)CXYIF.Y_End);}

        /* CODIGO -> VERIFICA SI POS A MOVERSE ESTA EN JAQUE ESTÁ EN -> CLASE TABLERO
        protected bool Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(ref bool pieza_mueve_mismo_color_hace_jaque, Tablero TTablero, CoordXYInicioFinal CXYIF, byte cod_4)
        {
            byte Pieza_Que_Hace_Jaque = 0;
            pieza_mueve_mismo_color_hace_jaque = false;
            bool jaque = false;
            byte Ficha_Q_hAY_En_Pos_A_Moverse = TTablero.GET_CodFicha((byte)(CXYIF.x_final), (byte)(CXYIF.y_final));

            TTablero.SET_CodFicha((byte)(CXYIF.x_final), (byte)(CXYIF.y_final), cod_4);
            TTablero.SET_CodFicha((byte)(CXYIF.x_inicial), (byte)(CXYIF.y_inicial), 0);
            
            jaque = TTablero.Verifica_SI_Hay_Jaque_En_El_Tablero(ref Pieza_Que_Hace_Jaque);

            if (Obtener_Color_Pieza(Pieza_Que_Hace_Jaque) == Obtener_Color_Pieza(cod_4)) pieza_mueve_mismo_color_hace_jaque = true;
            

            TTablero.SET_CodFicha((byte)(CXYIF.x_final), (byte)(CXYIF.y_final),Ficha_Q_hAY_En_Pos_A_Moverse);
            TTablero.SET_CodFicha((byte)(CXYIF.x_inicial), (byte)(CXYIF.y_inicial), cod_4);


            return jaque;
        }
        
        
        
        protected bool Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(Tablero TTablero, CoordXYInicioFinal CXYIF, byte cod_4, bool cuando_esta_en_jaque)
        {
            byte Pieza_Que_Hace_Jaque = 0;
            bool jaque = false;
            if (cuando_esta_en_jaque == true)
            {

                TTablero.SET_CodFicha((byte)(CXYIF.x_final), (byte)(CXYIF.y_final), cod_4);
                TTablero.SET_CodFicha((byte)(CXYIF.x_inicial), (byte)(CXYIF.y_inicial), 0);
                
                jaque = TTablero.Verifica_SI_Hay_Jaque_En_El_Tablero(ref Pieza_Que_Hace_Jaque);

                TTablero.SET_CodFicha((byte)(CXYIF.x_final), (byte)(CXYIF.y_final), 0);
                TTablero.SET_CodFicha((byte)(CXYIF.x_inicial), (byte)(CXYIF.y_inicial), cod_4);
            }
            return jaque;
        }
*/
          
        #endregion Métodos

    }
}
