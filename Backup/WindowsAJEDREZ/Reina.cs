﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    class Reina: Pieza
    {

        public override Movimiento ValidaMovimiento(Movimiento CXYIF, Tablero TTablero,
                byte Cod)
        {
            byte Cod_Aux;
            Movimiento movio = CXYIF;
            if (Cod == ObtenereCodificación(true) || Cod == ObtenereCodificación(false))
            {
                Tablero copia_Tablero = TTablero.CopyTo();
                byte ColorPieza = Obtener_Color_Pieza(Cod);
                Alfil TAlfil = new Alfil();
                Torre TTorre = new Torre();
                bool blanco_o_negro = true;
                if (ColorPieza == 1) blanco_o_negro = true;
                else blanco_o_negro = false;

                Cod_Aux = TAlfil.ObtenereCodificación(blanco_o_negro);
                //MUEVE COMO ALFIL
                movio = TAlfil.ValidaMovimiento(CXYIF, copia_Tablero, Cod_Aux);
                if (movio.MOVIO == false)
                {
                    Cod_Aux = TTorre.ObtenereCodificación(blanco_o_negro);
                    //MUEVE COMO TORRE
                    movio = TTorre.ValidaMovimiento(CXYIF, copia_Tablero, Cod_Aux);
                }
            }
            return movio;
        }

        public override Movimiento ValidaMovimientoYMueve(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, Tablero_Index TTablero_Index)
        {
            Cod_Nro dato = new Cod_Nro();
            dato.Cod = (byte)PanelX.Tag;
            dato.Nro = Convert.ToByte(PanelX.Name);
            Movimiento movio = CXYIF;


            if (dato.Cod == ObtenereCodificación(true) || dato.Cod == ObtenereCodificación(false))
            {
                byte ColorPieza = Obtener_Color_Pieza(dato.Cod);

                Alfil TAlfil = new Alfil();
                Torre TTorre = new Torre();
                
                if (ColorPieza == 1)
                {
                    PanelX.Tag = TAlfil.ObtenereCodificación(true);
                    //MUEVE COMO ALFIL
                
                    movio = TAlfil.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form, Panel_0, TTablero, TTablero_Index);
                    if (movio.MOVIO == false)
                    {
                        PanelX.Tag = TTorre.ObtenereCodificación(true);
                        //MUEVE COMO TORRE
                        movio = TTorre.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form, Panel_0, TTablero, TTablero_Index);
                    }
                }
                else if (ColorPieza == 0)
                {
                    PanelX.Tag = TAlfil.ObtenereCodificación(false);
                    //MUEVE COMO ALFIL
                    movio = TAlfil.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form, Panel_0, TTablero, TTablero_Index);
                    if (movio.MOVIO == false)
                    {
                        PanelX.Tag = TTorre.ObtenereCodificación(false);
                        //MUEVE COMO TORRE
                        movio = TTorre.ValidaMovimientoYMueve(ref m_Dragging, PanelX, CXYIF, m_Form, Panel_0, TTablero, TTablero_Index);
                    }
                }
                PanelX.Tag = dato.Cod;

                //MODIFICA CLASE TABLERO NUEVAMENTE PORQUE ANTERIORMENTE LO HIZO COMO ALFIL O TORRE
                if (movio.MOVIO == true)
                {
                    ModificaClaseTablero(CXYIF, dato, TTablero, TTablero_Index);
                }
                else
                {
                    byte Pieza_XY_Final = TTablero.GET_CodFicha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End));
                    byte Pieza_XY_Final_Index = TTablero_Index.GET_Ficha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End));
                    TTablero.SET_CodFicha((byte)(CXYIF.X_Start), (byte)(CXYIF.Y_Start), 0);
                    TTablero.SET_CodFicha((byte)(CXYIF.X_Start), (byte)(CXYIF.Y_Start), dato.Cod);
                    
                    byte[,] Tablero_Real = Obtener_Tablero_64_Casillas(TTablero);

                    TTablero.SET_CodFicha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End), Pieza_XY_Final);
                    TTablero_Index.SET_Ficha((byte)(CXYIF.X_Start), (byte)(CXYIF.Y_Start), dato.Nro);
                    TTablero_Index.SET_Ficha((byte)(CXYIF.X_End), (byte)(CXYIF.Y_End), Pieza_XY_Final_Index);
                }

            }
            PanelX.Parent = Panel_0;

            return movio;
        }
        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte Reina = m_CodificacionPiezas.QN(); 
            if (es_blanca == true)
                Reina = m_CodificacionPiezas.QB();
            
            return Reina;
        }


    }
}
