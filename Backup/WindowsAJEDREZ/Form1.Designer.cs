﻿namespace Chess

{
    public partial class Chess
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public int zzz = new int();

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.juegoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.juegoNuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioVsUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuarioVsPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dificilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarPartidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblTurnoDeLas = new System.Windows.Forms.Label();
            this.lblBlancasONegras = new System.Windows.Forms.Label();
            this.list_Player_A = new System.Windows.Forms.ListBox();
            this.list_Player_B = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_min_izq = new System.Windows.Forms.Label();
            this.lbl_seg_izq = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_seg_der = new System.Windows.Forms.Label();
            this.lbl_min_der = new System.Windows.Forms.Label();
            this.girarTableroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.juegoToolStripMenuItem,
            this.ayudaToolStripMenuItem,
            this.ayudaToolStripMenuItem1});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1059, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // juegoToolStripMenuItem
            // 
            this.juegoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.juegoNuevoToolStripMenuItem,
            this.cargarPartidaToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.juegoToolStripMenuItem.Name = "juegoToolStripMenuItem";
            this.juegoToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.juegoToolStripMenuItem.Text = "Juego";
            this.juegoToolStripMenuItem.Click += new System.EventHandler(this.juegoToolStripMenuItem_Click);
            // 
            // juegoNuevoToolStripMenuItem
            // 
            this.juegoNuevoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuarioVsUsuarioToolStripMenuItem,
            this.usuarioVsPCToolStripMenuItem});
            this.juegoNuevoToolStripMenuItem.Name = "juegoNuevoToolStripMenuItem";
            this.juegoNuevoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.juegoNuevoToolStripMenuItem.Text = "Juego Nuevo";
            // 
            // usuarioVsUsuarioToolStripMenuItem
            // 
            this.usuarioVsUsuarioToolStripMenuItem.Name = "usuarioVsUsuarioToolStripMenuItem";
            this.usuarioVsUsuarioToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.usuarioVsUsuarioToolStripMenuItem.Text = "usuario vs usuario";
            this.usuarioVsUsuarioToolStripMenuItem.Click += new System.EventHandler(this.usuarioVsUsuarioToolStripMenuItem_Click);
            // 
            // usuarioVsPCToolStripMenuItem
            // 
            this.usuarioVsPCToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.facilToolStripMenuItem,
            this.medioToolStripMenuItem,
            this.dificilToolStripMenuItem});
            this.usuarioVsPCToolStripMenuItem.Name = "usuarioVsPCToolStripMenuItem";
            this.usuarioVsPCToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.usuarioVsPCToolStripMenuItem.Text = "usuario vs PC";
            // 
            // facilToolStripMenuItem
            // 
            this.facilToolStripMenuItem.Name = "facilToolStripMenuItem";
            this.facilToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.facilToolStripMenuItem.Text = "Facil";
            this.facilToolStripMenuItem.Click += new System.EventHandler(this.facilToolStripMenuItem_Click);
            // 
            // medioToolStripMenuItem
            // 
            this.medioToolStripMenuItem.Name = "medioToolStripMenuItem";
            this.medioToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.medioToolStripMenuItem.Text = "Medio";
            this.medioToolStripMenuItem.Click += new System.EventHandler(this.medioToolStripMenuItem_Click);
            // 
            // dificilToolStripMenuItem
            // 
            this.dificilToolStripMenuItem.Name = "dificilToolStripMenuItem";
            this.dificilToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.dificilToolStripMenuItem.Text = "Dificil";
            this.dificilToolStripMenuItem.Click += new System.EventHandler(this.dificilToolStripMenuItem_Click);
            // 
            // cargarPartidaToolStripMenuItem
            // 
            this.cargarPartidaToolStripMenuItem.Name = "cargarPartidaToolStripMenuItem";
            this.cargarPartidaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cargarPartidaToolStripMenuItem.Text = "Cargar Partida";
            this.cargarPartidaToolStripMenuItem.Visible = false;
            this.cargarPartidaToolStripMenuItem.Click += new System.EventHandler(this.cargarPartidaToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click_1);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.girarTableroToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.ayudaToolStripMenuItem.Text = "Opciones";
            this.ayudaToolStripMenuItem.Click += new System.EventHandler(this.ayudaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickToolStripMenuItem,
            this.arrastrarToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem1.Text = "Mover Piezas";
            this.toolStripMenuItem1.Visible = false;
            // 
            // clickToolStripMenuItem
            // 
            this.clickToolStripMenuItem.Checked = true;
            this.clickToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.clickToolStripMenuItem.Name = "clickToolStripMenuItem";
            this.clickToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clickToolStripMenuItem.Text = "Click";
            this.clickToolStripMenuItem.Click += new System.EventHandler(this.clickToolStripMenuItem_Click);
            // 
            // arrastrarToolStripMenuItem
            // 
            this.arrastrarToolStripMenuItem.Name = "arrastrarToolStripMenuItem";
            this.arrastrarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.arrastrarToolStripMenuItem.Text = "Arrastrar";
            this.arrastrarToolStripMenuItem.Click += new System.EventHandler(this.arrastrarToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem1
            // 
            this.ayudaToolStripMenuItem1.Name = "ayudaToolStripMenuItem1";
            this.ayudaToolStripMenuItem1.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem1.Text = "Ayuda";
            // 
            // lblTurnoDeLas
            // 
            this.lblTurnoDeLas.AutoSize = true;
            this.lblTurnoDeLas.Location = new System.Drawing.Point(459, 35);
            this.lblTurnoDeLas.Name = "lblTurnoDeLas";
            this.lblTurnoDeLas.Size = new System.Drawing.Size(87, 13);
            this.lblTurnoDeLas.TabIndex = 1;
            this.lblTurnoDeLas.Text = "TURNO DE LAS";
            // 
            // lblBlancasONegras
            // 
            this.lblBlancasONegras.AutoSize = true;
            this.lblBlancasONegras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlancasONegras.Location = new System.Drawing.Point(552, 35);
            this.lblBlancasONegras.Name = "lblBlancasONegras";
            this.lblBlancasONegras.Size = new System.Drawing.Size(63, 13);
            this.lblBlancasONegras.TabIndex = 2;
            this.lblBlancasONegras.Text = "BLANCAS";
            // 
            // list_Player_A
            // 
            this.list_Player_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.list_Player_A.FormattingEnabled = true;
            this.list_Player_A.ItemHeight = 18;
            this.list_Player_A.Location = new System.Drawing.Point(82, 114);
            this.list_Player_A.Name = "list_Player_A";
            this.list_Player_A.ScrollAlwaysVisible = true;
            this.list_Player_A.Size = new System.Drawing.Size(165, 364);
            this.list_Player_A.TabIndex = 6;
            this.list_Player_A.Visible = false;
            // 
            // list_Player_B
            // 
            this.list_Player_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.list_Player_B.FormattingEnabled = true;
            this.list_Player_B.ItemHeight = 18;
            this.list_Player_B.Location = new System.Drawing.Point(844, 114);
            this.list_Player_B.Name = "list_Player_B";
            this.list_Player_B.ScrollAlwaysVisible = true;
            this.list_Player_B.Size = new System.Drawing.Size(165, 364);
            this.list_Player_B.TabIndex = 7;
            this.list_Player_B.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "BLANCAS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(859, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "NEGRAS";
            // 
            // lbl_min_izq
            // 
            this.lbl_min_izq.AutoSize = true;
            this.lbl_min_izq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_min_izq.Location = new System.Drawing.Point(142, 38);
            this.lbl_min_izq.Name = "lbl_min_izq";
            this.lbl_min_izq.Size = new System.Drawing.Size(23, 15);
            this.lbl_min_izq.TabIndex = 12;
            this.lbl_min_izq.Text = "00";
            this.lbl_min_izq.Visible = false;
            // 
            // lbl_seg_izq
            // 
            this.lbl_seg_izq.AutoSize = true;
            this.lbl_seg_izq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_seg_izq.Location = new System.Drawing.Point(187, 38);
            this.lbl_seg_izq.Name = "lbl_seg_izq";
            this.lbl_seg_izq.Size = new System.Drawing.Size(23, 15);
            this.lbl_seg_izq.TabIndex = 13;
            this.lbl_seg_izq.Text = "00";
            this.lbl_seg_izq.Visible = false;
            this.lbl_seg_izq.Click += new System.EventHandler(this.lbl_seg_izq_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(170, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = ":";
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(858, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "NEGRAS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(857, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 15);
            this.label9.TabIndex = 11;
            this.label9.Text = "NEGRAS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(856, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 15);
            this.label13.TabIndex = 11;
            this.label13.Text = "NEGRAS";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(956, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = ":";
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // lbl_seg_der
            // 
            this.lbl_seg_der.AutoSize = true;
            this.lbl_seg_der.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_seg_der.Location = new System.Drawing.Point(975, 38);
            this.lbl_seg_der.Name = "lbl_seg_der";
            this.lbl_seg_der.Size = new System.Drawing.Size(23, 15);
            this.lbl_seg_der.TabIndex = 15;
            this.lbl_seg_der.Text = "00";
            this.lbl_seg_der.Visible = false;
            this.lbl_seg_der.Click += new System.EventHandler(this.lbl_seg_der_Click);
            // 
            // lbl_min_der
            // 
            this.lbl_min_der.AutoSize = true;
            this.lbl_min_der.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_min_der.Location = new System.Drawing.Point(927, 38);
            this.lbl_min_der.Name = "lbl_min_der";
            this.lbl_min_der.Size = new System.Drawing.Size(23, 15);
            this.lbl_min_der.TabIndex = 14;
            this.lbl_min_der.Text = "00";
            this.lbl_min_der.Visible = false;
            this.lbl_min_der.Click += new System.EventHandler(this.lbl_min_der_Click);
            // 
            // girarTableroToolStripMenuItem
            // 
            this.girarTableroToolStripMenuItem.Name = "girarTableroToolStripMenuItem";
            this.girarTableroToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.girarTableroToolStripMenuItem.Text = "Girar Tablero";
            this.girarTableroToolStripMenuItem.Click += new System.EventHandler(this.girarTableroToolStripMenuItem_Click);
            // 
            // Chess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1059, 667);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_seg_der);
            this.Controls.Add(this.lbl_min_der);
            this.Controls.Add(this.lbl_seg_izq);
            this.Controls.Add(this.lbl_min_izq);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.list_Player_B);
            this.Controls.Add(this.list_Player_A);
            this.Controls.Add(this.lblBlancasONegras);
            this.Controls.Add(this.lblTurnoDeLas);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Chess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.SystemColors.ButtonShadow;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.LocationChanged += new System.EventHandler(this.Chess_LocationChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem juegoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem juegoNuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuarioVsUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuarioVsPCToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem facilToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem medioToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem dificilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.Label lblTurnoDeLas;
        public System.Windows.Forms.Label lblBlancasONegras;
        private System.Windows.Forms.ListBox list_Player_A;
        private System.Windows.Forms.ListBox list_Player_B;
        private System.Windows.Forms.ToolStripMenuItem cargarPartidaToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clickToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrastrarToolStripMenuItem;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_min_izq;
        private System.Windows.Forms.Label lbl_seg_izq;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_seg_der;
        private System.Windows.Forms.Label lbl_min_der;
        private System.Windows.Forms.ToolStripMenuItem girarTableroToolStripMenuItem;
    }
}

