﻿namespace Chess
{
    
    public partial class Form_Coronacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel_reina = new System.Windows.Forms.Panel();
            this.panel_torre = new System.Windows.Forms.Panel();
            this.panel_alfil = new System.Windows.Forms.Panel();
            this.panel_caballo = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel_reina);
            this.panel1.Controls.Add(this.panel_torre);
            this.panel1.Controls.Add(this.panel_alfil);
            this.panel1.Controls.Add(this.panel_caballo);
            this.panel1.Location = new System.Drawing.Point(40, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 150);
            this.panel1.TabIndex = 0;
            // 
            // panel_reina
            // 
            this.panel_reina.BackColor = System.Drawing.Color.Transparent;
            this.panel_reina.Location = new System.Drawing.Point(80, 80);
            this.panel_reina.Name = "panel_reina";
            this.panel_reina.Size = new System.Drawing.Size(60, 60);
            this.panel_reina.TabIndex = 4;
            this.panel_reina.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Reina_Click);
            // 
            // panel_torre
            // 
            this.panel_torre.BackColor = System.Drawing.Color.Transparent;
            this.panel_torre.Location = new System.Drawing.Point(10, 80);
            this.panel_torre.Name = "panel_torre";
            this.panel_torre.Size = new System.Drawing.Size(60, 60);
            this.panel_torre.TabIndex = 3;
            this.panel_torre.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Torre_Click);
            // 
            // panel_alfil
            // 
            this.panel_alfil.BackColor = System.Drawing.Color.Transparent;
            this.panel_alfil.Location = new System.Drawing.Point(80, 10);
            this.panel_alfil.Name = "panel_alfil";
            this.panel_alfil.Size = new System.Drawing.Size(60, 60);
            this.panel_alfil.TabIndex = 2;
            this.panel_alfil.Paint += new System.Windows.Forms.PaintEventHandler(this.Alfil_Paint);
            this.panel_alfil.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Alfil_Click);
            // 
            // panel_caballo
            // 
            this.panel_caballo.BackColor = System.Drawing.Color.Transparent;
            this.panel_caballo.Location = new System.Drawing.Point(10, 10);
            this.panel_caballo.Name = "panel_caballo";
            this.panel_caballo.Size = new System.Drawing.Size(60, 60);
            this.panel_caballo.TabIndex = 1;
            this.panel_caballo.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_caballo_Paint);
            this.panel_caballo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_caballo_MouseClick);
            // 
            // Form_Coronacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 216);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Form_Coronacion";
            this.Text = "Coronar";
            this.Load += new System.EventHandler(this.Form_Coronacion_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel_reina;
        public System.Windows.Forms.Panel panel_torre;
        public System.Windows.Forms.Panel panel_alfil;
        public System.Windows.Forms.Panel panel_caballo;
    }
}