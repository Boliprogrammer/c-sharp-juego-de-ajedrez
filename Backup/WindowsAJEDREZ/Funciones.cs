﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace Chess
{
    public class Funciones
    {
        
        #region Variables Protegidas

        protected Int16 m_pos_X_Tablero;
        protected Int16 m_pos_Y_Tablero;
        
        #endregion Variables Protegidas

        #region Métodos

        public byte Separacion_Piezas_X() { return 66; }
        public byte Separacion_Piezas_Y() { return 66; }

        public byte Obtener_Color_Pieza(byte Codificacion_4)
        {
            return (byte)(Codificacion_4 & 1);

        }

        public byte Convertir_PosXY_NROcasilla_64(byte x, byte y)
        {
            byte NRO_Casilla = 0;
            //PEONES NEGROS UP
            for (byte i = 0; i <= 7; i++)
                if (x == i && y == 1) { NRO_Casilla = i; goto exit; }

            //PEONES BLANCOS DOWN
            for (byte i = 0; i <= 7; i++)
                if (x == i && y == 6) { NRO_Casilla = (byte)(i + 8); goto exit; }

            //TCA NEGRO UP IZQ
            for (byte i = 0; i <= 2; i++)
                if (x == i && y == 0) { NRO_Casilla = (byte)(16 + (2 * i)); goto exit; }

            //TCA BLANCO DOWN IZQ
            for (byte i = 0; i <= 2; i++)
                if (x == i && y == 7) { NRO_Casilla = (byte)(22 + (2 * i)); goto exit; }

            //TCA NEGRO UP DER
            for (byte i = 5; i <= 7; i++)
                if (x == i && y == 0) { NRO_Casilla = (byte)(21 - (2 * (i - 5))); goto exit; }

            //TCA BLANCO UP DER
            for (byte i = 5; i <= 7; i++)
                if (x == i && y == 7) { NRO_Casilla = (byte)(27 - (2 * (i - 5))); goto exit; }

            //QK NEGRO UP
            for (byte i = 3; i <= 4; i++)
                if (x == i && y == 0) { NRO_Casilla = (byte)(28 + (i - 3)); goto exit; }

            //QK BLANCO DOWN
            for (byte i = 3; i <= 4; i++)
                if (x == i && y == 7) { NRO_Casilla = (byte)(30 + (i - 3)); goto exit; }

            //CASILLAS VACIAS PREDETERMINDAS
            for (byte X_aux = 0; X_aux <= 7; X_aux++)
                for (byte Y_aux = 2; Y_aux <= 5; Y_aux++)
                    if (x == X_aux && y == Y_aux) { NRO_Casilla = (byte)(32 + X_aux + 8 * (Y_aux - 2)); goto exit; }


        exit:
            return NRO_Casilla;
        }

        public byte NroToCodificacion(byte nro, bool m_P_Blancas_Abajo)
        {
            CodificacionPiezas TCodificacionPiezas;

            byte FichaCodificada = 0;
            byte i = nro;

            if (m_P_Blancas_Abajo == true)
            {
                //FICHAS DE ARRIBA
                if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8)
                    FichaCodificada = TCodificacionPiezas.PN();//PN
                else if (i == 17 || i == 18)
                    FichaCodificada = TCodificacionPiezas.TN(); //TN
                else if (i == 19 || i == 20)
                    FichaCodificada = TCodificacionPiezas.CN();//CN
                else if (i == 21 || i == 22)
                    FichaCodificada = TCodificacionPiezas.AN();//AN
                else if (i == 29)
                    FichaCodificada = TCodificacionPiezas.QN(); //QN
                else if (i == 30)
                    FichaCodificada = TCodificacionPiezas.KN(); //KN
                //FICHAS DE ABAJO
                else if (i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16)
                    FichaCodificada = TCodificacionPiezas.PB(); //PB
                else if (i == 23 || i == 24)
                    FichaCodificada = TCodificacionPiezas.TB(); //TB
                else if (i == 25 || i == 26)
                    FichaCodificada = TCodificacionPiezas.CB(); //CB
                else if (i == 27 || i == 28)
                    FichaCodificada = TCodificacionPiezas.AB(); //AB
                else if (i == 31)
                    FichaCodificada = TCodificacionPiezas.QB(); //QB
                else if (i == 32)
                    FichaCodificada = TCodificacionPiezas.KB(); //KB
            }
            else
            {
                //FICHAS DE ARRIBA
                if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8)
                    FichaCodificada = TCodificacionPiezas.PB();//PB
                else if (i == 17 || i == 18)
                    FichaCodificada = TCodificacionPiezas.TB(); //TB
                else if (i == 19 || i == 20)
                    FichaCodificada = TCodificacionPiezas.CB();//CB
                else if (i == 21 || i == 22)
                    FichaCodificada = TCodificacionPiezas.AB();//AB
                else if (i == 29)
                    FichaCodificada = TCodificacionPiezas.QB(); //QB
                else if (i == 30)
                    FichaCodificada = TCodificacionPiezas.KB(); //KB
                //FICHAS DE ABAJO
                else if (i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16)
                    FichaCodificada = TCodificacionPiezas.PN(); //PN
                else if (i == 23 || i == 24)
                    FichaCodificada = TCodificacionPiezas.TN(); //TN
                else if (i == 25 || i == 26)
                    FichaCodificada = TCodificacionPiezas.CN(); //CN
                else if (i == 27 || i == 28)
                    FichaCodificada = TCodificacionPiezas.AN(); //AN
                else if (i == 31)
                    FichaCodificada = TCodificacionPiezas.QN(); //QN
                else if (i == 32)
                    FichaCodificada = TCodificacionPiezas.KN(); //KN          
            }
            return FichaCodificada;

        }

        public byte[,] Obtener_Tablero_64_Casillas(Tablero TTablero)
        {
            byte[,] TableroChess = new byte[8, 8];
            for (byte y = 0; y <= 7; y++)
            {
                for (byte x = 0; x <= 7; x++)
                {
                    byte cod = TTablero.GET_CodFicha(x, y);
                    TableroChess[x, y] = cod;
                }
            }
            return TableroChess;
        }

        #endregion Métodos
    }

    public class Tablero_Index : Funciones
    {
        private byte[,] m_Tablero;

        public Tablero_Index(bool Blancas_Down)
        {
            m_Tablero = new byte[8, 8];
            
            if (Blancas_Down == true)
            {
                for (int i = 0; i <= 7; i++)
                    m_Tablero[i, 1] = (byte)i;
                for (int i = 8; i <= 15; i++)
                    m_Tablero[i - 8, 6] = (byte)i;

                m_Tablero[0, 0] = 16;
                m_Tablero[7, 0] = 17;
                m_Tablero[1, 0] = 18;
                m_Tablero[6, 0] = 19;
                m_Tablero[2, 0] = 20;
                m_Tablero[5, 0] = 21;

                m_Tablero[0, 7] = 22;
                m_Tablero[7, 7] = 23;
                m_Tablero[1, 7] = 24;
                m_Tablero[6, 7] = 25;
                m_Tablero[2, 7] = 26;
                m_Tablero[5, 7] = 27;

                m_Tablero[3, 0] = 28;
                m_Tablero[4, 0] = 29;
                m_Tablero[3, 7] = 30;
                m_Tablero[4, 7] = 31;
            }

        }

        public void SET_Ficha(byte x, byte y, byte dato)
        {
            m_Tablero[x, y] = dato;
        }

        public byte GET_Ficha(byte x, byte y)
        {
            return m_Tablero[x, y];
        }

    }

    public struct Cod_Nro
    {
        public byte Cod;
        public byte Nro;
    }

    class Partidas
    {
        private string[] m_lista_movimientos_string;
        private string m_ruta;
        private Chess m_Form1;
        private int m_Panel_0_Left;
        private int m_Panel_0_Top;
        private int m_cont;

        public Partidas() { m_cont = 0; }

        public void SET_Inf_Importante(string ruta, Chess Form1, int Panel_0_Left, int Panel_0_Top)
        {
            m_Form1 = Form1;
            m_ruta = ruta;
            Cargar_Movimientos_Del_Archivo_Al_Array_De_String();
            m_Panel_0_Left = Panel_0_Left;
            m_Panel_0_Top = Panel_0_Top;
        }

        protected byte Convertir_Notacion_a_Coordenadas_X(string X)
        {
            switch (X)
            {
                case "a":
                    return 0;
                case "b":
                    return 1;
                case "c":
                    return 2;
                case "d":
                    return 3;
                case "e":
                    return 4;
                case "f":
                    return 5;
                case "g":
                    return 6;
                case "h":
                    return 7;
                default:
                    return 8;

            }

        }

        private void Cargar_Movimientos_Del_Archivo_Al_Array_De_String()
        {
            List<string> lista_movimientos_list = new List<string>() ;
            FileStream archivo_cargar_partida_FS_XY = new FileStream(m_ruta, FileMode.Open, FileAccess.Read);
            StreamReader archivo_cargar_partida_XY = new StreamReader(archivo_cargar_partida_FS_XY);

            while (!archivo_cargar_partida_XY.EndOfStream)
            {
                lista_movimientos_list.Add(archivo_cargar_partida_XY.ReadLine());
            }
            archivo_cargar_partida_XY.Close();

            m_lista_movimientos_string = lista_movimientos_list.ToArray();
        }

        public void Inicializar_Partida() { m_cont = 0; }

        public bool Mover_Pieza_Siguiente_Movimiento()
        {

            if (m_cont < m_lista_movimientos_string.Length)
            {
                SimularMovimientosMouse simular_movimientos_mouse = new SimularMovimientosMouse();
                
                /*
                byte X_Start = Convertir_Notacion_a_Coordenadas_X(m_lista_movimientos_string[m_cont].Substring(0, 1));
                byte Y_Start = (byte)(7 - (byte)(Convert.ToByte(m_lista_movimientos_string[m_cont].Substring(1, 1)) - 1));

                byte X_End = Convertir_Notacion_a_Coordenadas_X(m_lista_movimientos_string[m_cont].Substring(5, 1));
                byte Y_End = (byte)(7 - (byte)(Convert.ToByte(m_lista_movimientos_string[m_cont].Substring(6, 1)) - 1));
                */
                
                /*
                float X_Start_Float = m_Form1.Left + (((float)(X_Start + 0.5)) * 66);
                float Y_Start_Float = m_Form1.Top + ((float)(Y_Start + 0.5) * 66);
                float X_End_Float = m_Form1.Left + ((float)(X_End + 0.5) * 66); // ( 306 - 24)
                float Y_End_Float = m_Form1.Top + ((float)(Y_End + 0.5) * 6); // (333 - 18)
                */

                /*
                float X_Start_Float = m_Form1.Left + (X_Start * 66);
                float Y_Start_Float = m_Form1.Top + (Y_Start * 66);
                float X_End_Float = m_Form1.Left + (X_End * 66); // ( 306 - 24)
                float Y_End_Float = m_Form1.Top + (Y_End * 66); // (333 - 18)
                */

                /*
                float X_Start_Float = 286 * (m_Form1.Left + (X_Start * 66)) / 306;
                float Y_Start_Float = 412 * (m_Form1.Top + (Y_Start * 66)) / 465;
                float X_End_Float = (265 * (m_Form1.Left + (X_End * 66))) / (306); // ( 306 - 24)
                float Y_End_Float = (268 * (m_Form1.Top + (Y_End * 66))) / (333); // (333 - 18)
                

                int X_Start_Int = (int)(X_Start_Float) + 325;
                int Y_Start_Int = (int)(Y_Start_Float) + 180 - 15; //175
                int X_End_Int = (int)(X_End_Float) + 325 + 22;
                int Y_End_Int = (int)(Y_End_Float) + 180 + 15;

                 */

                string []X_Y_Start_End = new string[4];

                byte cont = 0;

                for (byte i = 0; i < m_lista_movimientos_string[m_cont].Length; i++ )
                {
                    if (m_lista_movimientos_string[m_cont].Substring(i, 1) != " ")
                    {
                        X_Y_Start_End[cont] = X_Y_Start_End[cont] + m_lista_movimientos_string[m_cont].Substring(i, 1);
                    }
                    else
                        cont++;
                }

                int X_Start_Int = Convert.ToInt32(X_Y_Start_End[0]) /* + m_Form1.Left + m_Panel_0_Left + 33*/;
                int Y_Start_Int = Convert.ToInt32(X_Y_Start_End[1]) /*+ m_Form1.Top + m_Panel_0_Top + 60*/;
                int X_End_Int = Convert.ToInt32(X_Y_Start_End[2]) /*+ m_Form1.Left + m_Panel_0_Left + 33*/;
                int Y_End_Int = Convert.ToInt32(X_Y_Start_End[3]) /*+ m_Form1.Top + m_Panel_0_Top + 60*/;


                Thread.Sleep(1000);
                simular_movimientos_mouse.Haz_Mouse_Move(X_Start_Int, Y_Start_Int, X_End_Int, Y_End_Int);
                //simular_movimientos_mouse.Haz_Mouse_Move(0, 0, 0, 0);
                m_cont++;
                return true;
            }
            else
                return false;
        }

    }

    public struct Movimiento_Posible
    {
        public byte X_End;
        public byte Y_End;
    }

    struct Peon_struct
    {
        public List<Movimiento_Posible> GET_Movimientos_Posibles(byte x_inicial, byte y_inicial, byte cod)
        {
            List<Movimiento_Posible> list_posibles_mov = new List<Movimiento_Posible>();
            Movimiento_Posible posibles_mov = new Movimiento_Posible();

            if ((byte)(cod & 1) == (byte)1) //ES BLANCO
            {
                for (byte i = 1; i <= 2; i++)
                {
                    if (y_inicial - i >= 0 && y_inicial - i <= 7)
                    {
                        posibles_mov.Y_End = (byte)(y_inicial - i);
                        posibles_mov.X_End = x_inicial;
                        list_posibles_mov.Add(posibles_mov);
                    }
                }

                for (sbyte i = -1; i <= 1; i = (sbyte)(i + 2))
                {
                    if (x_inicial + i >= 0 && x_inicial + i <= 7 && y_inicial - 1 >= 0 && y_inicial - 1 <= 7)
                    {
                        posibles_mov.Y_End = (byte)(y_inicial - 1);
                        posibles_mov.X_End = (byte)(x_inicial + i);
                        list_posibles_mov.Add(posibles_mov);
                    }
                }

            }
            else
            {
                for (byte i = 1; i <= 2; i++)
                {
                    if (y_inicial + i >= 0 && y_inicial + i <= 7)
                    {
                        posibles_mov.Y_End = (byte)(y_inicial + i);
                        posibles_mov.X_End = x_inicial;
                        list_posibles_mov.Add(posibles_mov);
                    }
                }

                for (sbyte i = -1; i <= 1; i = (sbyte)(i + 2))
                {
                    if (x_inicial + i >= 0 && x_inicial + i <= 7 && y_inicial + 1 >= 0 && y_inicial + 1 <= 7)
                    {
                        posibles_mov.Y_End = (byte)(y_inicial + 1);
                        posibles_mov.X_End = (byte)(x_inicial + i);
                        list_posibles_mov.Add(posibles_mov);
                    }
                }
            }
            return list_posibles_mov;
        }
    }

    struct Torre_struct
    {
        public List<Movimiento_Posible> GET_Movimientos_Posibles(byte x_inicial, byte y_inicial, byte cod)
        {
            List<Movimiento_Posible> list_posibles_mov = new List<Movimiento_Posible>();
            Movimiento_Posible posibles_mov = new Movimiento_Posible();
            for (byte i = 0; i <= 7; i = (byte)(i + 1))
            {
                //vertical x se mantiene
                if (x_inicial >= 0 && x_inicial <= 7 && y_inicial + i >= 0 && y_inicial <= 7)
                {
                    posibles_mov.X_End = (byte)(x_inicial);
                    posibles_mov.Y_End = (byte)(y_inicial + (i - y_inicial));
                    if ((x_inicial != posibles_mov.X_End) || (y_inicial != posibles_mov.Y_End))
                    {
                        list_posibles_mov.Add(posibles_mov);
                    }
                }

            }
            for (byte j = 0; j <= 7; j = (byte)(j + 1))
            {
                //horizontal y se mantiene
                if (x_inicial + j >= 0 && x_inicial <= 7 && y_inicial >= 0 && y_inicial <= 7)
                {
                    posibles_mov.X_End = (byte)(x_inicial + (j - x_inicial));
                    posibles_mov.Y_End = (byte)(y_inicial);
                    if ((x_inicial != posibles_mov.X_End) || (y_inicial != posibles_mov.Y_End))
                    {
                        list_posibles_mov.Add(posibles_mov);
                    }
                }
            }
            return list_posibles_mov;
        }
    }
    struct Caballo_struct
    {
        public List<Movimiento_Posible> GET_Movimientos_Posibles(byte x_inicial, byte y_inicial, byte cod)
        {
            List<Movimiento_Posible> list_posibles_mov = new List<Movimiento_Posible>();
            Movimiento_Posible posibles_mov = new Movimiento_Posible();
            byte i = 2, j = 1;
            if ((y_inicial - i >= 0 && y_inicial - i <= 7 && x_inicial + j >= 0 && x_inicial + j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial - i);
                posibles_mov.X_End = (byte)(x_inicial + j);
                list_posibles_mov.Add(posibles_mov);
            }
            if ((y_inicial - i >= 0 && y_inicial - i <= 7 && x_inicial - j >= 0 && x_inicial - j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial - i);
                posibles_mov.X_End = (byte)(x_inicial - j);
                list_posibles_mov.Add(posibles_mov);
            }
            if ((y_inicial + i >= 0 && y_inicial + i <= 7 && x_inicial + j >= 0 && x_inicial + j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial + i);
                posibles_mov.X_End = (byte)(x_inicial + j);
                list_posibles_mov.Add(posibles_mov);
            }
            if ((y_inicial + i >= 0 && y_inicial + i <= 7 && x_inicial - j >= 0 && x_inicial - j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial + i);
                posibles_mov.X_End = (byte)(x_inicial - j);
                list_posibles_mov.Add(posibles_mov);
            }
            //i=2 y j=1
            if ((x_inicial + i >= 0 && x_inicial + i <= 7 && y_inicial - j >= 0 && y_inicial - j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial - j);
                posibles_mov.X_End = (byte)(x_inicial + i);
                list_posibles_mov.Add(posibles_mov);
            }
            if ((x_inicial + i >= 0 && x_inicial + i <= 7 && y_inicial + j >= 0 && y_inicial + j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial + j);
                posibles_mov.X_End = (byte)(x_inicial + i);
                list_posibles_mov.Add(posibles_mov);
            }
            if ((x_inicial - i >= 0 && x_inicial - i <= 7 && y_inicial - j >= 0 && y_inicial - j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial - j);
                posibles_mov.X_End = (byte)(x_inicial - i);
                list_posibles_mov.Add(posibles_mov);
            }
            if ((x_inicial - i >= 0 && x_inicial - i <= 7 && y_inicial + j >= 0 && y_inicial + j <= 7))
            {
                posibles_mov.Y_End = (byte)(y_inicial + j);
                posibles_mov.X_End = (byte)(x_inicial - i);
                list_posibles_mov.Add(posibles_mov);
            }
                     return list_posibles_mov;
        }
    }
    struct Alfil_struct
    {
        public List<Movimiento_Posible> GET_Movimientos_Posibles(byte x_inicial, byte y_inicial, byte cod)
        {
            List<Movimiento_Posible> list_posibles_mov = new List<Movimiento_Posible>();
            Movimiento_Posible posibles_mov = new Movimiento_Posible();
            byte i = 1, j = 1;
            while (i <= 7 && j <= 7)
            {

                if ((x_inicial + i >= 0 && x_inicial + i <= 7) && (y_inicial - j >= 0 && y_inicial - j <= 7))
                {
                    posibles_mov.X_End = (byte)(x_inicial + i);
                    posibles_mov.Y_End = (byte)(y_inicial - j);
                    list_posibles_mov.Add(posibles_mov);
                }
                if ((x_inicial - i >= 0 && x_inicial - i <= 7) && (y_inicial + j >= 0 && y_inicial + j <= 7))
                {
                    posibles_mov.X_End = (byte)(x_inicial - i);
                    posibles_mov.Y_End = (byte)(y_inicial + j);
                    list_posibles_mov.Add(posibles_mov);
                }
                if ((x_inicial - i >= 0 && x_inicial - i <= 7) && (y_inicial - j >= 0 && y_inicial - j <= 7))
                {
                    posibles_mov.X_End = (byte)(x_inicial - i);
                    posibles_mov.Y_End = (byte)(y_inicial - j);
                    list_posibles_mov.Add(posibles_mov);
                }
                if ((x_inicial + i >= 0 && x_inicial + i <= 7) && (y_inicial + j >= 0 && y_inicial + j <= 7))
                {
                    posibles_mov.X_End = (byte)(x_inicial + i);
                    posibles_mov.Y_End = (byte)(y_inicial + j);
                    list_posibles_mov.Add(posibles_mov);
                }
                i++;
                j++;
            }

            return list_posibles_mov;
        }
    }
    struct Rey_struct
    {
        public List<Movimiento_Posible> GET_Movimientos_Posibles(byte x_inicial, byte y_inicial, byte cod)
        {
            List<Movimiento_Posible> list_posibles_mov = new List<Movimiento_Posible>();
            Movimiento_Posible posibles_mov = new Movimiento_Posible();
            for (sbyte i = -1; i <= 1; i++)
            {
                for (sbyte j = -1; j <= 1; j++)
                {
                    if ((byte)(cod & 1) == (byte)1) //BLANCO 
                    {
                        if (((x_inicial + i) >= 0 && (x_inicial + i) <= 7) && ((y_inicial + j) >= 0 && (y_inicial + j) <= 7))
                        {
                            posibles_mov.X_End = (byte)(x_inicial + i);
                            posibles_mov.Y_End = (byte)(y_inicial + j);
                            if ((x_inicial != posibles_mov.X_End) || (y_inicial != posibles_mov.Y_End))
                            {
                                list_posibles_mov.Add(posibles_mov);
                            }
                            if ((posibles_mov.X_End == 3 && posibles_mov.Y_End == 7))
                            {
                                posibles_mov.X_End = (byte)(posibles_mov.X_End - 1);
                                list_posibles_mov.Add(posibles_mov);
                            }
                            if ((posibles_mov.X_End == 5 && posibles_mov.Y_End == 7))
                            {
                                posibles_mov.X_End = (byte)(posibles_mov.X_End + 1);
                                list_posibles_mov.Add(posibles_mov);
                            }
                        }
                    }
                    else
                    {
                        if ((x_inicial + i) >= 0 && (x_inicial + i) <= 7 && (y_inicial + j) >= 0 && (y_inicial + j) <= 7)
                        {
                            posibles_mov.X_End = (byte)(x_inicial + i);
                            posibles_mov.Y_End = (byte)(y_inicial + j);
                            if ((x_inicial != posibles_mov.X_End) || (y_inicial != posibles_mov.Y_End))
                            {
                                list_posibles_mov.Add(posibles_mov);
                            }
                            if ((posibles_mov.X_End == 3 && posibles_mov.Y_End == 0))
                            {
                                posibles_mov.X_End = (byte)(posibles_mov.X_End - 1);
                                list_posibles_mov.Add(posibles_mov);
                            }
                            if ((posibles_mov.X_End == 5 && posibles_mov.Y_End == 0))
                            {
                                posibles_mov.X_End = (byte)(posibles_mov.X_End + 1);
                                list_posibles_mov.Add(posibles_mov);
                            }
                        }
                    }

                }
            }
            return list_posibles_mov;
        }
    }

    struct Reina_struct
    {
        public List<Movimiento_Posible> GET_Movimientos_Posibles(byte x_inicial, byte y_inicial, byte cod)
        {
            List<Movimiento_Posible> list_posibles_mov_torre = new List<Movimiento_Posible>();
            List<Movimiento_Posible> list_posibles_mov2_alfil = new List<Movimiento_Posible>();

            Torre_struct torre_str = new Torre_struct();
            Alfil_struct alfil_str = new Alfil_struct();

            list_posibles_mov_torre = torre_str.GET_Movimientos_Posibles(x_inicial, y_inicial, cod);
            list_posibles_mov2_alfil = alfil_str.GET_Movimientos_Posibles(x_inicial, y_inicial, cod);
            Movimiento_Posible[] list_posibles_mov_alfil_array = list_posibles_mov2_alfil.ToArray();

            for (byte i = 0; i < list_posibles_mov_alfil_array.Length; i++)
                list_posibles_mov_torre.Add(list_posibles_mov_alfil_array[i]);
            return list_posibles_mov_torre;
        }
    }




}
