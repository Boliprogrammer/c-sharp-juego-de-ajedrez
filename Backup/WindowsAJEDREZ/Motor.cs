﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Chess
{
    
    class Motor : Funciones
    {

        private Evaluador m_Evaluador = new Evaluador();
        int m_dificultad = -1;
        byte m_tam = 10;

        private int Max(int profundidad, ref Movimiento Mejor_Movida, int Alfa, int Beta, ref int Nro_Nodos, Tablero TTablero
            , Torre TTorre)
        {

            List<Movimiento> List_Movimientos_VALIDOS = new List<Movimiento>();
            List<Movimiento> List_Mejores_Movimientos = new List<Movimiento>();
            Movimiento TMovimiento = new Movimiento();
            Movimiento Copia_TMovimiento = new Movimiento();
            short cont = 1;
            int val = 0, tam;
            //byte[,] tab = Obtener_Tablero_64_Casillas(TTablero);
            Nro_Nodos++;

            if (profundidad == 0)
            {
                Alfa = m_Evaluador.Evaluar(TTablero);
                //byte[,] tab2 = Obtener_Tablero_64_Casillas(TTablero); 
            }
            else
            {
                //byte[,] tab4 = Obtener_Tablero_64_Casillas(TTablero);
                List_Movimientos_VALIDOS = TTablero.Obtener_Movimientos_Validos(TTablero, false, TTorre);
                byte nro = 1;
                if (m_dificultad == 1)
                    nro = 2;
                //byte[,] tab3 = Obtener_Tablero_64_Casillas(TTablero);
                tam = List_Movimientos_VALIDOS.Count;
                while (cont <= tam / nro )
                {
                    TMovimiento = List_Movimientos_VALIDOS[cont - 1];
                    Copia_TMovimiento = TMovimiento.CopyTo();
                    Tablero copia_TTablero = TTablero.CopyTo();

                    /*
                    if (cont == 1 && tam >= 6 && m_dificultad == 1)
                    {
                        for (byte i = 0; i < tam; i++)
                        {
                            Tablero copia_Tablero2 = new Tablero();
                            copia_Tablero2 = copia_TTablero.CopyTo();
                            byte Cod_Ficha_Pos_End2 = copia_Tablero2.GET_CodFicha((byte)List_Movimientos_VALIDOS[i].X_End,
                                (byte)List_Movimientos_VALIDOS[i].Y_End);
                            byte Cod_Ficha_Pos_Start2 = copia_Tablero2.GET_CodFicha((byte)List_Movimientos_VALIDOS[i].X_Start,
                                (byte)List_Movimientos_VALIDOS[i].Y_Start);
                            copia_Tablero2.SET_CodFicha((byte)List_Movimientos_VALIDOS[i].X_End, (byte)List_Movimientos_VALIDOS[i].Y_End,
                                Cod_Ficha_Pos_Start2);
                            copia_Tablero2.SET_CodFicha((byte)List_Movimientos_VALIDOS[i].X_Start,
                                (byte)List_Movimientos_VALIDOS[i].Y_Start, 0);
                            Movimiento Nuevo_Movimiento = List_Movimientos_VALIDOS[i];
                            Nuevo_Movimiento.VALOR_TABLERO = m_Evaluador.Evaluar(copia_Tablero2);
                            List_Mejores_Movimientos.Add(Nuevo_Movimiento);

                        }
                        List_Mejores_Movimientos.Sort(delegate(Movimiento m1, Movimiento m2)
                        {
                            return m2.VALOR_TABLERO.CompareTo(m1.VALOR_TABLERO);
                        });

                        for (byte i = 0; i < m_tam; i++)
                        {
                            if (i == 0)
                            {
                                List_Movimientos_VALIDOS.Clear();
                            }
                            List_Movimientos_VALIDOS.Add(List_Mejores_Movimientos[i]);
                        }
                        tam = List_Movimientos_VALIDOS.Count;
                        TMovimiento = List_Movimientos_VALIDOS[cont - 1];
                        Copia_TMovimiento = TMovimiento.CopyTo();
                    }*/
                    ///****MUEVE LA PIEZA******////
                    byte Cod_Ficha_Pos_End = copia_TTablero.GET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End);
                    byte Cod_Ficha_Pos_Start = copia_TTablero.GET_CodFicha((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start);
                    copia_TTablero.SET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End, Cod_Ficha_Pos_Start);
                    copia_TTablero.SET_CodFicha((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start, 0);
                    ///****MUEVE LA PIEZA******////
                    //byte[,] tab2 = Obtener_Tablero_64_Casillas(TTablero); 
                    if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate != 0)
                    {
                        if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate == 1
                            || TTablero.m_Ahogado_Tablas_Jaque_mate == 2) ///EMPATE AHOGADO
                            
                        {
                            val = 0;
                        }
                        else if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate == 3) //JAQUE 
                            val = 100000;
                        else if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate == 4) //JAQUE MATE
                            val = 1000000;
                    }
                    else
                    {
                        
                        val = Min(profundidad - 1, ref TMovimiento, Alfa, Beta, ref Nro_Nodos, copia_TTablero, TTorre);
                    }

                    //byte[,] tab5 = Obtener_Tablero_64_Casillas(TTablero);

                    //byte[,] tab3 = Obtener_Tablero_64_Casillas(TTablero);

                    if (val > Alfa) //REALIZA LA PODA
                    {
                        Alfa = val;
                        Mejor_Movida = Copia_TMovimiento.CopyTo();
                    }

                    if (Alfa >= Beta)
                        cont = (short)(List_Movimientos_VALIDOS.Count + 1); //FORZA EL TERMINADO DE BUCLE

                    cont++;
                }

            }

            TMovimiento = null;
            Copia_TMovimiento = null;
            GC.Collect();
            return Alfa;
        }

        private int Min(int profundidad, ref Movimiento Mejor_Movida, int Alfa, int Beta, ref int Nro_Nodos, Tablero TTablero
            , Torre TTorre)
        {
            List<Movimiento> List_Movimientos_VALIDOS = new List<Movimiento>();
            Movimiento TMovimiento = new Movimiento();
            Movimiento Copia_TMovimiento = new Movimiento();
            short cont = 1;
            int val = 0, tam;
            Nro_Nodos++;

            if (profundidad == 0)
            {
                //byte[,] tab2 = Obtener_Tablero_64_Casillas(TTablero); 
                Beta = m_Evaluador.Evaluar(TTablero);
                //byte [,]tab = Obtener_Tablero_64_Casillas(TTablero); 
            }
            else
            {
                List_Movimientos_VALIDOS = TTablero.Obtener_Movimientos_Validos(TTablero, true, TTorre);
                //DEBERIA ORDENAR LA LISTA 
                tam = List_Movimientos_VALIDOS.Count;
                while (cont <= tam)
                {
                    TMovimiento = List_Movimientos_VALIDOS[cont - 1];
                    
                    Copia_TMovimiento = TMovimiento.CopyTo();
                    Tablero copia_TTablero = TTablero.CopyTo();

                    //byte[,] tab = Obtener_Tablero_64_Casillas(TTablero); 
                    ///****MUEVE LA PIEZA******////
                    byte Cod_Ficha_Pos_End = copia_TTablero.GET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End);
                    byte Cod_Ficha_Pos_Start = copia_TTablero.GET_CodFicha((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start);
                    copia_TTablero.SET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End, Cod_Ficha_Pos_Start);
                    copia_TTablero.SET_CodFicha((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start, 0);
                    ///****MUEVE LA PIEZA******////
                    //byte[,] tab5 = Obtener_Tablero_64_Casillas(TTablero);

                    if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate != 0)
                    {
                        if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate == 1
                            || TTablero.m_Ahogado_Tablas_Jaque_mate == 2) ///EMPATE AHOGADO
                            val = 0;
                        else if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate == 3) //JAQUE 
                            val = -100000;
                        else if (copia_TTablero.m_Ahogado_Tablas_Jaque_mate == 4) //JAQUE MATE
                            val = -1000000;
                    }
                    else
                        val = Max(profundidad - 1, ref TMovimiento, Alfa, Beta, ref Nro_Nodos, copia_TTablero, TTorre);

                    //****DESHACE MOVIMIENTO******////
                    //TTablero.SET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End, 0);
                    //TTablero.SET_CodFicha((byte)TMovimiento.X_End, (byte)TMovimiento.Y_End, Cod_Ficha_Pos_End);
                    //TTablero.SET_CodFicha((byte)TMovimiento.X_Start, (byte)TMovimiento.Y_Start, Cod_Ficha_Pos_Start);
                    //****DESHACE MOVIMIENTO******////

                    //byte[,] tab6 = Obtener_Tablero_64_Casillas(TTablero); 

                    if (val < Beta) //REALIZA LA PODA
                    {
                        Beta = val;
                        Mejor_Movida = Copia_TMovimiento.CopyTo() ;
                    }

                    if (Beta <= Alfa)
                        cont = (short)(List_Movimientos_VALIDOS.Count + 1); //FORZA EL TERMINADO DE BUCLE

                    cont++;
                }

            }
            TMovimiento = null;
            Copia_TMovimiento = null;
            GC.Collect();
            return Beta;
        }

        public Movimiento Pensar(int Dificultad, Tablero TTablero, Torre TTorre)
        {
            if (Dificultad > 1)
                Dificultad = Dificultad - 2;
            else
                m_dificultad = Dificultad;
            Movimiento Mejor_Movida = new Movimiento();
            int Nro_Nodos = 0;
            //byte[,] tab = Obtener_Tablero_64_Casillas(TTablero);
            Max(Dificultad, ref Mejor_Movida, -9999999, 9999999, ref Nro_Nodos, TTablero, TTorre);

            return Mejor_Movida;
        }

        private void Ordenar_Lista_Movimientos_Ascendentemente(ref List<Movimiento> List_Movimientos)
        {
            List_Movimientos.Sort();
        }
    }
}
