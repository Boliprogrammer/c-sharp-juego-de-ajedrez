﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chess
{
    public class Evaluador
    {
        public int Evaluar(Tablero TTablero)
        {
            int Puntuacion_Total = 0;
            Puntuacion_Total = Evaluacion_De_Balance_De_Material(TTablero);
            return Puntuacion_Total;
        }

        private int Cuidar_Reina()
        {
            return 0;
        }

        private int Penalizacion_Por_Ser_Atacado()
        {
            return 0;
            /*
            for (byte y = 0; y <= 7; y++)
            {
                for (byte y = 0; y <= 7; y++)
                { 
                                        
                }
            }
             */ 
        }



        private int Evaluacion_De_Balance_De_Material(Tablero TTablero)
        {
            //SOLO TOMA EN CUENTA QUE LAS NEGRAS ES LA COMPUTADORA
            int Puntuacion_Del_Balance_De_Material = 0;
            for (byte y = 0; y <= 7; y++)
            {
                for (byte x = 0; x <= 7; x++)
                {
                    byte cod = TTablero.GET_CodFicha(x, y);
                    int valor_pieza_X = Obtener_Valor_De_PIEZA_X(cod);
                    int valor_cada_casilla = Obtener_Valor_De_Cada_CASILLA(x, y);
                    Puntuacion_Del_Balance_De_Material = Puntuacion_Del_Balance_De_Material +
                        ( valor_cada_casilla * valor_pieza_X);
                }
            }

            return Puntuacion_Del_Balance_De_Material;
        }

        private byte Obtener_Valor_De_Cada_CASILLA(byte XX, byte YY)
        {
            byte[,] Valor_De_Cada_Casilla_Tablero = 
                        { { 1, 1, 1, 1, 1,1,1,1 },{1,2,2,2,2,2,2,1},{1,2,3,3,3,3,2,1},{1,2,3,5,5,3,2,1},
                          {1,2,3,5,5,3,2,1},{1,2,3,3,3,3,2,1},{1,2,2,2,2,2,2,1},{1, 1, 1, 1 , 1 ,1,1,1}};
            return Valor_De_Cada_Casilla_Tablero[XX, YY];

        }

        private sbyte Obtener_Valor_De_PIEZA_X(byte Cod_Pieza)
        {

            CodificacionPiezas cod_piezas = new CodificacionPiezas();
            if (Cod_Pieza == cod_piezas.KN())
                return 1;
            else if (Cod_Pieza == cod_piezas.QN())
                return 9;
            else if (Cod_Pieza == cod_piezas.TN())
                return 5;
            else if (Cod_Pieza == cod_piezas.AN())
                return 4;
            else if (Cod_Pieza == cod_piezas.CN())
                return 3;
            else if (Cod_Pieza == cod_piezas.PN())
                return 2;
            else if (Cod_Pieza == cod_piezas.KB())
                return -1;
            else if (Cod_Pieza == cod_piezas.QB())
                return -9 * 2;
            else if (Cod_Pieza == cod_piezas.TB())
                return -5 * 2;
            else if (Cod_Pieza == cod_piezas.AB())
                return -4 * 2;
            else if (Cod_Pieza == cod_piezas.CB())
                return -3 * 2;
            else if (Cod_Pieza == cod_piezas.PB())
                return -2 * 2;
            else
                return 0;
        }
    }
}
