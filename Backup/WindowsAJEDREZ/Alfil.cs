﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Chess
{
    class Alfil : Pieza
    {

        public override Movimiento ValidaMovimiento(Movimiento CXYIF, Tablero TTablero, 
                byte Cod)
        {
            Movimiento valido = CXYIF;
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UN ALFIL A TRAVES DE LA CODIFICACION
            if (Cod == ObtenereCodificación(true) || Cod == ObtenereCodificación(false))
            {
                //EL DESPLAZAMIENTO EN PUNTOS DE COORDENADAS REDONDEADOS
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;
                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTO
                if ((dif_Y != 0 && dif_X != 0 && (dif_X == dif_Y || dif_X == -dif_Y || dif_Y == -dif_X)) 
                    && (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(Cod) 
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                    && (TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, Cod) == false))
                {
                    if (VerificarCaminoOcupado(CXYIF, TTablero) == false)
                    {
                        valido.MOVIO = true;
                        //COMIO LA FICHA PORQUE LA POSICION EN QUE SE MOVIO HABÍA UNA FICHA ES DECIR ERA DISTINTO DE CERO
                        if (PosFichaAComer(CXYIF, TTablero) != 0)
                            valido.COMIO = true;
                    }
                }
            }
            return valido;
        }

        public override Movimiento ValidaMovimientoYMueve(ref bool m_Dragging, 
            System.Windows.Forms.Panel PanelX, Movimiento CXYIF, System.Windows.Forms.Form m_Form, 
            System.Windows.Forms.Panel Panel_0, Tablero TTablero, Tablero_Index TTablero_Index)
        {
            Movimiento valido = CXYIF;
            Cod_Nro dato = new Cod_Nro();
            dato.Cod = (byte)PanelX.Tag;
            dato.Nro = Convert.ToByte(PanelX.Name);
            
            ////VERIFICA QUE LA FICHA CLICKEADA SEA UN ALFIL A TRAVES DE LA CODIFICACION
            if (dato.Cod == ObtenereCodificación(true) || dato.Cod == ObtenereCodificación(false))
            {
                //EL DESPLAZAMIENTO EN PUNTOS DE COORDENADAS REDONDEADOS
                int dif_X = CXYIF.X_End - CXYIF.X_Start; // x_destino - x_inicial;
                int dif_Y = CXYIF.Y_End - CXYIF.Y_Start; // y_destino - y_inicial;

                //bool pieza_mueve_mismo_color_hace_jaque = false;
                //VALIDA MOVIMIENTO
                if ((dif_Y != 0 && dif_X != 0 && (dif_X == dif_Y || dif_X == -dif_Y || dif_Y == -dif_X)) 
                    && (Obtener_Color_Pieza(PosFichaAComer(CXYIF, TTablero)) != this.Obtener_Color_Pieza(dato.Cod) 
                    || (Verifica_SIEsCasillaVacia_EnPosAMoverse(TTablero, CXYIF))) 
                    && VerificaQueNOSeaRey(PosFichaAComer(CXYIF, TTablero)) 
                    && ( TTablero.Verifica_SI_Pos_A_Moverse_Esta_En_Jaque(TTablero, CXYIF, dato.Cod) == false))
                {
                    if (VerificarCaminoOcupado(CXYIF,TTablero) == false)
                    {
                        //MUEVE PIEZA
                        Mover_Solo_Piezas_Paneles(PanelX, CXYIF);
                        CambioInterfCuandoMovValido(ref m_Dragging, m_Form, PanelX);
                        valido.MOVIO = true;

                        
                        //COMIO LA FICHA PORQUE LA POSICION EN QUE SE MOVIO HABÍA UNA FICHA ES DECIR ERA DISTINTO DE CERO
                        if (PosFichaAComer(CXYIF,TTablero) != 0)
                        {
                            valido.COMIO = true;
                            valido.Index_Del_Panel = Index_Ficha_A_Comer(CXYIF, TTablero_Index);
                        }

                        //MODIFICA LA CLASE TABLERO
                        if (PanelX.Name == Convert.ToString(20) || PanelX.Name == Convert.ToString(21) || PanelX.Name == Convert.ToString(26) || PanelX.Name == Convert.ToString(27))
                            ModificaClaseTablero(CXYIF, dato , TTablero, TTablero_Index);

                    }
                    else //MOVIMIENTO NOOOOOOOOO VALIDO
                    {
                        //if (PanelX.Name == Convert.ToString(20) || PanelX.Name == Convert.ToString(21) || PanelX.Name == Convert.ToString(26) || PanelX.Name == Convert.ToString(27))
                            CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);
                    }

                }
                else //MOVIMIENTO NOOOOOOOOO VALIDO
                {
                    //if (PanelX.Name == Convert.ToString(20) || PanelX.Name == Convert.ToString(21) || PanelX.Name == Convert.ToString(26) || PanelX.Name == Convert.ToString(27))
                        CuandoMovimientoPiezasNOValido(ref m_Dragging, PanelX, Panel_0, m_Form, CXYIF);
                }
            }
            return valido;
        }
        public override byte ObtenereCodificación(bool es_blanca)
        {
            byte Alfil = m_CodificacionPiezas.AN();
            if (es_blanca == true)
                Alfil = m_CodificacionPiezas.AB(); 

            return Alfil;
        }

        public override bool VerificarCaminoOcupado(Movimiento CXYIF, Tablero TTablero)
        {
            bool ocupado = false;
            if (CXYIF.X_End - CXYIF.X_Start >= 0)
            {
                if (CXYIF.Y_End - CXYIF.Y_Start >= 0)
                {
                    //CUADRANTE IV +1 +1
                    int desplazamiento_y = CXYIF.Y_Start + 1;
                    for (sbyte i = (sbyte)(CXYIF.X_Start + 1); i <= (CXYIF.X_End - 1) && ocupado == false; i++)
                    {

                        if (TTablero.EsCasillaVacia((byte)i, (byte)desplazamiento_y) == false)
                            ocupado = true;
                        desplazamiento_y++;
                    }
                }
                else
                {
                    //CUADRANTE III +1 -1
                    int desplazamiento_y = CXYIF.Y_Start - 1;
                    for (sbyte i = (sbyte)(CXYIF.X_Start + 1); i <= (CXYIF.X_End - 1) && ocupado == false; i++)
                    {

                        if (TTablero.EsCasillaVacia((byte)i, (byte)desplazamiento_y) == false)
                            ocupado = true;
                        desplazamiento_y--;
                    }
                }
                
            }
            else
            {
                //CUADRANTE II -1 +1
                if (CXYIF.Y_End - CXYIF.Y_Start >= 0)
                {
                    int desplazamiento_y = CXYIF.Y_Start + 1;
                    for (sbyte i = (sbyte)(CXYIF.X_Start - 1); i >= (CXYIF.X_End + 1) && ocupado == false; i--)
                    {

                        if (TTablero.EsCasillaVacia((byte)i, (byte)desplazamiento_y) == false)
                            ocupado = true;
                        desplazamiento_y++;
                    }
                }
                else
                {
                    //CUADRANTE I -1 -1
                    int desplazamiento_y = CXYIF.Y_Start - 1;
                    for (sbyte i = (sbyte)(CXYIF.X_Start - 1); i >= (CXYIF.X_End + 1) && ocupado == false; i--)
                    {

                        if (TTablero.EsCasillaVacia((byte)i, (byte)desplazamiento_y) == false)
                            ocupado = true;
                        desplazamiento_y--;
                    }
                }
            }
            return ocupado;
        }

    }
}
